/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Release
	Model Element	: Display
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Release\Display.cpp
*********************************************************************/

//## auto_generated
#include "Display.h"
//## auto_generated
#include <iostream>
//## package Default

//## class Display
Display::Display() {
    //#[ operation Display()
    std::cout << "Constructed" << std::endl;
    //#]
}

Display::~Display() {
}

/*********************************************************************
	File Path	: Test\Release\Display.cpp
*********************************************************************/
