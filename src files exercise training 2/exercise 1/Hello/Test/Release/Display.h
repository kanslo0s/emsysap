/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Release
	Model Element	: Display
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Release\Display.h
*********************************************************************/

#ifndef Display_H
#define Display_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class Display
class Display {
    ////    Constructors and destructors    ////
    
public :

    //## operation Display()
    Display();
    
    //## auto_generated
    ~Display();
};

#endif
/*********************************************************************
	File Path	: Test\Release\Display.h
*********************************************************************/
