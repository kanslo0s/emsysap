/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Release
	Model Element	: Display
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Release\Display.h
*********************************************************************/

#ifndef Display_H
#define Display_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## package Default

//## class Display
class Display : public OMReactive {
    ////    Constructors and destructors    ////
    
public :

    //## operation Display()
    Display(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Display();
    
    ////    Operations    ////
    
    //## operation isDone()
    bool isDone();
    
    //## operation print(int)
    void print(int n);
    
    //## operation print(char*)
    void print(char* s);
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getCount() const;
    
    //## auto_generated
    void setCount(int p_count);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpStatechart();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    int count;		//## attribute count
    
    ////    Framework operations    ////

public :

    //## statechart_method
    void rootStateEntDef();
    
    //## statechart_method
    void ActiveEnter();
    
    //## statechart_method
    void ActiveExit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus ActiveTakeTimeout();
    
    ////    Framework    ////
    
//#[ ignore
    Display* concept;
    
    OMState* Active;
//#]

protected :

//#[ ignore
    IOxfTimeout* rootState_timeout;
//#]
};

//#[ ignore
class Display_ROOT : public OMComponentState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Display_ROOT(Display* c, OMState* p);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual void entDef();
    
    ////    Framework    ////
    
    Display* concept;
};

class Display_Active : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Display_Active(Display* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual void enterState();
    
    //## statechart_method
    virtual void exitState();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Display* concept;
};
//#]

#endif
/*********************************************************************
	File Path	: Test\Release\Display.h
*********************************************************************/
