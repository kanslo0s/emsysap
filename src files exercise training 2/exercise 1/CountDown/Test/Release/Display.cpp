/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Release
	Model Element	: Display
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Release\Display.cpp
*********************************************************************/

//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include "Display.h"
//## auto_generated
#include <iostream>
//## package Default

//## class Display
Display::Display(IOxfActive* theActiveContext) : count(0) {
    setActiveContext(theActiveContext, false);
    initStatechart();
    //#[ operation Display()
    print ("Constructed");
    //#]
}

Display::~Display() {
    cleanUpStatechart();
    cancelTimeouts();
}

bool Display::isDone() {
    //#[ operation isDone()
    return (0==count);
    //#]
}

void Display::print(int n) {
    //#[ operation print(int)
    std::cout << "Count = " << n << std::endl;
    //#]
}

void Display::print(char* s) {
    //#[ operation print(char*)
    std::cout << s << std::endl;
    //#]
}

int Display::getCount() const {
    return count;
}

void Display::setCount(int p_count) {
    count = p_count;
}

bool Display::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Display::initStatechart() {
    rootState_timeout = NULL;
    delete rootState;
    rootState = new Display_ROOT(this, NULL);
    rootState->subState = NULL;
    rootState->active = NULL;
    Active = new Display_Active(this, rootState, rootState);
    concept = this;
}

void Display::cleanUpStatechart() {
    delete rootState;
    rootState = NULL;
    delete Active;
    Active = NULL;
}

void Display::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Display::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void Display::rootStateEntDef() {
    //#[ transition 2 
    count=10;print("Started");
    //#]
    Active->entDef();
}

void Display::ActiveEnter() {
    rootState_timeout = scheduleTimeout(200, NULL);
}

void Display::ActiveExit() {
    cancel(rootState_timeout);
}

IOxfReactive::TakeEventStatus Display::ActiveTakeTimeout() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(getCurrentEvent() == rootState_timeout)
        {
            //## transition 1 
            if(isDone())
                {
                    Active->exitState();
                    //#[ transition 3 
                    print(count);
                    //#]
                    //#[ transition 1 
                    print("Done");
                    //#]
                    endBehavior();
                    res = eventConsumed;
                }
            else
                {
                    Active->exitState();
                    //#[ transition 3 
                    print(count);
                    //#]
                    //#[ transition 0 
                    --count;
                    //#]
                    Active->entDef();
                    res = eventConsumed;
                }
        }
    return res;
}

//#[ ignore
Display_ROOT::Display_ROOT(Display* c, OMState* p) : OMComponentState(p) {
    concept = c;
}

void Display_ROOT::entDef() {
    enterState();
    concept->rootStateEntDef();
}

Display_Active::Display_Active(Display* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
}

void Display_Active::enterState() {
    parent->setSubState(this);
    component->active = this;
    concept->ActiveEnter();
}

void Display_Active::exitState() {
    concept->ActiveExit();
}

IOxfReactive::TakeEventStatus Display_Active::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            res = concept->ActiveTakeTimeout();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}
//#]

/*********************************************************************
	File Path	: Test\Release\Display.cpp
*********************************************************************/
