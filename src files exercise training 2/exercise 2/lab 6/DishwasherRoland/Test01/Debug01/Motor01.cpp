/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: Motor01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\Motor01.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Motor01.h"
//#[ ignore
#define Default_Motor01_Motor01_SERIALIZE OM_NO_OP

#define Default_Motor01_off01_SERIALIZE OM_NO_OP

#define Default_Motor01_on01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Motor01
Motor01::Motor01() {
    NOTIFY_CONSTRUCTOR(Motor01, Motor01(), 0, Default_Motor01_Motor01_SERIALIZE);
}

Motor01::~Motor01() {
    NOTIFY_DESTRUCTOR(~Motor01, true);
}

void Motor01::off01() {
    NOTIFY_OPERATION(off01, off01(), 0, Default_Motor01_off01_SERIALIZE);
    //#[ operation off01()
    //#]
}

void Motor01::on01() {
    NOTIFY_OPERATION(on01, on01(), 0, Default_Motor01_on01_SERIALIZE);
    //#[ operation on01()
    //#]
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(Motor01, Default, Default, false, OMAnimatedMotor01)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Debug01\Motor01.cpp
*********************************************************************/
