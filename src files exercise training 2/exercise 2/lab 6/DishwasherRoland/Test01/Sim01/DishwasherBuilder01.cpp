/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Sim01
	Model Element	: DishwasherBuilder01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Sim01\DishwasherBuilder01.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "DishwasherBuilder01.h"
//#[ ignore
#define Default_DishwasherBuilder01_DishwasherBuilder01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DishwasherBuilder01
DishwasherBuilder01::DishwasherBuilder01(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DishwasherBuilder01, DishwasherBuilder01(), 0, Default_DishwasherBuilder01_DishwasherBuilder01_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsDishWasherRoland.setShouldDelete(false);
        }
        {
            itsFrontPanel01.setShouldDelete(false);
        }
    }
    initRelations();
}

DishwasherBuilder01::~DishwasherBuilder01() {
    NOTIFY_DESTRUCTOR(~DishwasherBuilder01, true);
}

DishWasherRoland* DishwasherBuilder01::getItsDishWasherRoland() const {
    return (DishWasherRoland*) &itsDishWasherRoland;
}

FrontPanel01* DishwasherBuilder01::getItsFrontPanel01() const {
    return (FrontPanel01*) &itsFrontPanel01;
}

Motor01* DishwasherBuilder01::getItsMotor01() const {
    return (Motor01*) &itsMotor01;
}

bool DishwasherBuilder01::startBehavior() {
    bool done = true;
    done &= itsDishWasherRoland.startBehavior();
    done &= itsFrontPanel01.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void DishwasherBuilder01::initRelations() {
    itsFrontPanel01.setItsDishWasherRoland(&itsDishWasherRoland);
    itsDishWasherRoland.setItsMotor01(&itsMotor01);
}

void DishwasherBuilder01::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsDishWasherRoland.setActiveContext(theActiveContext, false);
        itsFrontPanel01.setActiveContext(theActiveContext, false);
    }
}

void DishwasherBuilder01::destroy() {
    itsDishWasherRoland.destroy();
    itsFrontPanel01.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDishwasherBuilder01::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsMotor01", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsMotor01);
    aomsRelations->addRelation("itsDishWasherRoland", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDishWasherRoland);
    aomsRelations->addRelation("itsFrontPanel01", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsFrontPanel01);
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(DishwasherBuilder01, Default, Default, false, OMAnimatedDishwasherBuilder01)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Sim01\DishwasherBuilder01.cpp
*********************************************************************/
