/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Sim01
	Model Element	: Motor01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Sim01\Motor01.h
*********************************************************************/

#ifndef Motor01_H
#define Motor01_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class Motor01
class Motor01 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedMotor01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Motor01();
    
    //## auto_generated
    ~Motor01();
    
    ////    Operations    ////
    
    //## operation off01()
    void off01();
    
    //## operation on01()
    void on01();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedMotor01 : virtual public AOMInstance {
    DECLARE_META(Motor01, OMAnimatedMotor01)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\Sim01\Motor01.h
*********************************************************************/
