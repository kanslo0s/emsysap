/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_0
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_0.h
*********************************************************************/

#ifndef class_0_H
#define class_0_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_0
class class_0 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_0();
    
    //## auto_generated
    ~class_0();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_0.h
*********************************************************************/
