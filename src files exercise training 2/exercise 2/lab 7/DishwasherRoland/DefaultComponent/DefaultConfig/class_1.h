/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_1
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_1.h
*********************************************************************/

#ifndef class_1_H
#define class_1_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_1
class class_1 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_1();
    
    //## auto_generated
    ~class_1();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_1.h
*********************************************************************/
