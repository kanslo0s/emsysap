/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: Default
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "DishWasherRoland.h"
//## classInstance itsDishwasherBuilder01
#include "DishwasherBuilder01.h"
//## auto_generated
#include "FrontPanel01.h"
//## auto_generated
#include "Motor01.h"
//#[ ignore
#define evStart_SERIALIZE OM_NO_OP

#define evStart_UNSERIALIZE OM_NO_OP

#define evStart_CONSTRUCTOR evStart()

#define evOpen_SERIALIZE OM_NO_OP

#define evOpen_UNSERIALIZE OM_NO_OP

#define evOpen_CONSTRUCTOR evOpen()

#define evClose_SERIALIZE OM_NO_OP

#define evClose_UNSERIALIZE OM_NO_OP

#define evClose_CONSTRUCTOR evClose()

#define evService01_SERIALIZE OM_NO_OP

#define evService01_UNSERIALIZE OM_NO_OP

#define evService01_CONSTRUCTOR evService01()

#define evClose01_SERIALIZE OM_NO_OP

#define evClose01_UNSERIALIZE OM_NO_OP

#define evClose01_CONSTRUCTOR evClose01()

#define evOpen01_SERIALIZE OM_NO_OP

#define evOpen01_UNSERIALIZE OM_NO_OP

#define evOpen01_CONSTRUCTOR evOpen01()

#define evMode01_SERIALIZE OM_NO_OP

#define evMode01_UNSERIALIZE OM_NO_OP

#define evMode01_CONSTRUCTOR evMode01()

#define evStart01_SERIALIZE OM_NO_OP

#define evStart01_UNSERIALIZE OM_NO_OP

#define evStart01_CONSTRUCTOR evStart01()

#define evFault01_SERIALIZE OM_NO_OP

#define evFault01_UNSERIALIZE OM_NO_OP

#define evFault01_CONSTRUCTOR evFault01()

#define evKeyPress01_SERIALIZE OMADD_SER(key01, x2String(myEvent->key01))

#define evKeyPress01_UNSERIALIZE OMADD_UNSER(int, key01, OMDestructiveString2X)

#define evKeyPress01_CONSTRUCTOR evKeyPress01(key01)
//#]

//## package Default


//## classInstance itsDishwasherBuilder01
DishwasherBuilder01 itsDishwasherBuilder01;

#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

static void RenameGlobalInstances();

IMPLEMENT_META_PACKAGE(Default, Default)
#endif // _OMINSTRUMENT

void Default_initRelations() {
    {
        {
            itsDishwasherBuilder01.setShouldDelete(false);
        }
    }
    
    #ifdef _OMINSTRUMENT
    RenameGlobalInstances();
    #endif // _OMINSTRUMENT
}

bool Default_startBehavior() {
    bool done = true;
    done &= itsDishwasherBuilder01.startBehavior();
    return done;
}

#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}

static void RenameGlobalInstances() {
    OM_SET_INSTANCE_NAME(&itsDishwasherBuilder01, DishwasherBuilder01, "itsDishwasherBuilder01", AOMNoMultiplicity);
}
#endif // _OMINSTRUMENT

//#[ ignore
Default_OMInitializer::Default_OMInitializer() {
    Default_initRelations();
    Default_startBehavior();
}

Default_OMInitializer::~Default_OMInitializer() {
}
//#]

//## event evStart()
evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart())

//## event evOpen()
evOpen::evOpen() {
    NOTIFY_EVENT_CONSTRUCTOR(evOpen)
    setId(evOpen_Default_id);
}

bool evOpen::isTypeOf(const short id) const {
    return (evOpen_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evOpen, Default, Default, evOpen())

//## event evClose()
evClose::evClose() {
    NOTIFY_EVENT_CONSTRUCTOR(evClose)
    setId(evClose_Default_id);
}

bool evClose::isTypeOf(const short id) const {
    return (evClose_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evClose, Default, Default, evClose())

//## event evService01()
evService01::evService01() {
    NOTIFY_EVENT_CONSTRUCTOR(evService01)
    setId(evService01_Default_id);
}

bool evService01::isTypeOf(const short id) const {
    return (evService01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evService01, Default, Default, evService01())

//## event evClose01()
evClose01::evClose01() {
    NOTIFY_EVENT_CONSTRUCTOR(evClose01)
    setId(evClose01_Default_id);
}

bool evClose01::isTypeOf(const short id) const {
    return (evClose01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evClose01, Default, Default, evClose01())

//## event evOpen01()
evOpen01::evOpen01() {
    NOTIFY_EVENT_CONSTRUCTOR(evOpen01)
    setId(evOpen01_Default_id);
}

bool evOpen01::isTypeOf(const short id) const {
    return (evOpen01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evOpen01, Default, Default, evOpen01())

//## event evMode01()
evMode01::evMode01() {
    NOTIFY_EVENT_CONSTRUCTOR(evMode01)
    setId(evMode01_Default_id);
}

bool evMode01::isTypeOf(const short id) const {
    return (evMode01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evMode01, Default, Default, evMode01())

//## event evStart01()
evStart01::evStart01() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart01)
    setId(evStart01_Default_id);
}

bool evStart01::isTypeOf(const short id) const {
    return (evStart01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart01, Default, Default, evStart01())

//## event evFault01()
evFault01::evFault01() {
    NOTIFY_EVENT_CONSTRUCTOR(evFault01)
    setId(evFault01_Default_id);
}

bool evFault01::isTypeOf(const short id) const {
    return (evFault01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evFault01, Default, Default, evFault01())

//## event evKeyPress01(int)
evKeyPress01::evKeyPress01(int p_key01) : key01(p_key01) {
    NOTIFY_EVENT_CONSTRUCTOR(evKeyPress01)
    setId(evKeyPress01_Default_id);
}

bool evKeyPress01::isTypeOf(const short id) const {
    return (evKeyPress01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKeyPress01, Default, Default, evKeyPress01(int))

/*********************************************************************
	File Path	: Test01\Debug01\Default.cpp
*********************************************************************/
