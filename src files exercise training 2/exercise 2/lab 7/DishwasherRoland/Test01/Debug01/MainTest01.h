/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: Debug01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\MainTest01.h
*********************************************************************/

#ifndef MainTest01_H
#define MainTest01_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
class Test01 {
    ////    Constructors and destructors    ////
    
public :

    Test01();
};

#endif
/*********************************************************************
	File Path	: Test01\Debug01\MainTest01.h
*********************************************************************/
