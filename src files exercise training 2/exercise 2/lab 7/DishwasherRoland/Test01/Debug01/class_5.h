/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: class_5
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\class_5.h
*********************************************************************/

#ifndef class_5_H
#define class_5_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class class_5
class class_5 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedclass_5;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    class_5();
    
    //## auto_generated
    ~class_5();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedclass_5 : virtual public AOMInstance {
    DECLARE_META(class_5, OMAnimatedclass_5)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\Debug01\class_5.h
*********************************************************************/
