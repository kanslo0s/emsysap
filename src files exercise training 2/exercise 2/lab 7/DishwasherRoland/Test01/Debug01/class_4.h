/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: class_4
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\class_4.h
*********************************************************************/

#ifndef class_4_H
#define class_4_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class class_4
class class_4 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedclass_4;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    class_4();
    
    //## auto_generated
    ~class_4();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedclass_4 : virtual public AOMInstance {
    DECLARE_META(class_4, OMAnimatedclass_4)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\Debug01\class_4.h
*********************************************************************/
