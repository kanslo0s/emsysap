/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: class_4
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\class_4.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_4.h"
//#[ ignore
#define Default_class_4_class_4_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class class_4
class_4::class_4() {
    NOTIFY_CONSTRUCTOR(class_4, class_4(), 0, Default_class_4_class_4_SERIALIZE);
}

class_4::~class_4() {
    NOTIFY_DESTRUCTOR(~class_4, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_4, Default, Default, false, OMAnimatedclass_4)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Debug01\class_4.cpp
*********************************************************************/
