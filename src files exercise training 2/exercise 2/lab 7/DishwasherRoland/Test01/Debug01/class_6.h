/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: class_6
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\class_6.h
*********************************************************************/

#ifndef class_6_H
#define class_6_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class class_6
class class_6 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedclass_6;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    class_6();
    
    //## auto_generated
    ~class_6();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedclass_6 : virtual public AOMInstance {
    DECLARE_META(class_6, OMAnimatedclass_6)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\Debug01\class_6.h
*********************************************************************/
