/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: DC_Sim01
	Model Element	: DC_Sim01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\DC_Sim01\MainTest01.cpp
*********************************************************************/

//## auto_generated
#include "MainTest01.h"
//## auto_generated
#include "Default.h"
//## auto_generated
#include "DC_DishwasherBuilder01.h"
Test01::Test01() {
    Default_initRelations();
    Default_startBehavior();
}

int main(int argc, char* argv[]) {
    int status = 0;
    if(OXF::initialize(argc, argv, 6423, "", 0, 0, false))
        {
            DC_DishwasherBuilder01 * p_DC_DishwasherBuilder01;
            Test01 initializer_Test01;
            p_DC_DishwasherBuilder01 = new DC_DishwasherBuilder01;
            p_DC_DishwasherBuilder01->startBehavior();
            //#[ configuration Test01::DC_Sim01 
            //#]
            OXF::start();
            delete p_DC_DishwasherBuilder01;
            status = 0;
        }
    else
        {
            status = 1;
        }
    return status;
}

/*********************************************************************
	File Path	: Test01\DC_Sim01\MainTest01.cpp
*********************************************************************/
