/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: DC_Sim01
	Model Element	: DC_DishwasherBuilder01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\DC_Sim01\DC_DishwasherBuilder01.h
*********************************************************************/

#ifndef DC_DishwasherBuilder01_H
#define DC_DishwasherBuilder01_H

//## auto_generated
#include <WebComponents\WebComponentsTypes.h>
//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## classInstance itsDC
#include "DC.h"
//## classInstance itsDishWasherRoland
#include "DishWasherRoland.h"
//## classInstance itsFrontPanel01
#include "FrontPanel01.h"
//## package Default

//## class DC_DishwasherBuilder01
class DC_DishwasherBuilder01 : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDC_DishwasherBuilder01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    DC_DishwasherBuilder01(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~DC_DishwasherBuilder01();
    
    ////    Additional operations    ////
    
    //## auto_generated
    DC* getItsDC() const;
    
    //## auto_generated
    DishWasherRoland* getItsDishWasherRoland() const;
    
    //## auto_generated
    FrontPanel01* getItsFrontPanel01() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();

public :

//#[ ignore
    virtual const ClassWebAdapter * getItsWebAdapter() const;
    
    void visitWebAdaptedRelations() const;
//#]

protected :

//#[ ignore
    void initWebAdapters();
//#]

private :

//#[ ignore
    void notifyWebRelationModified() const;
//#]

    ////    Relations and components    ////

protected :

    DC itsDC;		//## classInstance itsDC
    
    DishWasherRoland itsDishWasherRoland;		//## classInstance itsDishWasherRoland
    
    FrontPanel01 itsFrontPanel01;		//## classInstance itsFrontPanel01
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
    
    ////    Framework    ////
    
//#[ ignore
    ClassWebAdapter * itsWebAdapter;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDC_DishwasherBuilder01 : virtual public AOMInstance {
    DECLARE_META(DC_DishwasherBuilder01, OMAnimatedDC_DishwasherBuilder01)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\DC_Sim01\DC_DishwasherBuilder01.h
*********************************************************************/
