/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: DC_Sim01
	Model Element	: DC_DishwasherBuilder01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\DC_Sim01\DC_DishwasherBuilder01.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "DC_DishwasherBuilder01.h"
//#[ ignore
#define Default_DC_DishwasherBuilder01_DC_DishwasherBuilder01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DC_DishwasherBuilder01
DC_DishwasherBuilder01::DC_DishwasherBuilder01(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DC_DishwasherBuilder01, DC_DishwasherBuilder01(), 0, Default_DC_DishwasherBuilder01_DC_DishwasherBuilder01_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsDishWasherRoland.setShouldDelete(false);
        }
        {
            itsFrontPanel01.setShouldDelete(false);
        }
    }
    initRelations();
    initWebAdapters();
}

DC_DishwasherBuilder01::~DC_DishwasherBuilder01() {
    NOTIFY_DESTRUCTOR(~DC_DishwasherBuilder01, true);
    delete itsWebAdapter;
}

DC* DC_DishwasherBuilder01::getItsDC() const {
    return (DC*) &itsDC;
}

DishWasherRoland* DC_DishwasherBuilder01::getItsDishWasherRoland() const {
    return (DishWasherRoland*) &itsDishWasherRoland;
}

FrontPanel01* DC_DishwasherBuilder01::getItsFrontPanel01() const {
    return (FrontPanel01*) &itsFrontPanel01;
}

bool DC_DishwasherBuilder01::startBehavior() {
    bool done = true;
    done &= itsDishWasherRoland.startBehavior();
    done &= itsFrontPanel01.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void DC_DishwasherBuilder01::initRelations() {
    itsFrontPanel01.setItsDishWasherRoland(&itsDishWasherRoland);
    itsDishWasherRoland.setItsMotor01(&itsDC);
}

//#[ ignore
const ClassWebAdapter * DC_DishwasherBuilder01::getItsWebAdapter() const {
    return itsWebAdapter;
}

void DC_DishwasherBuilder01::visitWebAdaptedRelations() const {
    {
        const DishWasherRoland* itsClient = getItsDishWasherRoland();
        if (itsClient != NULL) {
            const ClassWebAdapter * peerAdapter = itsClient->getItsWebAdapter();
            if (peerAdapter != NULL) {
                itsWebAdapter->UpdateWebRelation(peerAdapter);
            }
        }
    }
    {
        const FrontPanel01* itsClient = getItsFrontPanel01();
        if (itsClient != NULL) {
            const ClassWebAdapter * peerAdapter = itsClient->getItsWebAdapter();
            if (peerAdapter != NULL) {
                itsWebAdapter->UpdateWebRelation(peerAdapter);
            }
        }
    }
}

void DC_DishwasherBuilder01::initWebAdapters() {
    itsWebAdapter = new ClassWebAdapterTmpl< DC_DishwasherBuilder01 >(this, "DC_DishwasherBuilder01");
}

void DC_DishwasherBuilder01::notifyWebRelationModified() const {
    ClassWebAdapter::NotifyWebRelationModified();
}
//#]

void DC_DishwasherBuilder01::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsDishWasherRoland.setActiveContext(theActiveContext, false);
        itsFrontPanel01.setActiveContext(theActiveContext, false);
    }
}

void DC_DishwasherBuilder01::destroy() {
    itsDishWasherRoland.destroy();
    itsFrontPanel01.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDC_DishwasherBuilder01::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsDC", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDC);
    aomsRelations->addRelation("itsDishWasherRoland", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDishWasherRoland);
    aomsRelations->addRelation("itsFrontPanel01", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsFrontPanel01);
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(DC_DishwasherBuilder01, Default, Default, false, OMAnimatedDC_DishwasherBuilder01)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\DC_Sim01\DC_DishwasherBuilder01.cpp
*********************************************************************/
