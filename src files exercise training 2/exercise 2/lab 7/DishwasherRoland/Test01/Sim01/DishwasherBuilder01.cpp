/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Sim01
	Model Element	: DishwasherBuilder01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Sim01\DishwasherBuilder01.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "DishwasherBuilder01.h"
//#[ ignore
#define Default_DishwasherBuilder01_DishwasherBuilder01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DishwasherBuilder01
DishwasherBuilder01::DishwasherBuilder01(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DishwasherBuilder01, DishwasherBuilder01(), 0, Default_DishwasherBuilder01_DishwasherBuilder01_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsDishWasherRoland.setShouldDelete(false);
        }
        {
            itsFrontPanel01.setShouldDelete(false);
        }
    }
    initRelations();
    initWebAdapters();
}

DishwasherBuilder01::~DishwasherBuilder01() {
    NOTIFY_DESTRUCTOR(~DishwasherBuilder01, true);
    delete itsWebAdapter;
}

AC* DishwasherBuilder01::getItsAC() const {
    return (AC*) &itsAC;
}

DishWasherRoland* DishwasherBuilder01::getItsDishWasherRoland() const {
    return (DishWasherRoland*) &itsDishWasherRoland;
}

FrontPanel01* DishwasherBuilder01::getItsFrontPanel01() const {
    return (FrontPanel01*) &itsFrontPanel01;
}

bool DishwasherBuilder01::startBehavior() {
    bool done = true;
    done &= itsDishWasherRoland.startBehavior();
    done &= itsFrontPanel01.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void DishwasherBuilder01::initRelations() {
    itsFrontPanel01.setItsDishWasherRoland(&itsDishWasherRoland);
    itsDishWasherRoland.setItsMotor01(&itsAC);
}

//#[ ignore
const ClassWebAdapter * DishwasherBuilder01::getItsWebAdapter() const {
    return itsWebAdapter;
}

void DishwasherBuilder01::visitWebAdaptedRelations() const {
    {
        const DishWasherRoland* itsClient = getItsDishWasherRoland();
        if (itsClient != NULL) {
            const ClassWebAdapter * peerAdapter = itsClient->getItsWebAdapter();
            if (peerAdapter != NULL) {
                itsWebAdapter->UpdateWebRelation(peerAdapter);
            }
        }
    }
    {
        const FrontPanel01* itsClient = getItsFrontPanel01();
        if (itsClient != NULL) {
            const ClassWebAdapter * peerAdapter = itsClient->getItsWebAdapter();
            if (peerAdapter != NULL) {
                itsWebAdapter->UpdateWebRelation(peerAdapter);
            }
        }
    }
}

void DishwasherBuilder01::initWebAdapters() {
    itsWebAdapter = new ClassWebAdapterTmpl< DishwasherBuilder01 >(this, "DishwasherBuilder01");
}

void DishwasherBuilder01::notifyWebRelationModified() const {
    ClassWebAdapter::NotifyWebRelationModified();
}
//#]

void DishwasherBuilder01::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsDishWasherRoland.setActiveContext(theActiveContext, false);
        itsFrontPanel01.setActiveContext(theActiveContext, false);
    }
}

void DishwasherBuilder01::destroy() {
    itsDishWasherRoland.destroy();
    itsFrontPanel01.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDishwasherBuilder01::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsDishWasherRoland", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDishWasherRoland);
    aomsRelations->addRelation("itsFrontPanel01", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsFrontPanel01);
    aomsRelations->addRelation("itsAC", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsAC);
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(DishwasherBuilder01, Default, Default, false, OMAnimatedDishwasherBuilder01)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Sim01\DishwasherBuilder01.cpp
*********************************************************************/
