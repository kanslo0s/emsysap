/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: AC_Sim01
	Model Element	: AC_Sim01
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: Test01\AC_Sim01\MainTest01.h
*********************************************************************/

#ifndef MainTest01_H
#define MainTest01_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
class Test01 {
    ////    Constructors and destructors    ////
    
public :

    Test01();
};

#endif
/*********************************************************************
	File Path	: Test01\AC_Sim01\MainTest01.h
*********************************************************************/
