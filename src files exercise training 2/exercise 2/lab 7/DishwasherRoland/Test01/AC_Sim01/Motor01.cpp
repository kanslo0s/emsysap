/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: AC_Sim01
	Model Element	: Motor01
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: Test01\AC_Sim01\Motor01.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Motor01.h"
//#[ ignore
#define Default_Motor01_Motor01_SERIALIZE OM_NO_OP

#define Default_Motor01_off01_SERIALIZE OM_NO_OP

#define Default_Motor01_on01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Motor01
Motor01::Motor01() {
    NOTIFY_CONSTRUCTOR(Motor01, Motor01(), 0, Default_Motor01_Motor01_SERIALIZE);
}

Motor01::~Motor01() {
    NOTIFY_DESTRUCTOR(~Motor01, true);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedMotor01::serializeAttributes(AOMSAttributes* aomsAttributes) const {
}

void OMAnimatedMotor01::serializeRelations(AOMSRelations* aomsRelations) const {
}
//#]

IMPLEMENT_META_P(Motor01, Default, Default, false, OMAnimatedMotor01)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\AC_Sim01\Motor01.cpp
*********************************************************************/
