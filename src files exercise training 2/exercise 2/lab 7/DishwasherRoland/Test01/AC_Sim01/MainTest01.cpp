/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: AC_Sim01
	Model Element	: AC_Sim01
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: Test01\AC_Sim01\MainTest01.cpp
*********************************************************************/

//## auto_generated
#include "MainTest01.h"
//## auto_generated
#include "Default.h"
//## auto_generated
#include "AC_DishwasherBuilder01.h"
Test01::Test01() {
    Default_initRelations();
    Default_startBehavior();
}

int main(int argc, char* argv[]) {
    int status = 0;
    if(OXF::initialize(argc, argv, 6423, "", 0, 0, false))
        {
            AC_DishwasherBuilder01 * p_AC_DishwasherBuilder01;
            Test01 initializer_Test01;
            p_AC_DishwasherBuilder01 = new AC_DishwasherBuilder01;
            p_AC_DishwasherBuilder01->startBehavior();
            //#[ configuration Test01::AC_Sim01 
            //#]
            OXF::start();
            delete p_AC_DishwasherBuilder01;
            status = 0;
        }
    else
        {
            status = 1;
        }
    return status;
}

/*********************************************************************
	File Path	: Test01\AC_Sim01\MainTest01.cpp
*********************************************************************/
