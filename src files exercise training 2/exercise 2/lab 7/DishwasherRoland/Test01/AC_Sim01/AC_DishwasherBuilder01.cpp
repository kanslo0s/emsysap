/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: AC_Sim01
	Model Element	: AC_DishwasherBuilder01
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: Test01\AC_Sim01\AC_DishwasherBuilder01.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "AC_DishwasherBuilder01.h"
//#[ ignore
#define Default_AC_DishwasherBuilder01_AC_DishwasherBuilder01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class AC_DishwasherBuilder01
AC_DishwasherBuilder01::AC_DishwasherBuilder01(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(AC_DishwasherBuilder01, AC_DishwasherBuilder01(), 0, Default_AC_DishwasherBuilder01_AC_DishwasherBuilder01_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsDishWasherRoland.setShouldDelete(false);
        }
        {
            itsFrontPanel01.setShouldDelete(false);
        }
    }
    initRelations();
    initWebAdapters();
}

AC_DishwasherBuilder01::~AC_DishwasherBuilder01() {
    NOTIFY_DESTRUCTOR(~AC_DishwasherBuilder01, true);
    delete itsWebAdapter;
}

AC* AC_DishwasherBuilder01::getItsAC() const {
    return (AC*) &itsAC;
}

DishWasherRoland* AC_DishwasherBuilder01::getItsDishWasherRoland() const {
    return (DishWasherRoland*) &itsDishWasherRoland;
}

FrontPanel01* AC_DishwasherBuilder01::getItsFrontPanel01() const {
    return (FrontPanel01*) &itsFrontPanel01;
}

bool AC_DishwasherBuilder01::startBehavior() {
    bool done = true;
    done &= itsDishWasherRoland.startBehavior();
    done &= itsFrontPanel01.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void AC_DishwasherBuilder01::initRelations() {
    itsFrontPanel01.setItsDishWasherRoland(&itsDishWasherRoland);
    itsDishWasherRoland.setItsMotor01(&itsAC);
}

void AC_DishwasherBuilder01::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsDishWasherRoland.setActiveContext(theActiveContext, false);
        itsFrontPanel01.setActiveContext(theActiveContext, false);
    }
}

void AC_DishwasherBuilder01::destroy() {
    itsDishWasherRoland.destroy();
    itsFrontPanel01.destroy();
    OMReactive::destroy();
}

//#[ ignore
const ClassWebAdapter * AC_DishwasherBuilder01::getItsWebAdapter() const {
    return itsWebAdapter;
}

void AC_DishwasherBuilder01::visitWebAdaptedRelations() const {
    {
        const DishWasherRoland* itsClient = getItsDishWasherRoland();
        if (itsClient != NULL) {
            const ClassWebAdapter * peerAdapter = itsClient->getItsWebAdapter();
            if (peerAdapter != NULL) {
                itsWebAdapter->UpdateWebRelation(peerAdapter);
            }
        }
    }
    {
        const FrontPanel01* itsClient = getItsFrontPanel01();
        if (itsClient != NULL) {
            const ClassWebAdapter * peerAdapter = itsClient->getItsWebAdapter();
            if (peerAdapter != NULL) {
                itsWebAdapter->UpdateWebRelation(peerAdapter);
            }
        }
    }
}

void AC_DishwasherBuilder01::initWebAdapters() {
    itsWebAdapter = new ClassWebAdapterTmpl< AC_DishwasherBuilder01 >(this, "AC_DishwasherBuilder01");
}

void AC_DishwasherBuilder01::notifyWebRelationModified() const {
    ClassWebAdapter::NotifyWebRelationModified();
}
//#]

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedAC_DishwasherBuilder01::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsAC", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsAC);
    aomsRelations->addRelation("itsDishWasherRoland", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDishWasherRoland);
    aomsRelations->addRelation("itsFrontPanel01", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsFrontPanel01);
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(AC_DishwasherBuilder01, Default, Default, false, OMAnimatedAC_DishwasherBuilder01)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\AC_Sim01\AC_DishwasherBuilder01.cpp
*********************************************************************/
