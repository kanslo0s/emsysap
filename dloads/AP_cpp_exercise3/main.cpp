#include "GradeBook.h"
#include "Student.h"
#include <iostream>
using namespace std;

void display(Student** tempArray, int);

int main() {

	static const int numStudents = 5;
	string studentNames[numStudents] = { "jan", "piet" , "klaas", "henk", "mohammed" }; 

	Student* gradeArray[Student::numGrades];

	for (int i = 0; i < numStudents; i++) {
		double randomGrades[Student::numGrades] = { rand() % 10 + 1, rand() % 10 + 1, rand() % 10 + 1 };
		gradeArray[i] = new Student(studentNames[i], randomGrades);
	}

	GradeBook gradeBook1("Programming Book", "Teacher");
	gradeBook1.displayMessage();
	gradeBook1.sortAverage(gradeArray, Student::numGrades);
	display(gradeArray, Student::numGrades);

	while (1) {

	}
}

void display(Student** tempArray, int size) {
	for (int i = 0; 0 < size; i++) {
		cout << tempArray[i]->getStudentName() << "\t\t" << tempArray[i]->calculateAverage() << endl;
	}
}
