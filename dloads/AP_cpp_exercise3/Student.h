#include <string>
using namespace std;

class Student {
public:
	static const int numGrades = 3;

	Student(string, const double []);
	double calculateAverage();
	double grades[numGrades];
	string getStudentName();
	void gradeMatrix();

private:
	string studentName;
	double average;
	
};
