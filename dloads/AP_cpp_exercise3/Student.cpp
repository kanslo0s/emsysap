#include <iostream>
#include "Student.h"
using namespace std;

Student::Student(string name, const double a[]) {
	studentName = name;

	for (int i = 0; i < numGrades; i++)
		grades[i] = a[i];
}

double Student::calculateAverage() {
	double total = 0;
	for (auto& num : grades)
		total += num;
	average = total / numGrades;
	return average;
}

string Student::getStudentName() {
	return studentName;
}

void Student::gradeMatrix() {

}