#include <string>
using namespace std;

class GradeBook {
public:
	GradeBook(string, string);
	void setCourseName(string);
	void setInstructorName(string);
	string getInstructorName();
	string getCourseName();
	void displayMessage();
	int sortAverage(Student* [], int);
private:
	string courseName;
	string instructorName;
};
