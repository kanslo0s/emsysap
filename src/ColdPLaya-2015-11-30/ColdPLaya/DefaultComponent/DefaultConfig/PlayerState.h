/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: PlayerState
//!	Generated Date	: Fri, 11, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\PlayerState.h
*********************************************************************/

#ifndef PlayerState_H
#define PlayerState_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "PlayerStateMachine.h"
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include <oxf\omthread.h>
//## attribute CurrentSongPtr
class Song;

//## link itsBASS_Controller
class BASS_Controller;

//## package PlayerStateMachine

//## class PlayerState
class PlayerState : public OMReactive {
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPlayerState;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation PlayerState()
    PlayerState(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~PlayerState();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getCurrentPlaylist() const;
    
    //## auto_generated
    void setCurrentPlaylist(int p_CurrentPlaylist);
    
    //## auto_generated
    BASS_Controller* getItsBASS_Controller() const;
    
    //## auto_generated
    void setItsBASS_Controller(BASS_Controller* p_BASS_Controller);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int CurrentPlaylist;		//## attribute CurrentPlaylist
    
    Song* CurrentSongPtr;		//## attribute CurrentSongPtr
    
    ////    Relations and components    ////
    
    BASS_Controller* itsBASS_Controller;		//## link itsBASS_Controller
    
    ////    Framework operations    ////
    
    ////    Framework    ////

public :

    //## auto_generated
    int getCurrentSongInt() const;
    
    //## auto_generated
    void setCurrentSongInt(int p_CurrentSongInt);
    
    //## auto_generated
    Song* getCurrentSongPtr() const;
    
    //## auto_generated
    void setCurrentSongPtr(Song* p_CurrentSongPtr);

protected :

    int CurrentSongInt;		//## attribute CurrentSongInt

public :

    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    void rootStateEntDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // State:
    //## statechart_method
    inline bool State_IN() const;
    
    //## statechart_method
    void State_entDef();
    
    //## statechart_method
    void State_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus State_processEvent();
    
    // SMVolume:
    //## statechart_method
    inline bool SMVolume_IN() const;
    
    //## statechart_method
    void SMVolume_entDef();
    
    //## statechart_method
    void SMVolume_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus SMVolume_processEvent();
    
    // idle:
    //## statechart_method
    inline bool idle_IN() const;
    
    // Shuffle:
    //## statechart_method
    inline bool Shuffle_IN() const;
    
    //## statechart_method
    void Shuffle_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Shuffle_processEvent();
    
    // shuffle_on:
    //## statechart_method
    inline bool shuffle_on_IN() const;
    
    // shuffle_off:
    //## statechart_method
    inline bool shuffle_off_IN() const;
    
    // Repeat:
    //## statechart_method
    inline bool Repeat_IN() const;
    
    //## statechart_method
    void Repeat_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Repeat_processEvent();
    
    // oneRepeat:
    //## statechart_method
    inline bool oneRepeat_IN() const;
    
    // noRepeat:
    //## statechart_method
    inline bool noRepeat_IN() const;
    
    // allRepeat:
    //## statechart_method
    inline bool allRepeat_IN() const;
    
    // Playa:
    //## statechart_method
    inline bool Playa_IN() const;
    
    //## statechart_method
    void Playa_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Playa_processEvent();
    
    // Playing:
    //## statechart_method
    inline bool Playing_IN() const;
    
    // Paused:
    //## statechart_method
    inline bool Paused_IN() const;
    
    // Idle:
    //## statechart_method
    inline bool Idle_IN() const;

protected :

//#[ ignore
    enum PlayerState_Enum {
        OMNonState = 0,
        State = 1,
        SMVolume = 2,
        idle = 3,
        Shuffle = 4,
        shuffle_on = 5,
        shuffle_off = 6,
        Repeat = 7,
        oneRepeat = 8,
        noRepeat = 9,
        allRepeat = 10,
        Playa = 11,
        Playing = 12,
        Paused = 13,
        Idle = 14
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int SMVolume_subState;
    
    int SMVolume_active;
    
    int Shuffle_subState;
    
    int Shuffle_active;
    
    int Repeat_subState;
    
    int Repeat_active;
    
    int Playa_subState;
    
    int Playa_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPlayerState : virtual public AOMInstance {
    DECLARE_REACTIVE_META(PlayerState, OMAnimatedPlayerState)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void State_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void SMVolume_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void idle_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Shuffle_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void shuffle_on_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void shuffle_off_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Repeat_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void oneRepeat_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void noRepeat_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void allRepeat_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Playa_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Playing_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Paused_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Idle_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool PlayerState::rootState_IN() const {
    return true;
}

inline bool PlayerState::State_IN() const {
    return rootState_subState == State;
}

inline bool PlayerState::SMVolume_IN() const {
    return State_IN();
}

inline bool PlayerState::idle_IN() const {
    return SMVolume_subState == idle;
}

inline bool PlayerState::Shuffle_IN() const {
    return State_IN();
}

inline bool PlayerState::shuffle_on_IN() const {
    return Shuffle_subState == shuffle_on;
}

inline bool PlayerState::shuffle_off_IN() const {
    return Shuffle_subState == shuffle_off;
}

inline bool PlayerState::Repeat_IN() const {
    return State_IN();
}

inline bool PlayerState::oneRepeat_IN() const {
    return Repeat_subState == oneRepeat;
}

inline bool PlayerState::noRepeat_IN() const {
    return Repeat_subState == noRepeat;
}

inline bool PlayerState::allRepeat_IN() const {
    return Repeat_subState == allRepeat;
}

inline bool PlayerState::Playa_IN() const {
    return State_IN();
}

inline bool PlayerState::Playing_IN() const {
    return Playa_subState == Playing;
}

inline bool PlayerState::Paused_IN() const {
    return Playa_subState == Paused;
}

inline bool PlayerState::Idle_IN() const {
    return Playa_subState == Idle;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\PlayerState.h
*********************************************************************/
