/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Album
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Album.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Album.h"
//## link itsArtist
#include "Artist.h"
//#[ ignore
#define Library_Album_Album_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class Album
Album::Album() {
    NOTIFY_CONSTRUCTOR(Album, Album(), 0, Library_Album_Album_SERIALIZE);
    //#[ operation Album()
    //#]
}

Album::~Album() {
    NOTIFY_DESTRUCTOR(~Album, true);
    cleanUpRelations();
}

RhpString Album::getAlbumTitle() const {
    return albumTitle;
}

void Album::setAlbumTitle(RhpString p_albumTitle) {
    albumTitle = p_albumTitle;
}

OMIterator<Artist*> Album::getItsArtist() const {
    OMIterator<Artist*> iter(itsArtist);
    return iter;
}

void Album::addItsArtist(Artist* p_Artist) {
    if(p_Artist != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsArtist", p_Artist, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsArtist");
        }
    itsArtist.add(p_Artist);
}

void Album::removeItsArtist(Artist* p_Artist) {
    NOTIFY_RELATION_ITEM_REMOVED("itsArtist", p_Artist);
    itsArtist.remove(p_Artist);
}

void Album::clearItsArtist() {
    NOTIFY_RELATION_CLEARED("itsArtist");
    itsArtist.removeAll();
}

void Album::cleanUpRelations() {
    {
        itsArtist.removeAll();
    }
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedAlbum::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("albumTitle", x2String(myReal->albumTitle));
}

void OMAnimatedAlbum::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsArtist", false, false);
    {
        OMIterator<Artist*> iter(myReal->itsArtist);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
}
//#]

IMPLEMENT_META_P(Album, Library, Library, false, OMAnimatedAlbum)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Album.cpp
*********************************************************************/
