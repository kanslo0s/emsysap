/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_1
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\class_1.h
*********************************************************************/

#ifndef class_1_H
#define class_1_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Library.h"
//## package Library

//## class class_1
class class_1 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedclass_1;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    class_1();
    
    //## auto_generated
    ~class_1();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedclass_1 : virtual public AOMInstance {
    DECLARE_META(class_1, OMAnimatedclass_1)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_1.h
*********************************************************************/
