/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_0
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\class_0.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_0.h"
//#[ ignore
#define Library_class_0_class_0_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class class_0
class_0::class_0() {
    NOTIFY_CONSTRUCTOR(class_0, class_0(), 0, Library_class_0_class_0_SERIALIZE);
}

class_0::~class_0() {
    NOTIFY_DESTRUCTOR(~class_0, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_0, Library, Library, false, OMAnimatedclass_0)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_0.cpp
*********************************************************************/
