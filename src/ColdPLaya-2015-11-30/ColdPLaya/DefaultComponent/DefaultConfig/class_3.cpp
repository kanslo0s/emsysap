/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_3
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\class_3.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_3.h"
//#[ ignore
#define Library_class_3_class_3_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class class_3
class_3::class_3() {
    NOTIFY_CONSTRUCTOR(class_3, class_3(), 0, Library_class_3_class_3_SERIALIZE);
}

class_3::~class_3() {
    NOTIFY_DESTRUCTOR(~class_3, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_3, Library, Library, false, OMAnimatedclass_3)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_3.cpp
*********************************************************************/
