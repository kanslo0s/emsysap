/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_2
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\class_2.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_2.h"
//#[ ignore
#define Library_class_2_class_2_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class class_2
class_2::class_2() {
    NOTIFY_CONSTRUCTOR(class_2, class_2(), 0, Library_class_2_class_2_SERIALIZE);
}

class_2::~class_2() {
    NOTIFY_DESTRUCTOR(~class_2, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_2, Library, Library, false, OMAnimatedclass_2)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_2.cpp
*********************************************************************/
