/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: PlayerStateMachine
//!	Generated Date	: Tue, 1, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\PlayerStateMachine.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "PlayerStateMachine.h"
//## auto_generated
#include "PlayerState.h"
//#[ ignore
#define event_1_SERIALIZE OM_NO_OP

#define event_1_UNSERIALIZE OM_NO_OP

#define event_1_CONSTRUCTOR event_1()

#define Paused_SERIALIZE OM_NO_OP

#define Paused_UNSERIALIZE OM_NO_OP

#define Paused_CONSTRUCTOR Paused()

#define Playing_SERIALIZE OM_NO_OP

#define Playing_UNSERIALIZE OM_NO_OP

#define Playing_CONSTRUCTOR Playing()

#define evPaused_SERIALIZE OM_NO_OP

#define evPaused_UNSERIALIZE OM_NO_OP

#define evPaused_CONSTRUCTOR evPaused()

#define evPlaying_SERIALIZE OM_NO_OP

#define evPlaying_UNSERIALIZE OM_NO_OP

#define evPlaying_CONSTRUCTOR evPlaying()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evOpen_SERIALIZE OM_NO_OP

#define evOpen_UNSERIALIZE OM_NO_OP

#define evOpen_CONSTRUCTOR evOpen()

#define evRepeat_SERIALIZE OM_NO_OP

#define evRepeat_UNSERIALIZE OM_NO_OP

#define evRepeat_CONSTRUCTOR evRepeat()

#define evShuffle_SERIALIZE OM_NO_OP

#define evShuffle_UNSERIALIZE OM_NO_OP

#define evShuffle_CONSTRUCTOR evShuffle()

#define evVolUp_SERIALIZE OM_NO_OP

#define evVolUp_UNSERIALIZE OM_NO_OP

#define evVolUp_CONSTRUCTOR evVolUp()

#define evVolDown_SERIALIZE OM_NO_OP

#define evVolDown_UNSERIALIZE OM_NO_OP

#define evVolDown_CONSTRUCTOR evVolDown()
//#]

//## package PlayerStateMachine


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(PlayerStateMachine, PlayerStateMachine)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event event_1()
event_1::event_1() {
    NOTIFY_EVENT_CONSTRUCTOR(event_1)
    setId(event_1_PlayerStateMachine_id);
}

bool event_1::isTypeOf(const short id) const {
    return (event_1_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(event_1, PlayerStateMachine, PlayerStateMachine, event_1())

//## event Paused()
Paused::Paused() {
    NOTIFY_EVENT_CONSTRUCTOR(Paused)
    setId(Paused_PlayerStateMachine_id);
}

bool Paused::isTypeOf(const short id) const {
    return (Paused_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(Paused, PlayerStateMachine, PlayerStateMachine, Paused())

//## event Playing()
Playing::Playing() {
    NOTIFY_EVENT_CONSTRUCTOR(Playing)
    setId(Playing_PlayerStateMachine_id);
}

bool Playing::isTypeOf(const short id) const {
    return (Playing_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(Playing, PlayerStateMachine, PlayerStateMachine, Playing())

//## event evPaused()
evPaused::evPaused() {
    NOTIFY_EVENT_CONSTRUCTOR(evPaused)
    setId(evPaused_PlayerStateMachine_id);
}

bool evPaused::isTypeOf(const short id) const {
    return (evPaused_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evPaused, PlayerStateMachine, PlayerStateMachine, evPaused())

//## event evPlaying()
evPlaying::evPlaying() {
    NOTIFY_EVENT_CONSTRUCTOR(evPlaying)
    setId(evPlaying_PlayerStateMachine_id);
}

bool evPlaying::isTypeOf(const short id) const {
    return (evPlaying_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evPlaying, PlayerStateMachine, PlayerStateMachine, evPlaying())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_PlayerStateMachine_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, PlayerStateMachine, PlayerStateMachine, evStop())

//## event evOpen()
evOpen::evOpen() {
    NOTIFY_EVENT_CONSTRUCTOR(evOpen)
    setId(evOpen_PlayerStateMachine_id);
}

bool evOpen::isTypeOf(const short id) const {
    return (evOpen_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evOpen, PlayerStateMachine, PlayerStateMachine, evOpen())

//## event evRepeat()
evRepeat::evRepeat() {
    NOTIFY_EVENT_CONSTRUCTOR(evRepeat)
    setId(evRepeat_PlayerStateMachine_id);
}

bool evRepeat::isTypeOf(const short id) const {
    return (evRepeat_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evRepeat, PlayerStateMachine, PlayerStateMachine, evRepeat())

//## event evShuffle()
evShuffle::evShuffle() {
    NOTIFY_EVENT_CONSTRUCTOR(evShuffle)
    setId(evShuffle_PlayerStateMachine_id);
}

bool evShuffle::isTypeOf(const short id) const {
    return (evShuffle_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evShuffle, PlayerStateMachine, PlayerStateMachine, evShuffle())

//## event evVolUp()
evVolUp::evVolUp() {
    NOTIFY_EVENT_CONSTRUCTOR(evVolUp)
    setId(evVolUp_PlayerStateMachine_id);
}

bool evVolUp::isTypeOf(const short id) const {
    return (evVolUp_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evVolUp, PlayerStateMachine, PlayerStateMachine, evVolUp())

//## event evVolDown()
evVolDown::evVolDown() {
    NOTIFY_EVENT_CONSTRUCTOR(evVolDown)
    setId(evVolDown_PlayerStateMachine_id);
}

bool evVolDown::isTypeOf(const short id) const {
    return (evVolDown_PlayerStateMachine_id==id);
}

IMPLEMENT_META_EVENT_P(evVolDown, PlayerStateMachine, PlayerStateMachine, evVolDown())

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\PlayerStateMachine.cpp
*********************************************************************/
