/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Library
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Library.h
*********************************************************************/

#ifndef Library_H
#define Library_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
class Album;

//## auto_generated
class Artist;

//## auto_generated
class Library;

//## auto_generated
class Playlist;

//## auto_generated
class Song;

//## package Library

//## class Library
class Library {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedLibrary;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation Library()
    Library();
    
    //## auto_generated
    ~Library();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedLibrary : virtual public AOMInstance {
    DECLARE_META(Library, OMAnimatedLibrary)
};
//#]
#endif // _OMINSTRUMENT

//## package Library



#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Library.h
*********************************************************************/
