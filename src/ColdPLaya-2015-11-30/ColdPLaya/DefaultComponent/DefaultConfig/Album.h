/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Album
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Album.h
*********************************************************************/

#ifndef Album_H
#define Album_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Library.h"
//## auto_generated
#include <oxf\omcollec.h>
//## link itsArtist
class Artist;

//## package Library

//## class Album
class Album {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedAlbum;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation Album()
    Album();
    
    //## auto_generated
    ~Album();
    
    ////    Additional operations    ////
    
    //## auto_generated
    RhpString getAlbumTitle() const;
    
    //## auto_generated
    void setAlbumTitle(RhpString p_albumTitle);
    
    //## auto_generated
    OMIterator<Artist*> getItsArtist() const;
    
    //## auto_generated
    void addItsArtist(Artist* p_Artist);
    
    //## auto_generated
    void removeItsArtist(Artist* p_Artist);
    
    //## auto_generated
    void clearItsArtist();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    RhpString albumTitle;		//## attribute albumTitle
    
    ////    Relations and components    ////
    
    OMCollection<Artist*> itsArtist;		//## link itsArtist
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedAlbum : virtual public AOMInstance {
    DECLARE_META(Album, OMAnimatedAlbum)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Album.h
*********************************************************************/
