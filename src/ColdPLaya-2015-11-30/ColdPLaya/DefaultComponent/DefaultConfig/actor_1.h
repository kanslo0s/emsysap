/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: actor_1
//!	Generated Date	: Mon, 30, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\actor_1.h
*********************************************************************/

#ifndef actor_1_H
#define actor_1_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "C:\Users\Derek\Desktop\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Default.h"
//## package Default

//## actor actor_1
class actor_1 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedactor_1;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    actor_1();
    
    //## auto_generated
    ~actor_1();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedactor_1 : virtual public AOMInstance {
    DECLARE_META(actor_1, OMAnimatedactor_1)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\actor_1.h
*********************************************************************/
