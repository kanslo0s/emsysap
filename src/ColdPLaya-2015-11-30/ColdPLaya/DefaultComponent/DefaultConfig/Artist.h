/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Artist
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Artist.h
*********************************************************************/

#ifndef Artist_H
#define Artist_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Library.h"
//## auto_generated
#include <oxf\omcollec.h>
//## link itsLibrary
class Library;

//## package Library

//## class Artist
class Artist {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedArtist;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation Artist()
    Artist();
    
    //## auto_generated
    ~Artist();
    
    ////    Additional operations    ////
    
    //## auto_generated
    RhpString getName() const;
    
    //## auto_generated
    void setName(RhpString p_name);
    
    //## auto_generated
    OMIterator<Library*> getItsLibrary() const;
    
    //## auto_generated
    void addItsLibrary(Library* p_Library);
    
    //## auto_generated
    void removeItsLibrary(Library* p_Library);
    
    //## auto_generated
    void clearItsLibrary();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    RhpString name;		//## attribute name
    
    ////    Relations and components    ////
    
    OMCollection<Library*> itsLibrary;		//## link itsLibrary
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedArtist : virtual public AOMInstance {
    DECLARE_META(Artist, OMAnimatedArtist)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Artist.h
*********************************************************************/
