/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: BASS_Controller
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\BASS_Controller.h
*********************************************************************/

#ifndef BASS_Controller_H
#define BASS_Controller_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "MusicStream.h"
//## package MusicStream

//## class BASS_Controller
class BASS_Controller {
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedBASS_Controller;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation BASS_Controller()
    BASS_Controller();
    
    //## operation ~BASS_Controller()
    ~BASS_Controller();
    
    ////    Operations    ////
    
    //## operation OpenFile()
    void OpenFile();
    
    //## operation Pause()
    void Pause();
    
    //## operation Play()
    void Play();
    
    //## operation StopPlaying()
    void StopPlaying();

private :

    //## auto_generated
    HSTREAM getStreamHandle() const;
    
    //## auto_generated
    void setStreamHandle(HSTREAM p_streamHandle);

protected :

    HSTREAM streamHandle;		//## attribute streamHandle

public :

    //## operation VolumeDown()
    void VolumeDown();
    
    //## operation VolumeUp()
    void VolumeUp();

private :

    //## auto_generated
    float getVolume() const;
    
    //## auto_generated
    void setVolume(float p_Volume);

protected :

    float Volume;		//## attribute Volume

private :

    //## auto_generated
    int getRepeating() const;
    
    //## auto_generated
    void setRepeating(int p_Repeating);
    
    //## auto_generated
    bool getShuffling() const;
    
    //## auto_generated
    void setShuffling(bool p_Shuffling);

protected :

    int Repeating;		//## attribute Repeating
    
    bool Shuffling;		//## attribute Shuffling
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedBASS_Controller : virtual public AOMInstance {
    DECLARE_META(BASS_Controller, OMAnimatedBASS_Controller)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\BASS_Controller.h
*********************************************************************/
