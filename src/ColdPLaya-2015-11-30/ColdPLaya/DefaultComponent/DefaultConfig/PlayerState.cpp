/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: PlayerState
//!	Generated Date	: Fri, 11, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\PlayerState.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "PlayerState.h"
//## link itsBASS_Controller
#include "BASS_Controller.h"
//## attribute CurrentSongPtr
#include "Song.h"
//#[ ignore
#define PlayerStateMachine_PlayerState_PlayerState_SERIALIZE OM_NO_OP
//#]

//## package PlayerStateMachine

//## class PlayerState
PlayerState::PlayerState(IOxfActive* theActiveContext) : itsBASS_Controller (new BASS_Controller()) {
    NOTIFY_REACTIVE_CONSTRUCTOR(PlayerState, PlayerState(), 0, PlayerStateMachine_PlayerState_PlayerState_SERIALIZE);
    setActiveContext(theActiveContext, false);
    initStatechart();
    //#[ operation PlayerState()
    //#]
}

PlayerState::~PlayerState() {
    NOTIFY_DESTRUCTOR(~PlayerState, true);
    cleanUpRelations();
}

int PlayerState::getCurrentPlaylist() const {
    return CurrentPlaylist;
}

void PlayerState::setCurrentPlaylist(int p_CurrentPlaylist) {
    CurrentPlaylist = p_CurrentPlaylist;
}

BASS_Controller* PlayerState::getItsBASS_Controller() const {
    return itsBASS_Controller;
}

void PlayerState::setItsBASS_Controller(BASS_Controller* p_BASS_Controller) {
    itsBASS_Controller = p_BASS_Controller;
    if(p_BASS_Controller != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsBASS_Controller", p_BASS_Controller, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsBASS_Controller");
        }
}

bool PlayerState::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void PlayerState::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    SMVolume_subState = OMNonState;
    SMVolume_active = OMNonState;
    Shuffle_subState = OMNonState;
    Shuffle_active = OMNonState;
    Repeat_subState = OMNonState;
    Repeat_active = OMNonState;
    Playa_subState = OMNonState;
    Playa_active = OMNonState;
}

void PlayerState::cleanUpRelations() {
    if(itsBASS_Controller != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsBASS_Controller");
            itsBASS_Controller = NULL;
        }
}

int PlayerState::getCurrentSongInt() const {
    return CurrentSongInt;
}

void PlayerState::setCurrentSongInt(int p_CurrentSongInt) {
    CurrentSongInt = p_CurrentSongInt;
}

Song* PlayerState::getCurrentSongPtr() const {
    return CurrentSongPtr;
}

void PlayerState::setCurrentSongPtr(Song* p_CurrentSongPtr) {
    CurrentSongPtr = p_CurrentSongPtr;
}

void PlayerState::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        rootStateEntDef();
    }
}

void PlayerState::rootStateEntDef() {
    State_entDef();
}

IOxfReactive::TakeEventStatus PlayerState::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State State
    if(rootState_active == State)
        {
            res = State_processEvent();
        }
    return res;
}

void PlayerState::State_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.State");
    rootState_subState = State;
    rootState_active = State;
    Playa_entDef();
    Shuffle_entDef();
    Repeat_entDef();
    SMVolume_entDef();
}

void PlayerState::State_exit() {
    switch (Playa_subState) {
        // State Playing
        case Playing:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Playa.Playing");
        }
        break;
        // State Paused
        case Paused:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Playa.Paused");
        }
        break;
        // State Idle
        case Idle:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Playa.Idle");
        }
        break;
        default:
            break;
    }
    Playa_subState = OMNonState;
    NOTIFY_STATE_EXITED("ROOT.State.Playa");
    switch (Shuffle_subState) {
        // State shuffle_on
        case shuffle_on:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Shuffle.shuffle_on");
        }
        break;
        // State shuffle_off
        case shuffle_off:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Shuffle.shuffle_off");
        }
        break;
        default:
            break;
    }
    Shuffle_subState = OMNonState;
    NOTIFY_STATE_EXITED("ROOT.State.Shuffle");
    switch (Repeat_subState) {
        // State noRepeat
        case noRepeat:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Repeat.noRepeat");
        }
        break;
        // State oneRepeat
        case oneRepeat:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Repeat.oneRepeat");
        }
        break;
        // State allRepeat
        case allRepeat:
        {
            NOTIFY_STATE_EXITED("ROOT.State.Repeat.allRepeat");
        }
        break;
        default:
            break;
    }
    Repeat_subState = OMNonState;
    NOTIFY_STATE_EXITED("ROOT.State.Repeat");
    SMVolume_exit();
    
    NOTIFY_STATE_EXITED("ROOT.State");
}

IOxfReactive::TakeEventStatus PlayerState::State_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State Playa
    if(Playa_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(State))
                {
                    return res;
                }
        }
    // State Shuffle
    if(Shuffle_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(State))
                {
                    return res;
                }
        }
    // State Repeat
    if(Repeat_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(State))
                {
                    return res;
                }
        }
    // State SMVolume
    if(SMVolume_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(State))
                {
                    return res;
                }
        }
    
    return res;
}

void PlayerState::SMVolume_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.State.SMVolume");
    NOTIFY_TRANSITION_STARTED("15");
    NOTIFY_STATE_ENTERED("ROOT.State.SMVolume.idle");
    SMVolume_subState = idle;
    SMVolume_active = idle;
    NOTIFY_TRANSITION_TERMINATED("15");
}

void PlayerState::SMVolume_exit() {
    // State idle
    if(SMVolume_subState == idle)
        {
            NOTIFY_STATE_EXITED("ROOT.State.SMVolume.idle");
        }
    SMVolume_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.State.SMVolume");
}

IOxfReactive::TakeEventStatus PlayerState::SMVolume_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State idle
    if(SMVolume_active == idle)
        {
            if(IS_EVENT_TYPE_OF(evVolDown_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("14");
                    NOTIFY_STATE_EXITED("ROOT.State.SMVolume.idle");
                    //#[ transition 14 
                    itsBASS_Controller->VolumeDown();
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.State.SMVolume.idle");
                    SMVolume_subState = idle;
                    SMVolume_active = idle;
                    NOTIFY_TRANSITION_TERMINATED("14");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evVolUp_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("13");
                    NOTIFY_STATE_EXITED("ROOT.State.SMVolume.idle");
                    //#[ transition 13 
                    itsBASS_Controller->VolumeUp();
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.State.SMVolume.idle");
                    SMVolume_subState = idle;
                    SMVolume_active = idle;
                    NOTIFY_TRANSITION_TERMINATED("13");
                    res = eventConsumed;
                }
            
            
        }
    return res;
}

void PlayerState::Shuffle_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.State.Shuffle");
    NOTIFY_TRANSITION_STARTED("9");
    NOTIFY_STATE_ENTERED("ROOT.State.Shuffle.shuffle_off");
    Shuffle_subState = shuffle_off;
    Shuffle_active = shuffle_off;
    NOTIFY_TRANSITION_TERMINATED("9");
}

IOxfReactive::TakeEventStatus PlayerState::Shuffle_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (Shuffle_active) {
        // State shuffle_on
        case shuffle_on:
        {
            if(IS_EVENT_TYPE_OF(evShuffle_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_STATE_EXITED("ROOT.State.Shuffle.shuffle_on");
                    NOTIFY_STATE_ENTERED("ROOT.State.Shuffle.shuffle_off");
                    Shuffle_subState = shuffle_off;
                    Shuffle_active = shuffle_off;
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State shuffle_off
        case shuffle_off:
        {
            if(IS_EVENT_TYPE_OF(evShuffle_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    NOTIFY_STATE_EXITED("ROOT.State.Shuffle.shuffle_off");
                    NOTIFY_STATE_ENTERED("ROOT.State.Shuffle.shuffle_on");
                    Shuffle_subState = shuffle_on;
                    Shuffle_active = shuffle_on;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

void PlayerState::Repeat_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.State.Repeat");
    NOTIFY_TRANSITION_STARTED("10");
    NOTIFY_STATE_ENTERED("ROOT.State.Repeat.noRepeat");
    Repeat_subState = noRepeat;
    Repeat_active = noRepeat;
    NOTIFY_TRANSITION_TERMINATED("10");
}

IOxfReactive::TakeEventStatus PlayerState::Repeat_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (Repeat_active) {
        // State noRepeat
        case noRepeat:
        {
            if(IS_EVENT_TYPE_OF(evRepeat_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    NOTIFY_STATE_EXITED("ROOT.State.Repeat.noRepeat");
                    NOTIFY_STATE_ENTERED("ROOT.State.Repeat.oneRepeat");
                    Repeat_subState = oneRepeat;
                    Repeat_active = oneRepeat;
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State oneRepeat
        case oneRepeat:
        {
            if(IS_EVENT_TYPE_OF(evRepeat_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    NOTIFY_STATE_EXITED("ROOT.State.Repeat.oneRepeat");
                    NOTIFY_STATE_ENTERED("ROOT.State.Repeat.allRepeat");
                    Repeat_subState = allRepeat;
                    Repeat_active = allRepeat;
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State allRepeat
        case allRepeat:
        {
            if(IS_EVENT_TYPE_OF(evRepeat_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("11");
                    NOTIFY_STATE_EXITED("ROOT.State.Repeat.allRepeat");
                    NOTIFY_STATE_ENTERED("ROOT.State.Repeat.noRepeat");
                    Repeat_subState = noRepeat;
                    Repeat_active = noRepeat;
                    NOTIFY_TRANSITION_TERMINATED("11");
                    res = eventConsumed;
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

void PlayerState::Playa_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.State.Playa");
    NOTIFY_TRANSITION_STARTED("0");
    NOTIFY_STATE_ENTERED("ROOT.State.Playa.Idle");
    Playa_subState = Idle;
    Playa_active = Idle;
    NOTIFY_TRANSITION_TERMINATED("0");
}

IOxfReactive::TakeEventStatus PlayerState::Playa_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (Playa_active) {
        // State Playing
        case Playing:
        {
            if(IS_EVENT_TYPE_OF(evStop_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.State.Playa.Playing");
                    //#[ transition 2 
                    itsBASS_Controller->StopPlaying();
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.State.Playa.Idle");
                    Playa_subState = Idle;
                    Playa_active = Idle;
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evPaused_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    NOTIFY_STATE_EXITED("ROOT.State.Playa.Playing");
                    //#[ transition 7 
                    itsBASS_Controller->Pause();
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.State.Playa.Paused");
                    Playa_subState = Paused;
                    Playa_active = Paused;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State Paused
        case Paused:
        {
            if(IS_EVENT_TYPE_OF(evStop_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("12");
                    NOTIFY_STATE_EXITED("ROOT.State.Playa.Paused");
                    //#[ transition 12 
                    itsBASS_Controller->StopPlaying();
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.State.Playa.Idle");
                    Playa_subState = Idle;
                    Playa_active = Idle;
                    NOTIFY_TRANSITION_TERMINATED("12");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evPlaying_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    NOTIFY_STATE_EXITED("ROOT.State.Playa.Paused");
                    //#[ transition 8 
                    itsBASS_Controller->Play();
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.State.Playa.Playing");
                    Playa_subState = Playing;
                    Playa_active = Playing;
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State Idle
        case Idle:
        {
            if(IS_EVENT_TYPE_OF(evOpen_PlayerStateMachine_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.State.Playa.Idle");
                    //#[ transition 1 
                    itsBASS_Controller->OpenFile();
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.State.Playa.Playing");
                    Playa_subState = Playing;
                    Playa_active = Playing;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPlayerState::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("CurrentPlaylist", x2String(myReal->CurrentPlaylist));
    aomsAttributes->addAttribute("CurrentSongPtr", X2ITEM(myReal->CurrentSongPtr));
    aomsAttributes->addAttribute("CurrentSongInt", x2String(myReal->CurrentSongInt));
}

void OMAnimatedPlayerState::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsBASS_Controller", false, true);
    if(myReal->itsBASS_Controller)
        {
            aomsRelations->ADD_ITEM(myReal->itsBASS_Controller);
        }
}

void OMAnimatedPlayerState::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    if(myReal->rootState_subState == PlayerState::State)
        {
            State_serializeStates(aomsState);
        }
}

void OMAnimatedPlayerState::State_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State");
    Playa_serializeStates(aomsState);
    Shuffle_serializeStates(aomsState);
    Repeat_serializeStates(aomsState);
    SMVolume_serializeStates(aomsState);
}

void OMAnimatedPlayerState::SMVolume_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.SMVolume");
    if(myReal->SMVolume_subState == PlayerState::idle)
        {
            idle_serializeStates(aomsState);
        }
}

void OMAnimatedPlayerState::idle_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.SMVolume.idle");
}

void OMAnimatedPlayerState::Shuffle_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Shuffle");
    switch (myReal->Shuffle_subState) {
        case PlayerState::shuffle_on:
        {
            shuffle_on_serializeStates(aomsState);
        }
        break;
        case PlayerState::shuffle_off:
        {
            shuffle_off_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPlayerState::shuffle_on_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Shuffle.shuffle_on");
}

void OMAnimatedPlayerState::shuffle_off_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Shuffle.shuffle_off");
}

void OMAnimatedPlayerState::Repeat_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Repeat");
    switch (myReal->Repeat_subState) {
        case PlayerState::noRepeat:
        {
            noRepeat_serializeStates(aomsState);
        }
        break;
        case PlayerState::oneRepeat:
        {
            oneRepeat_serializeStates(aomsState);
        }
        break;
        case PlayerState::allRepeat:
        {
            allRepeat_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPlayerState::oneRepeat_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Repeat.oneRepeat");
}

void OMAnimatedPlayerState::noRepeat_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Repeat.noRepeat");
}

void OMAnimatedPlayerState::allRepeat_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Repeat.allRepeat");
}

void OMAnimatedPlayerState::Playa_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Playa");
    switch (myReal->Playa_subState) {
        case PlayerState::Playing:
        {
            Playing_serializeStates(aomsState);
        }
        break;
        case PlayerState::Paused:
        {
            Paused_serializeStates(aomsState);
        }
        break;
        case PlayerState::Idle:
        {
            Idle_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPlayerState::Playing_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Playa.Playing");
}

void OMAnimatedPlayerState::Paused_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Playa.Paused");
}

void OMAnimatedPlayerState::Idle_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.State.Playa.Idle");
}
//#]

IMPLEMENT_REACTIVE_META_P(PlayerState, PlayerStateMachine, PlayerStateMachine, false, OMAnimatedPlayerState)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\PlayerState.cpp
*********************************************************************/
