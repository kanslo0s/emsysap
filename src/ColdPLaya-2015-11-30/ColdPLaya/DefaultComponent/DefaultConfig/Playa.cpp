/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Playa
//!	Generated Date	: Mon, 30, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Playa.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Playa.h"
//#[ ignore
#define Default_Playa_Playa_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor Playa
Playa::Playa() {
    NOTIFY_CONSTRUCTOR(Playa, Playa(), 0, Default_Playa_Playa_SERIALIZE);
}

Playa::~Playa() {
    NOTIFY_DESTRUCTOR(~Playa, true);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPlaya::serializeRelations(AOMSRelations* aomsRelations) const {
}
//#]

IMPLEMENT_META_P(Playa, Default, Default, false, OMAnimatedPlaya)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Playa.cpp
*********************************************************************/
