/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_1
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\class_1.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_1.h"
//#[ ignore
#define Library_class_1_class_1_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class class_1
class_1::class_1() {
    NOTIFY_CONSTRUCTOR(class_1, class_1(), 0, Library_class_1_class_1_SERIALIZE);
}

class_1::~class_1() {
    NOTIFY_DESTRUCTOR(~class_1, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_1, Library, Library, false, OMAnimatedclass_1)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_1.cpp
*********************************************************************/
