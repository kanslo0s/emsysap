/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MusicStream
//!	Generated Date	: Mon, 30, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\MusicStream.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "MusicStream.h"
//## auto_generated
#include "BASS_Controller.h"
//## package MusicStream


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(MusicStream, MusicStream)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\MusicStream.cpp
*********************************************************************/
