/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Playlist
//!	Generated Date	: Fri, 11, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Playlist.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Playlist.h"
//## operation NextSong(bool,int)
#include "Song.h"
//#[ ignore
#define Library_Playlist_NextSong_SERIALIZE \
    aomsmethod->addAttribute("shuffle", x2String(shuffle));\
    aomsmethod->addAttribute("repeat", x2String(repeat));
#define Library_Playlist_Playlist_SERIALIZE OM_NO_OP

#define Library_Playlist_AddSong_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class Playlist
Playlist::Playlist() {
    NOTIFY_CONSTRUCTOR(Playlist, Playlist(), 0, Library_Playlist_Playlist_SERIALIZE);
    //#[ operation Playlist()
    //#]
}

Song* Playlist::NextSong(bool shuffle, int repeat) {
    NOTIFY_OPERATION(NextSong, NextSong(bool,int), 2, Library_Playlist_NextSong_SERIALIZE);
    //#[ operation NextSong(bool,int)
    Song* tempCurrentSongPtr = PlayerState->currentSongPtr;
    int tempCurrentSongInt = PlayerState->currentSongInt;
    int songsCount = itsSong.getCount() - 1; //total number of songs in a playlist  - 1 (for array reasons)
    
    try
    {
      	if(tempCurrentSongInt > songsCount)
      	{
      		throw 1;
      	}
      	
      	if(shuffle)
        {
        	return itsSong[songsCount * rand()];
        }
        else 
        {  
        	if(repeat == 1) // repeat the playlist once
        	{   
        		if(songsCount < tempCurrentSongInt)
        		{
        			return itsSong[tempCurrentSongInt + 1];
        		}
        		else { return NULL; }
        	}		 	
            else if (repeat == 2) //repeat the same song
        	{
        		return itsSong[tempCurrentSongInt]; 
        	} 
        	else if(repeat == 3) //repeat the playlist forever
        	{
        		if(songsCount < tempCurrentSongInt)
        		{
        			return itsSong[tempCurrentSongInt + 1];
        		}
        		else { return itsSong[0]; } 
        	}
        	else
        	{
    		 	if(tempCurrentSongInt > songsCount)
    	      	{
    	      		throw 2;
    	      	}
        	}
        }
    }
    catch (int e)
    {
     	cout << "An exception occurred. Exception Nr. " << e << '\n';
    } 
    return NULL;
    
    
    //#]
}

void Playlist::AddSong() {
    NOTIFY_OPERATION(AddSong, AddSong(), 0, Library_Playlist_AddSong_SERIALIZE);
    //#[ operation AddSong()
    //#]
}

Playlist::~Playlist() {
    NOTIFY_DESTRUCTOR(~Playlist, true);
    cleanUpRelations();
}

RhpString Playlist::getPlaylistName() const {
    return playlistName;
}

void Playlist::setPlaylistName(RhpString p_playlistName) {
    playlistName = p_playlistName;
}

OMIterator<Song*> Playlist::getItsSong() const {
    OMIterator<Song*> iter(itsSong);
    return iter;
}

void Playlist::addItsSong(Song* p_Song) {
    if(p_Song != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSong", p_Song, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSong");
        }
    itsSong.add(p_Song);
}

void Playlist::removeItsSong(Song* p_Song) {
    NOTIFY_RELATION_ITEM_REMOVED("itsSong", p_Song);
    itsSong.remove(p_Song);
}

void Playlist::clearItsSong() {
    NOTIFY_RELATION_CLEARED("itsSong");
    itsSong.removeAll();
}

void Playlist::cleanUpRelations() {
    {
        itsSong.removeAll();
    }
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPlaylist::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("playlistName", x2String(myReal->playlistName));
}

void OMAnimatedPlaylist::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSong", false, false);
    {
        OMIterator<Song*> iter(myReal->itsSong);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
}
//#]

IMPLEMENT_META_P(Playlist, Library, Library, false, OMAnimatedPlaylist)
#endif // _OMINSTRUMENT

//total number of songs in a playlist   - 1 (for array reasons)

// repeat the playlist once

//repeat the same song

//repeat the playlist forever

// no repeat

//repeat the same song

//repeat the same song

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Playlist.cpp
*********************************************************************/
