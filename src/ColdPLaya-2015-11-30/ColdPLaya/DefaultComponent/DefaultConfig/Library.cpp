/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Library
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Library.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Library.h"
//## auto_generated
#include "Album.h"
//## auto_generated
#include "Artist.h"
//## auto_generated
#include "Playlist.h"
//## auto_generated
#include "Song.h"
//#[ ignore
#define Library_Library_Library_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class Library
Library::Library() {
    NOTIFY_CONSTRUCTOR(Library, Library(), 0, Library_Library_Library_SERIALIZE);
    //#[ operation Library()
    //#]
}

Library::~Library() {
    NOTIFY_DESTRUCTOR(~Library, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(Library, Library, Library, false, OMAnimatedLibrary)
#endif // _OMINSTRUMENT

//## package Library


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Library, Library)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Library.cpp
*********************************************************************/
