/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Playlist
//!	Generated Date	: Fri, 11, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Playlist.h
*********************************************************************/

#ifndef Playlist_H
#define Playlist_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Library.h"
//## auto_generated
#include <oxf\omcollec.h>
//## operation NextSong(bool,int)
class Song;

//## package Library

//## class Playlist
class Playlist {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPlaylist;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation Playlist()
    Playlist();
    
    //## auto_generated
    ~Playlist();
    
    ////    Operations    ////
    
    //## operation NextSong(bool,int)
    Song* NextSong(bool shuffle = false, int repeat = 0);
    
    ////    Additional operations    ////
    
    //## auto_generated
    RhpString getPlaylistName() const;
    
    //## auto_generated
    void setPlaylistName(RhpString p_playlistName);
    
    //## auto_generated
    OMIterator<Song*> getItsSong() const;
    
    //## auto_generated
    void addItsSong(Song* p_Song);
    
    //## auto_generated
    void removeItsSong(Song* p_Song);
    
    //## auto_generated
    void clearItsSong();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    RhpString playlistName;		//## attribute playlistName
    
    ////    Relations and components    ////

public :

    //## operation AddSong()
    void AddSong();

protected :

    OMCollection<Song*> itsSong;		//## link itsSong
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPlaylist : virtual public AOMInstance {
    DECLARE_META(Playlist, OMAnimatedPlaylist)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Playlist.h
*********************************************************************/
