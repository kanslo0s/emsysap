/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Song
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Song.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Song.h"
//## link itsAlbum
#include "Album.h"
//#[ ignore
#define Library_Song_Song_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class Song
Song::Song() {
    NOTIFY_CONSTRUCTOR(Song, Song(), 0, Library_Song_Song_SERIALIZE);
    //#[ operation Song()
    //#]
}

Song::~Song() {
    NOTIFY_DESTRUCTOR(~Song, true);
    cleanUpRelations();
}

char* Song::getFilename() const {
    return filename;
}

void Song::setFilename(char* p_filename) {
    filename = p_filename;
}

RhpString Song::getSongTitle() const {
    return songTitle;
}

void Song::setSongTitle(RhpString p_songTitle) {
    songTitle = p_songTitle;
}

OMIterator<Album*> Song::getItsAlbum() const {
    OMIterator<Album*> iter(itsAlbum);
    return iter;
}

void Song::addItsAlbum(Album* p_Album) {
    if(p_Album != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsAlbum", p_Album, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsAlbum");
        }
    itsAlbum.add(p_Album);
}

void Song::removeItsAlbum(Album* p_Album) {
    NOTIFY_RELATION_ITEM_REMOVED("itsAlbum", p_Album);
    itsAlbum.remove(p_Album);
}

void Song::clearItsAlbum() {
    NOTIFY_RELATION_CLEARED("itsAlbum");
    itsAlbum.removeAll();
}

void Song::cleanUpRelations() {
    {
        itsAlbum.removeAll();
    }
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSong::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("filename", x2String(myReal->filename));
    aomsAttributes->addAttribute("songTitle", x2String(myReal->songTitle));
}

void OMAnimatedSong::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsAlbum", false, false);
    {
        OMIterator<Album*> iter(myReal->itsAlbum);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
}
//#]

IMPLEMENT_META_P(Song, Library, Library, false, OMAnimatedSong)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Song.cpp
*********************************************************************/
