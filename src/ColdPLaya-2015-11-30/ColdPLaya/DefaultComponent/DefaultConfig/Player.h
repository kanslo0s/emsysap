/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Player
//!	Generated Date	: Tue, 1, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Player.h
*********************************************************************/

#ifndef Player_H
#define Player_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Default.h"
//## package Default

//## actor Player
class Player {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPlayer;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Player();
    
    //## auto_generated
    ~Player();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPlayer : virtual public AOMInstance {
    DECLARE_META(Player, OMAnimatedPlayer)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Player.h
*********************************************************************/
