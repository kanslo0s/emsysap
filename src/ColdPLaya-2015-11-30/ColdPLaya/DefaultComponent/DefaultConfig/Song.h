/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Song
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Song.h
*********************************************************************/

#ifndef Song_H
#define Song_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Library.h"
//## auto_generated
#include <oxf\omcollec.h>
//## link itsAlbum
class Album;

//## package Library

//## class Song
class Song {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSong;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation Song()
    Song();
    
    //## auto_generated
    ~Song();
    
    ////    Additional operations    ////
    
    //## auto_generated
    char* getFilename() const;
    
    //## auto_generated
    void setFilename(char* p_filename);
    
    //## auto_generated
    RhpString getSongTitle() const;
    
    //## auto_generated
    void setSongTitle(RhpString p_songTitle);
    
    //## auto_generated
    OMIterator<Album*> getItsAlbum() const;
    
    //## auto_generated
    void addItsAlbum(Album* p_Album);
    
    //## auto_generated
    void removeItsAlbum(Album* p_Album);
    
    //## auto_generated
    void clearItsAlbum();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    char* filename;		//## attribute filename
    
    RhpString songTitle;		//## attribute songTitle
    
    ////    Relations and components    ////
    
    OMCollection<Album*> itsAlbum;		//## link itsAlbum
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSong : virtual public AOMInstance {
    DECLARE_META(Song, OMAnimatedSong)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Song.h
*********************************************************************/
