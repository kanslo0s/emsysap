/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Artist
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Artist.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Artist.h"
//## link itsLibrary
#include "Library.h"
//#[ ignore
#define Library_Artist_Artist_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class Artist
Artist::Artist() {
    NOTIFY_CONSTRUCTOR(Artist, Artist(), 0, Library_Artist_Artist_SERIALIZE);
    //#[ operation Artist()
    //#]
}

Artist::~Artist() {
    NOTIFY_DESTRUCTOR(~Artist, true);
    cleanUpRelations();
}

RhpString Artist::getName() const {
    return name;
}

void Artist::setName(RhpString p_name) {
    name = p_name;
}

OMIterator<Library*> Artist::getItsLibrary() const {
    OMIterator<Library*> iter(itsLibrary);
    return iter;
}

void Artist::addItsLibrary(Library* p_Library) {
    if(p_Library != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsLibrary", p_Library, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsLibrary");
        }
    itsLibrary.add(p_Library);
}

void Artist::removeItsLibrary(Library* p_Library) {
    NOTIFY_RELATION_ITEM_REMOVED("itsLibrary", p_Library);
    itsLibrary.remove(p_Library);
}

void Artist::clearItsLibrary() {
    NOTIFY_RELATION_CLEARED("itsLibrary");
    itsLibrary.removeAll();
}

void Artist::cleanUpRelations() {
    {
        itsLibrary.removeAll();
    }
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedArtist::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("name", x2String(myReal->name));
}

void OMAnimatedArtist::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsLibrary", false, false);
    {
        OMIterator<Library*> iter(myReal->itsLibrary);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
}
//#]

IMPLEMENT_META_P(Artist, Library, Library, false, OMAnimatedArtist)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Artist.cpp
*********************************************************************/
