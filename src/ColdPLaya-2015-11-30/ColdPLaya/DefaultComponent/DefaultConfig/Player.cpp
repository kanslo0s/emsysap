/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Player
//!	Generated Date	: Tue, 1, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\Player.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Player.h"
//#[ ignore
#define Default_Player_Player_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor Player
Player::Player() {
    NOTIFY_CONSTRUCTOR(Player, Player(), 0, Default_Player_Player_SERIALIZE);
}

Player::~Player() {
    NOTIFY_DESTRUCTOR(~Player, true);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPlayer::serializeRelations(AOMSRelations* aomsRelations) const {
}
//#]

IMPLEMENT_META_P(Player, Default, Default, false, OMAnimatedPlayer)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Player.cpp
*********************************************************************/
