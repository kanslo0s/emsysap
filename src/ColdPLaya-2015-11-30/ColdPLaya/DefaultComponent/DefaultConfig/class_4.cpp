/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_4
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\class_4.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_4.h"
//#[ ignore
#define Library_class_4_class_4_SERIALIZE OM_NO_OP
//#]

//## package Library

//## class class_4
class_4::class_4() {
    NOTIFY_CONSTRUCTOR(class_4, class_4(), 0, Library_class_4_class_4_SERIALIZE);
}

class_4::~class_4() {
    NOTIFY_DESTRUCTOR(~class_4, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_4, Library, Library, false, OMAnimatedclass_4)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_4.cpp
*********************************************************************/
