/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: BASS_Controller
//!	Generated Date	: Tue, 8, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\BASS_Controller.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "BASS_Controller.h"
//## auto_generated
#include <iostream>
//## auto_generated
#include <stdio.h>
//## auto_generated
#include <windows.h>
//## auto_generated
#include <commctrl.h>
//#[ ignore
#define MusicStream_BASS_Controller_BASS_Controller_SERIALIZE OM_NO_OP

#define MusicStream_BASS_Controller_OpenFile_SERIALIZE OM_NO_OP

#define MusicStream_BASS_Controller_Pause_SERIALIZE OM_NO_OP

#define MusicStream_BASS_Controller_Play_SERIALIZE OM_NO_OP

#define MusicStream_BASS_Controller_StopPlaying_SERIALIZE OM_NO_OP

#define MusicStream_BASS_Controller_VolumeDown_SERIALIZE OM_NO_OP

#define MusicStream_BASS_Controller_VolumeUp_SERIALIZE OM_NO_OP
//#]

/* As very last, close Bass */

// Default Sounddevice

// Sample rate (Hz)

/* Initialize output device */

/* Load your soundfile and play it */

// Default Sounddevice

// Sample rate (Hz)

/* Initialize output device */

/* Load your soundfile and play it */

//## package MusicStream

//## class BASS_Controller
BASS_Controller::BASS_Controller() {
    NOTIFY_CONSTRUCTOR(BASS_Controller, BASS_Controller(), 0, MusicStream_BASS_Controller_BASS_Controller_SERIALIZE);
    //#[ operation BASS_Controller()
    std::cout << "Welcome to ColdPlaya! " << BASS_GetVolume() << BASS_ErrorGetCode() << std::endl;  
    
    
    //#]
}

BASS_Controller::~BASS_Controller() {
    NOTIFY_DESTRUCTOR(~BASS_Controller, true);
    //#[ operation ~BASS_Controller()
    /* As very last, close Bass */
    BASS_Free();
    //#]
}

void BASS_Controller::OpenFile() {
    NOTIFY_OPERATION(OpenFile, OpenFile(), 0, MusicStream_BASS_Controller_OpenFile_SERIALIZE);
    //#[ operation OpenFile()
    int device = -1; // Default Sounddevice
    int freq = 44100; // Sample rate (Hz)
    
    
    /* Initialize output device */
    BASS_Init(device, freq, 0, 0, NULL);
    setVolume(BASS_GetVolume());
    
    /* Load your soundfile and play it */
    setStreamHandle(BASS_StreamCreateFile(FALSE, "StrawberrySwing.mp3", 0, 0, 0));
    BASS_ChannelPlay(getStreamHandle(), FALSE);
    //#]
}

void BASS_Controller::Pause() {
    NOTIFY_OPERATION(Pause, Pause(), 0, MusicStream_BASS_Controller_Pause_SERIALIZE);
    //#[ operation Pause()
    BASS_Pause();
    //#]
}

void BASS_Controller::Play() {
    NOTIFY_OPERATION(Play, Play(), 0, MusicStream_BASS_Controller_Play_SERIALIZE);
    //#[ operation Play()
    BASS_Start();
    //#]
}

void BASS_Controller::StopPlaying() {
    NOTIFY_OPERATION(StopPlaying, StopPlaying(), 0, MusicStream_BASS_Controller_StopPlaying_SERIALIZE);
    //#[ operation StopPlaying()
    BASS_ChannelStop(getStreamHandle());
    BASS_StreamFree(getStreamHandle());
    //#]
}

void BASS_Controller::VolumeDown() {
    NOTIFY_OPERATION(VolumeDown, VolumeDown(), 0, MusicStream_BASS_Controller_VolumeDown_SERIALIZE);
    //#[ operation VolumeDown()
    setVolume(getVolume() - (float)0.2);
    if(getVolume() < 0 )
    {
    	 setVolume(0);
    }  
    BASS_SetVolume(getVolume());	
    //#]
}

void BASS_Controller::VolumeUp() {
    NOTIFY_OPERATION(VolumeUp, VolumeUp(), 0, MusicStream_BASS_Controller_VolumeUp_SERIALIZE);
    //#[ operation VolumeUp()
    setVolume(getVolume() + (float)0.2);
    if(getVolume() > 1 )
    {
    	 setVolume(1);
    } 
    BASS_SetVolume(getVolume()); 
    //#]
}

float BASS_Controller::getVolume() const {
    return Volume;
}

void BASS_Controller::setVolume(float p_Volume) {
    Volume = p_Volume;
}

HSTREAM BASS_Controller::getStreamHandle() const {
    return streamHandle;
}

void BASS_Controller::setStreamHandle(HSTREAM p_streamHandle) {
    streamHandle = p_streamHandle;
}

int BASS_Controller::getRepeating() const {
    return Repeating;
}

void BASS_Controller::setRepeating(int p_Repeating) {
    Repeating = p_Repeating;
}

bool BASS_Controller::getShuffling() const {
    return Shuffling;
}

void BASS_Controller::setShuffling(bool p_Shuffling) {
    Shuffling = p_Shuffling;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedBASS_Controller::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("streamHandle", UNKNOWN2STRING(myReal->streamHandle));
    aomsAttributes->addAttribute("Volume", x2String(myReal->Volume));
    aomsAttributes->addAttribute("Shuffling", x2String(myReal->Shuffling));
    aomsAttributes->addAttribute("Repeating", x2String(myReal->Repeating));
}
//#]

IMPLEMENT_META_P(BASS_Controller, MusicStream, MusicStream, false, OMAnimatedBASS_Controller)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\BASS_Controller.cpp
*********************************************************************/
