/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Playa
//!	Generated Date	: Mon, 30, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Playa.h
*********************************************************************/

#ifndef Playa_H
#define Playa_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include "Default.h"
//## package Default

//## actor Playa
class Playa {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPlaya;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Playa();
    
    //## auto_generated
    ~Playa();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPlaya : virtual public AOMInstance {
    DECLARE_META(Playa, OMAnimatedPlaya)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Playa.h
*********************************************************************/
