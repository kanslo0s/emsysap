/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: PlayerStateMachine
//!	Generated Date	: Tue, 1, Dec 2015  
	File Path	: DefaultComponent\DefaultConfig\PlayerStateMachine.h
*********************************************************************/

#ifndef PlayerStateMachine_H
#define PlayerStateMachine_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "D:\Git\EmsysAP\src\ColdPLaya-2015-11-30\ColdPLaya\Lib\bass\c\bass.h"
//## auto_generated
#include <oxf\event.h>
//## auto_generated
class PlayerState;

//#[ ignore
#define event_1_PlayerStateMachine_id 3201

#define Paused_PlayerStateMachine_id 3202

#define Playing_PlayerStateMachine_id 3203

#define evPaused_PlayerStateMachine_id 3204

#define evPlaying_PlayerStateMachine_id 3205

#define evStop_PlayerStateMachine_id 3206

#define evOpen_PlayerStateMachine_id 3207

#define evRepeat_PlayerStateMachine_id 3208

#define evShuffle_PlayerStateMachine_id 3209

#define evVolUp_PlayerStateMachine_id 3210

#define evVolDown_PlayerStateMachine_id 3211
//#]

//## package PlayerStateMachine



//## event event_1()
class event_1 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevent_1;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    event_1();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevent_1 : virtual public AOMEvent {
    DECLARE_META_EVENT(event_1)
};
//#]
#endif // _OMINSTRUMENT

//## event Paused()
class Paused : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPaused;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Paused();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPaused : virtual public AOMEvent {
    DECLARE_META_EVENT(Paused)
};
//#]
#endif // _OMINSTRUMENT

//## event Playing()
class Playing : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPlaying;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Playing();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPlaying : virtual public AOMEvent {
    DECLARE_META_EVENT(Playing)
};
//#]
#endif // _OMINSTRUMENT

//## event evPaused()
class evPaused : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPaused;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPaused();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPaused : virtual public AOMEvent {
    DECLARE_META_EVENT(evPaused)
};
//#]
#endif // _OMINSTRUMENT

//## event evPlaying()
class evPlaying : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPlaying;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPlaying();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPlaying : virtual public AOMEvent {
    DECLARE_META_EVENT(evPlaying)
};
//#]
#endif // _OMINSTRUMENT

//## event evStop()
class evStop : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStop;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStop();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStop : virtual public AOMEvent {
    DECLARE_META_EVENT(evStop)
};
//#]
#endif // _OMINSTRUMENT

//## event evOpen()
class evOpen : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevOpen;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evOpen();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevOpen : virtual public AOMEvent {
    DECLARE_META_EVENT(evOpen)
};
//#]
#endif // _OMINSTRUMENT

//## event evRepeat()
class evRepeat : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevRepeat;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evRepeat();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevRepeat : virtual public AOMEvent {
    DECLARE_META_EVENT(evRepeat)
};
//#]
#endif // _OMINSTRUMENT

//## event evShuffle()
class evShuffle : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevShuffle;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evShuffle();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevShuffle : virtual public AOMEvent {
    DECLARE_META_EVENT(evShuffle)
};
//#]
#endif // _OMINSTRUMENT

//## event evVolUp()
class evVolUp : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevVolUp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evVolUp();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevVolUp : virtual public AOMEvent {
    DECLARE_META_EVENT(evVolUp)
};
//#]
#endif // _OMINSTRUMENT

//## event evVolDown()
class evVolDown : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevVolDown;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evVolDown();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevVolDown : virtual public AOMEvent {
    DECLARE_META_EVENT(evVolDown)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\PlayerStateMachine.h
*********************************************************************/
