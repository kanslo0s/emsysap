/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_30
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_30.h
*********************************************************************/

#ifndef class_30_H
#define class_30_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_30
class class_30 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_30();
    
    //## auto_generated
    ~class_30();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_30.h
*********************************************************************/
