/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_37
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_37.h
*********************************************************************/

#ifndef class_37_H
#define class_37_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_37
class class_37 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_37();
    
    //## auto_generated
    ~class_37();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_37.h
*********************************************************************/
