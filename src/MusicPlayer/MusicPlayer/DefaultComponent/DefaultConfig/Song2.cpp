/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Song2
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Song2.cpp
*********************************************************************/

//## auto_generated
#include "Song2.h"
//## link itsPlaylist2
#include "Playlist2.h"
//## package MusicPlayer

//## class Song2
Song2::Song2() {
    itsPlaylist2 = NULL;
    itsPlaylist2_1 = NULL;
    itsPlaylist2_2 = NULL;
}

Song2::~Song2() {
    cleanUpRelations();
}

void Song2::Information() {
    //#[ operation Information()
    //#]
}

char Song2::getAlbum() const {
    return Album;
}

void Song2::setAlbum(char p_Album) {
    Album = p_Album;
}

char Song2::getArtist() const {
    return Artist;
}

void Song2::setArtist(char p_Artist) {
    Artist = p_Artist;
}

int Song2::getLength() const {
    return Length;
}

void Song2::setLength(int p_Length) {
    Length = p_Length;
}

char Song2::getTitle() const {
    return Title;
}

void Song2::setTitle(char p_Title) {
    Title = p_Title;
}

Playlist2* Song2::getItsPlaylist2() const {
    return itsPlaylist2;
}

void Song2::setItsPlaylist2(Playlist2* p_Playlist2) {
    if(p_Playlist2 != NULL)
        {
            p_Playlist2->_setItsSong2(this);
        }
    _setItsPlaylist2(p_Playlist2);
}

Playlist2* Song2::getItsPlaylist2_1() const {
    return itsPlaylist2_1;
}

void Song2::setItsPlaylist2_1(Playlist2* p_Playlist2) {
    _setItsPlaylist2_1(p_Playlist2);
}

Playlist2* Song2::getItsPlaylist2_2() const {
    return itsPlaylist2_2;
}

void Song2::setItsPlaylist2_2(Playlist2* p_Playlist2) {
    _setItsPlaylist2_2(p_Playlist2);
}

OMIterator<Playlist2*> Song2::getItsPlaylist2_3() const {
    OMIterator<Playlist2*> iter(itsPlaylist2_3);
    return iter;
}

void Song2::addItsPlaylist2_3(Playlist2* p_Playlist2) {
    if(p_Playlist2 != NULL)
        {
            p_Playlist2->_addItsSong2_3(this);
        }
    _addItsPlaylist2_3(p_Playlist2);
}

void Song2::removeItsPlaylist2_3(Playlist2* p_Playlist2) {
    if(p_Playlist2 != NULL)
        {
            p_Playlist2->_removeItsSong2_3(this);
        }
    _removeItsPlaylist2_3(p_Playlist2);
}

void Song2::clearItsPlaylist2_3() {
    OMIterator<Playlist2*> iter(itsPlaylist2_3);
    while (*iter){
        Playlist2* current = *iter;
        if(current != NULL)
            {
                current->_removeItsSong2_3(this);
            }
        iter++;
    }
    _clearItsPlaylist2_3();
}

void Song2::cleanUpRelations() {
    if(itsPlaylist2 != NULL)
        {
            Song2* p_Song2 = itsPlaylist2->getItsSong2();
            if(p_Song2 != NULL)
                {
                    itsPlaylist2->__setItsSong2(NULL);
                }
            itsPlaylist2 = NULL;
        }
    if(itsPlaylist2_1 != NULL)
        {
            itsPlaylist2_1 = NULL;
        }
    if(itsPlaylist2_2 != NULL)
        {
            itsPlaylist2_2 = NULL;
        }
    {
        OMIterator<Playlist2*> iter(itsPlaylist2_3);
        while (*iter){
            Playlist2* current = *iter;
            if(current != NULL)
                {
                    current->_removeItsSong2_3(this);
                }
            iter++;
        }
        itsPlaylist2_3.removeAll();
    }
}

void Song2::__setItsPlaylist2(Playlist2* p_Playlist2) {
    itsPlaylist2 = p_Playlist2;
}

void Song2::_setItsPlaylist2(Playlist2* p_Playlist2) {
    if(itsPlaylist2 != NULL)
        {
            itsPlaylist2->__setItsSong2(NULL);
        }
    __setItsPlaylist2(p_Playlist2);
}

void Song2::_clearItsPlaylist2() {
    itsPlaylist2 = NULL;
}

void Song2::__setItsPlaylist2_1(Playlist2* p_Playlist2) {
    itsPlaylist2_1 = p_Playlist2;
}

void Song2::_setItsPlaylist2_1(Playlist2* p_Playlist2) {
    __setItsPlaylist2_1(p_Playlist2);
}

void Song2::_clearItsPlaylist2_1() {
    itsPlaylist2_1 = NULL;
}

void Song2::__setItsPlaylist2_2(Playlist2* p_Playlist2) {
    itsPlaylist2_2 = p_Playlist2;
}

void Song2::_setItsPlaylist2_2(Playlist2* p_Playlist2) {
    __setItsPlaylist2_2(p_Playlist2);
}

void Song2::_clearItsPlaylist2_2() {
    itsPlaylist2_2 = NULL;
}

void Song2::_addItsPlaylist2_3(Playlist2* p_Playlist2) {
    itsPlaylist2_3.add(p_Playlist2);
}

void Song2::_removeItsPlaylist2_3(Playlist2* p_Playlist2) {
    itsPlaylist2_3.remove(p_Playlist2);
}

void Song2::_clearItsPlaylist2_3() {
    itsPlaylist2_3.removeAll();
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Song2.cpp
*********************************************************************/
