/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_31
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_31.h
*********************************************************************/

#ifndef class_31_H
#define class_31_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_31
class class_31 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_31();
    
    //## auto_generated
    ~class_31();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_31.h
*********************************************************************/
