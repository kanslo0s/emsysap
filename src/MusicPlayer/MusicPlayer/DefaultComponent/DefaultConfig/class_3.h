/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_3
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_3.h
*********************************************************************/

#ifndef class_3_H
#define class_3_H

//## auto_generated
#include <oxf\oxf.h>
//## package SoundStream

//## class class_3
class class_3 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_3();
    
    //## auto_generated
    ~class_3();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_3.h
*********************************************************************/
