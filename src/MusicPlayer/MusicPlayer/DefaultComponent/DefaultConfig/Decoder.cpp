/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Decoder
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Decoder.cpp
*********************************************************************/

//## auto_generated
#include "Decoder.h"
//## link itsOutputSound
#include "OutputSound.h"
//## package MusicStream

//## class Decoder
Decoder::Decoder() {
    itsOutputSound = NULL;
}

Decoder::~Decoder() {
    cleanUpRelations();
}

OutputSound* Decoder::getItsOutputSound() const {
    return itsOutputSound;
}

void Decoder::setItsOutputSound(OutputSound* p_OutputSound) {
    itsOutputSound = p_OutputSound;
}

void Decoder::cleanUpRelations() {
    if(itsOutputSound != NULL)
        {
            itsOutputSound = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Decoder.cpp
*********************************************************************/
