/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_38
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_38.h
*********************************************************************/

#ifndef class_38_H
#define class_38_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_38
class class_38 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_38();
    
    //## auto_generated
    ~class_38();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_38.h
*********************************************************************/
