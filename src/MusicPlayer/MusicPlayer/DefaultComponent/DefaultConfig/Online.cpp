/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Online
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Online.cpp
*********************************************************************/

//## auto_generated
#include "Online.h"
//## package Library

//## class Online
Online::Online() {
}

Online::~Online() {
}

void Online::Login() {
    //#[ operation Login()
    //#]
}

void Online::Logout() {
    //#[ operation Logout()
    //#]
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Online.cpp
*********************************************************************/
