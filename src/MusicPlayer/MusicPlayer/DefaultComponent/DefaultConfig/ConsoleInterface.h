/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: ConsoleInterface
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\ConsoleInterface.h
*********************************************************************/

#ifndef ConsoleInterface_H
#define ConsoleInterface_H

//## auto_generated
#include <oxf\oxf.h>
//## class ConsoleInterface
#include "Interface.h"
//## package Control

//## class ConsoleInterface
class ConsoleInterface : public Interface {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    ConsoleInterface();
    
    //## auto_generated
    ~ConsoleInterface();
    
    ////    Operations    ////
    
    //## operation parseInput()
    void parseInput();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\ConsoleInterface.h
*********************************************************************/
