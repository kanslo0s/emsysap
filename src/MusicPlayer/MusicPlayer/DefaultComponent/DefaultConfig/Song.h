/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Song
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Song.h
*********************************************************************/

#ifndef Song_H
#define Song_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <oxf\omcollec.h>
//## link itsPlaylist
class Playlist;

//## package Library

//## class Song
class Song {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Song();
    
    //## auto_generated
    ~Song();
    
    ////    Operations    ////
    
    //## operation Information()
    void Information();
    
    ////    Additional operations    ////
    
    //## auto_generated
    char getAlbum() const;
    
    //## auto_generated
    void setAlbum(char p_Album);
    
    //## auto_generated
    char getArtist() const;
    
    //## auto_generated
    void setArtist(char p_Artist);
    
    //## auto_generated
    int getLength() const;
    
    //## auto_generated
    void setLength(int p_Length);
    
    //## auto_generated
    char getTitle() const;
    
    //## auto_generated
    void setTitle(char p_Title);
    
    //## auto_generated
    Playlist* getItsPlaylist() const;
    
    //## auto_generated
    void setItsPlaylist(Playlist* p_Playlist);
    
    //## auto_generated
    Playlist* getItsPlaylist_1() const;
    
    //## auto_generated
    void setItsPlaylist_1(Playlist* p_Playlist);
    
    //## auto_generated
    Playlist* getItsPlaylist_2() const;
    
    //## auto_generated
    void setItsPlaylist_2(Playlist* p_Playlist);
    
    //## auto_generated
    OMIterator<Playlist*> getItsPlaylist_3() const;
    
    //## auto_generated
    void addItsPlaylist_3(Playlist* p_Playlist);
    
    //## auto_generated
    void removeItsPlaylist_3(Playlist* p_Playlist);
    
    //## auto_generated
    void clearItsPlaylist_3();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    char Album;		//## attribute Album
    
    char Artist;		//## attribute Artist
    
    int Length;		//## attribute Length
    
    char Title;		//## attribute Title
    
    ////    Relations and components    ////
    
    Playlist* itsPlaylist;		//## link itsPlaylist
    
    Playlist* itsPlaylist_1;		//## link itsPlaylist_1
    
    Playlist* itsPlaylist_2;		//## link itsPlaylist_2
    
    OMCollection<Playlist*> itsPlaylist_3;		//## link itsPlaylist_3
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsPlaylist(Playlist* p_Playlist);
    
    //## auto_generated
    void _setItsPlaylist(Playlist* p_Playlist);
    
    //## auto_generated
    void _clearItsPlaylist();
    
    //## auto_generated
    void __setItsPlaylist_1(Playlist* p_Playlist);
    
    //## auto_generated
    void _setItsPlaylist_1(Playlist* p_Playlist);
    
    //## auto_generated
    void _clearItsPlaylist_1();
    
    //## auto_generated
    void __setItsPlaylist_2(Playlist* p_Playlist);
    
    //## auto_generated
    void _setItsPlaylist_2(Playlist* p_Playlist);
    
    //## auto_generated
    void _clearItsPlaylist_2();
    
    //## auto_generated
    void _addItsPlaylist_3(Playlist* p_Playlist);
    
    //## auto_generated
    void _removeItsPlaylist_3(Playlist* p_Playlist);
    
    //## auto_generated
    void _clearItsPlaylist_3();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Song.h
*********************************************************************/
