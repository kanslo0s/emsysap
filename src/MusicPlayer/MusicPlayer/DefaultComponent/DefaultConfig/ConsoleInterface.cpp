/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: ConsoleInterface
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\ConsoleInterface.cpp
*********************************************************************/

//## auto_generated
#include "ConsoleInterface.h"
//## package Control

//## class ConsoleInterface
ConsoleInterface::ConsoleInterface() {
}

ConsoleInterface::~ConsoleInterface() {
}

void ConsoleInterface::parseInput() {
    //#[ operation parseInput()
    //#]
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\ConsoleInterface.cpp
*********************************************************************/
