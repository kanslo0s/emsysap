/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: actor_42
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\actor_42.h
*********************************************************************/

#ifndef actor_42_H
#define actor_42_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## actor actor_42
class actor_42 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    actor_42();
    
    //## auto_generated
    ~actor_42();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\actor_42.h
*********************************************************************/
