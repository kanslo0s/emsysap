/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_2
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_2.h
*********************************************************************/

#ifndef class_2_H
#define class_2_H

//## auto_generated
#include <oxf\oxf.h>
//## package MusicPlayer

//## class class_2
class class_2 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_2();
    
    //## auto_generated
    ~class_2();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_2.h
*********************************************************************/
