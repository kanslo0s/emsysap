/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: D
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\D.h
*********************************************************************/

#ifndef D_H
#define D_H

//## auto_generated
#include <oxf\oxf.h>
//## package SoundStream

//## class D
class D {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    D();
    
    //## auto_generated
    ~D();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\D.h
*********************************************************************/
