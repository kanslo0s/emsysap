/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: OutputSound
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\OutputSound.h
*********************************************************************/

#ifndef OutputSound_H
#define OutputSound_H

//## auto_generated
#include <oxf\oxf.h>
//## package Output

//## class OutputSound
class OutputSound {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    OutputSound();
    
    //## auto_generated
    ~OutputSound();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getVolume() const;
    
    //## auto_generated
    void setVolume(int p_Volume);
    
    ////    Attributes    ////

protected :

    int Volume;		//## attribute Volume
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\OutputSound.h
*********************************************************************/
