/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MusicController
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\MusicController.h
*********************************************************************/

#ifndef MusicController_H
#define MusicController_H

//## auto_generated
#include <oxf\oxf.h>
//## package MusicPlayer

//## class MusicController
class MusicController {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    MusicController();
    
    //## auto_generated
    ~MusicController();
    
    ////    Operations    ////
    
    //## operation AdjustVolume()
    void AdjustVolume();
    
    //## operation Next()
    void Next();
    
    //## operation Pause()
    void Pause();
    
    //## operation Play()
    void Play();
    
    //## operation Previous()
    void Previous();
    
    //## operation Repeat()
    void Repeat();
    
    //## operation Shuffle()
    void Shuffle();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getVolumeLevel() const;
    
    //## auto_generated
    void setVolumeLevel(int p_VolumeLevel);
    
    ////    Attributes    ////

protected :

    int VolumeLevel;		//## attribute VolumeLevel
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\MusicController.h
*********************************************************************/
