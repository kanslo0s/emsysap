/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: User
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\User.h
*********************************************************************/

#ifndef User_H
#define User_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## actor User
class User {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    User();
    
    //## auto_generated
    ~User();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\User.h
*********************************************************************/
