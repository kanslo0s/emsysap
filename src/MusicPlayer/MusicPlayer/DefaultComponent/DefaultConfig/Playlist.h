/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Playlist
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Playlist.h
*********************************************************************/

#ifndef Playlist_H
#define Playlist_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <oxf\omcollec.h>
//## link itsSong_1
#include "Song.h"
//## package Library

//## class Playlist
class Playlist {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Playlist();
    
    //## auto_generated
    ~Playlist();
    
    ////    Operations    ////
    
    //## operation AddSong()
    void AddSong();
    
    //## operation EditPlaylistName()
    void EditPlaylistName();
    
    //## operation RemoveSong()
    void RemoveSong();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getNumberOfSongs() const;
    
    //## auto_generated
    void setNumberOfSongs(int p_NumberOfSongs);
    
    //## auto_generated
    char getPlaylistName() const;
    
    //## auto_generated
    void setPlaylistName(char p_PlaylistName);
    
    //## auto_generated
    OMIterator<Song*> getItsSong() const;
    
    //## auto_generated
    void addItsSong(Song* p_Song);
    
    //## auto_generated
    void removeItsSong(Song* p_Song);
    
    //## auto_generated
    void clearItsSong();
    
    //## auto_generated
    int getItsSong_1() const;
    
    //## auto_generated
    Song* getItsSong_2() const;
    
    //## auto_generated
    void setItsSong_2(Song* p_Song);
    
    //## auto_generated
    Song* getItsSong_3() const;

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int NumberOfSongs;		//## attribute NumberOfSongs
    
    char PlaylistName;		//## attribute PlaylistName
    
    ////    Relations and components    ////
    
    OMCollection<Song*> itsSong;		//## link itsSong
    
    Song itsSong_1[n];		//## link itsSong_1
    
    Song* itsSong_2;		//## link itsSong_2
    
    Song itsSong_3;		//## link itsSong_3
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void _addItsSong(Song* p_Song);
    
    //## auto_generated
    void _removeItsSong(Song* p_Song);
    
    //## auto_generated
    void _clearItsSong();
    
    //## auto_generated
    void __setItsSong_2(Song* p_Song);
    
    //## auto_generated
    void _setItsSong_2(Song* p_Song);
    
    //## auto_generated
    void _clearItsSong_2();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Playlist.h
*********************************************************************/
