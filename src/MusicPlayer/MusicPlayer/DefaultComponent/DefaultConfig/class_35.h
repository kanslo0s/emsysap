/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_35
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_35.h
*********************************************************************/

#ifndef class_35_H
#define class_35_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_35
class class_35 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_35();
    
    //## auto_generated
    ~class_35();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_35.h
*********************************************************************/
