/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MusicPlayer
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\MusicPlayer.h
*********************************************************************/

#ifndef MusicPlayer_H
#define MusicPlayer_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class MusicPlayer
class MusicPlayer {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    MusicPlayer();
    
    //## auto_generated
    ~MusicPlayer();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\MusicPlayer.h
*********************************************************************/
