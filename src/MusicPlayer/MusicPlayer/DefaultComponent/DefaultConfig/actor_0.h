/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: actor_0
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\actor_0.h
*********************************************************************/

#ifndef actor_0_H
#define actor_0_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## actor actor_0
class actor_0 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    actor_0();
    
    //## auto_generated
    ~actor_0();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\actor_0.h
*********************************************************************/
