/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Interface
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Interface.cpp
*********************************************************************/

//## auto_generated
#include "Interface.h"
//## link itsPlayer
#include "Player.h"
//## package Control

//## class Interface
Interface::Interface() {
    itsPlayer = NULL;
}

Interface::~Interface() {
    cleanUpRelations();
}

void Interface::AdjustVolume() {
    //#[ operation AdjustVolume()
    //#]
}

void Interface::Next() {
    //#[ operation Next()
    //#]
}

void Interface::Pause() {
    //#[ operation Pause()
    //#]
}

void Interface::Play() {
    //#[ operation Play()
    //#]
}

void Interface::Previous() {
    //#[ operation Previous()
    //#]
}

void Interface::Repeat() {
    //#[ operation Repeat()
    //#]
}

void Interface::Shuffle() {
    //#[ operation Shuffle()
    //#]
}

int Interface::getVolumeLevel() const {
    return VolumeLevel;
}

void Interface::setVolumeLevel(int p_VolumeLevel) {
    VolumeLevel = p_VolumeLevel;
}

Player* Interface::getItsPlayer() const {
    return itsPlayer;
}

void Interface::setItsPlayer(Player* p_Player) {
    itsPlayer = p_Player;
}

void Interface::cleanUpRelations() {
    if(itsPlayer != NULL)
        {
            itsPlayer = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Interface.cpp
*********************************************************************/
