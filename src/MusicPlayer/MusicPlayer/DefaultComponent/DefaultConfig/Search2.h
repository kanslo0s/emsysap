/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Search2
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Search2.h
*********************************************************************/

#ifndef Search2_H
#define Search2_H

//## auto_generated
#include <oxf\oxf.h>
//## package MusicPlayer

//## class Search2
class Search2 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Search2();
    
    //## auto_generated
    ~Search2();
    
    ////    Operations    ////
    
    //## operation Keyboard()
    void Keyboard();
    
    //## operation Voice()
    void Voice();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Search2.h
*********************************************************************/
