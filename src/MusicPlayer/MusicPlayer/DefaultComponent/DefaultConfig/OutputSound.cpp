/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: OutputSound
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\OutputSound.cpp
*********************************************************************/

//## auto_generated
#include "OutputSound.h"
//## package Output

//## class OutputSound
OutputSound::OutputSound() {
}

OutputSound::~OutputSound() {
}

int OutputSound::getVolume() const {
    return Volume;
}

void OutputSound::setVolume(int p_Volume) {
    Volume = p_Volume;
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\OutputSound.cpp
*********************************************************************/
