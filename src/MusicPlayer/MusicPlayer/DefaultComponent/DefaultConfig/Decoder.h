/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Decoder
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Decoder.h
*********************************************************************/

#ifndef Decoder_H
#define Decoder_H

//## auto_generated
#include <oxf\oxf.h>
//## link itsOutputSound
class OutputSound;

//## package MusicStream

//## class Decoder
class Decoder {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Decoder();
    
    //## auto_generated
    ~Decoder();
    
    ////    Additional operations    ////
    
    //## auto_generated
    OutputSound* getItsOutputSound() const;
    
    //## auto_generated
    void setItsOutputSound(OutputSound* p_OutputSound);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    OutputSound* itsOutputSound;		//## link itsOutputSound
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Decoder.h
*********************************************************************/
