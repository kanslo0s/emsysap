/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Online
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Online.h
*********************************************************************/

#ifndef Online_H
#define Online_H

//## auto_generated
#include <oxf\oxf.h>
//## package Library

//## class Online
class Online {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Online();
    
    //## auto_generated
    ~Online();
    
    ////    Operations    ////
    
    //## operation Login()
    void Login();
    
    //## operation Logout()
    void Logout();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Online.h
*********************************************************************/
