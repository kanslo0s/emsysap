/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_41
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_41.h
*********************************************************************/

#ifndef class_41_H
#define class_41_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_41
class class_41 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_41();
    
    //## auto_generated
    ~class_41();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_41.h
*********************************************************************/
