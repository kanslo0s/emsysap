/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Interface
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Interface.h
*********************************************************************/

#ifndef Interface_H
#define Interface_H

//## auto_generated
#include <oxf\oxf.h>
//## link itsPlayer
class Player;

//## package Control

//## class Interface
class Interface {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Interface();
    
    //## auto_generated
    ~Interface();
    
    ////    Operations    ////
    
    //## operation AdjustVolume()
    void AdjustVolume();
    
    //## operation Next()
    void Next();
    
    //## operation Pause()
    void Pause();
    
    //## operation Play()
    void Play();
    
    //## operation Previous()
    void Previous();
    
    //## operation Repeat()
    void Repeat();
    
    //## operation Shuffle()
    void Shuffle();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getVolumeLevel() const;
    
    //## auto_generated
    void setVolumeLevel(int p_VolumeLevel);
    
    //## auto_generated
    Player* getItsPlayer() const;
    
    //## auto_generated
    void setItsPlayer(Player* p_Player);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int VolumeLevel;		//## attribute VolumeLevel
    
    ////    Relations and components    ////
    
    Player* itsPlayer;		//## link itsPlayer
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Interface.h
*********************************************************************/
