/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Song
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Song.cpp
*********************************************************************/

//## auto_generated
#include "Song.h"
//## link itsPlaylist
#include "Playlist.h"
//## package Library

//## class Song
Song::Song() {
    itsPlaylist = NULL;
    itsPlaylist_1 = NULL;
    itsPlaylist_2 = NULL;
}

Song::~Song() {
    cleanUpRelations();
}

void Song::Information() {
    //#[ operation Information()
    //#]
}

char Song::getAlbum() const {
    return Album;
}

void Song::setAlbum(char p_Album) {
    Album = p_Album;
}

char Song::getArtist() const {
    return Artist;
}

void Song::setArtist(char p_Artist) {
    Artist = p_Artist;
}

int Song::getLength() const {
    return Length;
}

void Song::setLength(int p_Length) {
    Length = p_Length;
}

char Song::getTitle() const {
    return Title;
}

void Song::setTitle(char p_Title) {
    Title = p_Title;
}

Playlist* Song::getItsPlaylist() const {
    return itsPlaylist;
}

void Song::setItsPlaylist(Playlist* p_Playlist) {
    if(p_Playlist != NULL)
        {
            p_Playlist->_setItsSong_2(this);
        }
    _setItsPlaylist(p_Playlist);
}

Playlist* Song::getItsPlaylist_1() const {
    return itsPlaylist_1;
}

void Song::setItsPlaylist_1(Playlist* p_Playlist) {
    _setItsPlaylist_1(p_Playlist);
}

Playlist* Song::getItsPlaylist_2() const {
    return itsPlaylist_2;
}

void Song::setItsPlaylist_2(Playlist* p_Playlist) {
    _setItsPlaylist_2(p_Playlist);
}

OMIterator<Playlist*> Song::getItsPlaylist_3() const {
    OMIterator<Playlist*> iter(itsPlaylist_3);
    return iter;
}

void Song::addItsPlaylist_3(Playlist* p_Playlist) {
    if(p_Playlist != NULL)
        {
            p_Playlist->_addItsSong(this);
        }
    _addItsPlaylist_3(p_Playlist);
}

void Song::removeItsPlaylist_3(Playlist* p_Playlist) {
    if(p_Playlist != NULL)
        {
            p_Playlist->_removeItsSong(this);
        }
    _removeItsPlaylist_3(p_Playlist);
}

void Song::clearItsPlaylist_3() {
    OMIterator<Playlist*> iter(itsPlaylist_3);
    while (*iter){
        Playlist* current = *iter;
        if(current != NULL)
            {
                current->_removeItsSong(this);
            }
        iter++;
    }
    _clearItsPlaylist_3();
}

void Song::cleanUpRelations() {
    if(itsPlaylist != NULL)
        {
            Song* p_Song = itsPlaylist->getItsSong_2();
            if(p_Song != NULL)
                {
                    itsPlaylist->__setItsSong_2(NULL);
                }
            itsPlaylist = NULL;
        }
    if(itsPlaylist_1 != NULL)
        {
            itsPlaylist_1 = NULL;
        }
    if(itsPlaylist_2 != NULL)
        {
            itsPlaylist_2 = NULL;
        }
    {
        OMIterator<Playlist*> iter(itsPlaylist_3);
        while (*iter){
            Playlist* current = *iter;
            if(current != NULL)
                {
                    current->_removeItsSong(this);
                }
            iter++;
        }
        itsPlaylist_3.removeAll();
    }
}

void Song::__setItsPlaylist(Playlist* p_Playlist) {
    itsPlaylist = p_Playlist;
}

void Song::_setItsPlaylist(Playlist* p_Playlist) {
    if(itsPlaylist != NULL)
        {
            itsPlaylist->__setItsSong_2(NULL);
        }
    __setItsPlaylist(p_Playlist);
}

void Song::_clearItsPlaylist() {
    itsPlaylist = NULL;
}

void Song::__setItsPlaylist_1(Playlist* p_Playlist) {
    itsPlaylist_1 = p_Playlist;
}

void Song::_setItsPlaylist_1(Playlist* p_Playlist) {
    __setItsPlaylist_1(p_Playlist);
}

void Song::_clearItsPlaylist_1() {
    itsPlaylist_1 = NULL;
}

void Song::__setItsPlaylist_2(Playlist* p_Playlist) {
    itsPlaylist_2 = p_Playlist;
}

void Song::_setItsPlaylist_2(Playlist* p_Playlist) {
    __setItsPlaylist_2(p_Playlist);
}

void Song::_clearItsPlaylist_2() {
    itsPlaylist_2 = NULL;
}

void Song::_addItsPlaylist_3(Playlist* p_Playlist) {
    itsPlaylist_3.add(p_Playlist);
}

void Song::_removeItsPlaylist_3(Playlist* p_Playlist) {
    itsPlaylist_3.remove(p_Playlist);
}

void Song::_clearItsPlaylist_3() {
    itsPlaylist_3.removeAll();
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Song.cpp
*********************************************************************/
