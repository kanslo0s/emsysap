/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Search2
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Search2.cpp
*********************************************************************/

//## auto_generated
#include "Search2.h"
//## package MusicPlayer

//## class Search2
Search2::Search2() {
}

Search2::~Search2() {
}

void Search2::Keyboard() {
    //#[ operation Keyboard()
    //#]
}

void Search2::Voice() {
    //#[ operation Voice()
    //#]
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Search2.cpp
*********************************************************************/
