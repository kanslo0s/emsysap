/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_34
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_34.h
*********************************************************************/

#ifndef class_34_H
#define class_34_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_34
class class_34 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_34();
    
    //## auto_generated
    ~class_34();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_34.h
*********************************************************************/
