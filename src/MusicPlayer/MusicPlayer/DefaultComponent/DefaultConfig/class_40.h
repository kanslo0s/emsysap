/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_40
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_40.h
*********************************************************************/

#ifndef class_40_H
#define class_40_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_40
class class_40 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_40();
    
    //## auto_generated
    ~class_40();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_40.h
*********************************************************************/
