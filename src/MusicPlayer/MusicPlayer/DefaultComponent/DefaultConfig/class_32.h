/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_32
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_32.h
*********************************************************************/

#ifndef class_32_H
#define class_32_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_32
class class_32 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_32();
    
    //## auto_generated
    ~class_32();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_32.h
*********************************************************************/
