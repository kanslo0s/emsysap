/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Player
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Player.h
*********************************************************************/

#ifndef Player_H
#define Player_H

//## auto_generated
#include <oxf\oxf.h>
//## attribute CurrentPlaylist
#include "Playlist.h"
//## attribute CurrentSong
#include "Song.h"
//## link itsFileReader
class FileReader;

//## link itsOutputSound
class OutputSound;

//## package PSM

//## class Player
class Player {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Player();
    
    //## auto_generated
    ~Player();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Playlist* getCurrentPlaylist() const;
    
    //## auto_generated
    Song* getCurrentSong() const;
    
    //## auto_generated
    bool getPlaying() const;
    
    //## auto_generated
    void setPlaying(bool p_Playing);
    
    //## auto_generated
    bool getShuffleOn() const;
    
    //## auto_generated
    void setShuffleOn(bool p_ShuffleOn);
    
    //## auto_generated
    int getVolume() const;
    
    //## auto_generated
    void setVolume(int p_Volume);
    
    //## auto_generated
    FileReader* getItsFileReader() const;
    
    //## auto_generated
    void setItsFileReader(FileReader* p_FileReader);
    
    //## auto_generated
    OutputSound* getItsOutputSound() const;
    
    //## auto_generated
    void setItsOutputSound(OutputSound* p_OutputSound);
    
    //## auto_generated
    Playlist* getItsPlaylist() const;
    
    //## auto_generated
    void setItsPlaylist(Playlist* p_Playlist);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    Playlist CurrentPlaylist;		//## attribute CurrentPlaylist
    
    Song CurrentSong;		//## attribute CurrentSong
    
    bool Playing;		//## attribute Playing
    
    bool ShuffleOn;		//## attribute ShuffleOn
    
    int Volume;		//## attribute Volume
    
    ////    Relations and components    ////
    
    FileReader* itsFileReader;		//## link itsFileReader
    
    OutputSound* itsOutputSound;		//## link itsOutputSound
    
    Playlist* itsPlaylist;		//## link itsPlaylist
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Player.h
*********************************************************************/
