/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: FileReader
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\FileReader.cpp
*********************************************************************/

//## auto_generated
#include "FileReader.h"
//## link itsDecoder
#include "Decoder.h"
//## package MusicStream

//## class FileReader
FileReader::FileReader() {
    itsDecoder = NULL;
}

FileReader::~FileReader() {
    cleanUpRelations();
}

char* FileReader::getFilename() const {
    return filename;
}

void FileReader::setFilename(char* p_filename) {
    filename = p_filename;
}

Decoder* FileReader::getItsDecoder() const {
    return itsDecoder;
}

void FileReader::setItsDecoder(Decoder* p_Decoder) {
    itsDecoder = p_Decoder;
}

void FileReader::cleanUpRelations() {
    if(itsDecoder != NULL)
        {
            itsDecoder = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\FileReader.cpp
*********************************************************************/
