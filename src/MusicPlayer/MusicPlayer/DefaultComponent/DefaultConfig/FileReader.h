/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: FileReader
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\FileReader.h
*********************************************************************/

#ifndef FileReader_H
#define FileReader_H

//## auto_generated
#include <oxf\oxf.h>
//## link itsDecoder
class Decoder;

//## package MusicStream

//## class FileReader
class FileReader {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    FileReader();
    
    //## auto_generated
    ~FileReader();
    
    ////    Additional operations    ////
    
    //## auto_generated
    char* getFilename() const;
    
    //## auto_generated
    void setFilename(char* p_filename);
    
    //## auto_generated
    Decoder* getItsDecoder() const;
    
    //## auto_generated
    void setItsDecoder(Decoder* p_Decoder);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    char* filename;		//## attribute filename
    
    ////    Relations and components    ////
    
    Decoder* itsDecoder;		//## link itsDecoder
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\FileReader.h
*********************************************************************/
