/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: GraphicalInterface
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\GraphicalInterface.h
*********************************************************************/

#ifndef GraphicalInterface_H
#define GraphicalInterface_H

//## auto_generated
#include <oxf\oxf.h>
//## class GraphicalInterface
#include "Interface.h"
//## package Control

//## class GraphicalInterface
class GraphicalInterface : public Interface {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    GraphicalInterface();
    
    //## auto_generated
    ~GraphicalInterface();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\GraphicalInterface.h
*********************************************************************/
