/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Playlist2
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Playlist2.cpp
*********************************************************************/

//## auto_generated
#include "Playlist2.h"
//## package MusicPlayer

//## class Playlist2
Playlist2::Playlist2() {
    itsSong_2 = NULL;
    initRelations();
}

Playlist2::~Playlist2() {
    cleanUpRelations();
}

void Playlist2::AddSong() {
    //#[ operation AddSong()
    //#]
}

void Playlist2::EditPlaylistName() {
    //#[ operation EditPlaylistName()
    //#]
}

void Playlist2::Playlist() {
    //#[ operation Playlist()
    //#]
}

void Playlist2::RemoveSong() {
    //#[ operation RemoveSong()
    //#]
}

int Playlist2::getNumberOfSongs() const {
    return NumberOfSongs;
}

void Playlist2::setNumberOfSongs(int p_NumberOfSongs) {
    NumberOfSongs = p_NumberOfSongs;
}

char Playlist2::getPlaylistName() const {
    return PlaylistName;
}

void Playlist2::setPlaylistName(char p_PlaylistName) {
    PlaylistName = p_PlaylistName;
}

OMIterator<Song*> Playlist2::getItsSong() const {
    OMIterator<Song*> iter(itsSong);
    return iter;
}

void Playlist2::addItsSong(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_addItsPlaylist2_3(this);
        }
    _addItsSong(p_Song);
}

void Playlist2::removeItsSong(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_removeItsPlaylist2_3(this);
        }
    _removeItsSong(p_Song);
}

void Playlist2::clearItsSong() {
    OMIterator<Song*> iter(itsSong);
    while (*iter){
        Song* current = *iter;
        if(current != NULL)
            {
                current->_removeItsPlaylist2_3(this);
            }
        iter++;
    }
    _clearItsSong();
}

int Playlist2::getItsSong_1() const {
    int iter = 0;
    return iter;
}

Song* Playlist2::getItsSong_2() const {
    return itsSong_2;
}

void Playlist2::setItsSong_2(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_setItsPlaylist2(this);
        }
    _setItsSong_2(p_Song);
}

Song* Playlist2::getItsSong_3() const {
    return (Song*) &itsSong_3;
}

void Playlist2::initRelations() {
    {
        int iter = 0;
        while (iter < n){
            ((Song*)&itsSong_1[iter])->_setItsPlaylist2_1(this);
            iter++;
        }
    }
    itsSong_3._setItsPlaylist2_2(this);
}

void Playlist2::cleanUpRelations() {
    {
        OMIterator<Song*> iter(itsSong);
        while (*iter){
            Song* current = *iter;
            if(current != NULL)
                {
                    current->_removeItsPlaylist2_3(this);
                }
            iter++;
        }
        itsSong.removeAll();
    }
    if(itsSong_2 != NULL)
        {
            Playlist2* p_Playlist2 = itsSong_2->getItsPlaylist2();
            if(p_Playlist2 != NULL)
                {
                    itsSong_2->__setItsPlaylist2(NULL);
                }
            itsSong_2 = NULL;
        }
}

void Playlist2::_addItsSong(Song* p_Song) {
    itsSong.add(p_Song);
}

void Playlist2::_removeItsSong(Song* p_Song) {
    itsSong.remove(p_Song);
}

void Playlist2::_clearItsSong() {
    itsSong.removeAll();
}

void Playlist2::__setItsSong_2(Song* p_Song) {
    itsSong_2 = p_Song;
}

void Playlist2::_setItsSong_2(Song* p_Song) {
    if(itsSong_2 != NULL)
        {
            itsSong_2->__setItsPlaylist2(NULL);
        }
    __setItsSong_2(p_Song);
}

void Playlist2::_clearItsSong_2() {
    itsSong_2 = NULL;
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Playlist2.cpp
*********************************************************************/
