/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Playlist
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Playlist.cpp
*********************************************************************/

//## auto_generated
#include "Playlist.h"
//## package Library

//## class Playlist
Playlist::Playlist() {
    itsSong_2 = NULL;
    initRelations();
}

Playlist::~Playlist() {
    cleanUpRelations();
}

void Playlist::AddSong() {
    //#[ operation AddSong()
    //#]
}

void Playlist::EditPlaylistName() {
    //#[ operation EditPlaylistName()
    //#]
}

void Playlist::RemoveSong() {
    //#[ operation RemoveSong()
    //#]
}

int Playlist::getNumberOfSongs() const {
    return NumberOfSongs;
}

void Playlist::setNumberOfSongs(int p_NumberOfSongs) {
    NumberOfSongs = p_NumberOfSongs;
}

char Playlist::getPlaylistName() const {
    return PlaylistName;
}

void Playlist::setPlaylistName(char p_PlaylistName) {
    PlaylistName = p_PlaylistName;
}

OMIterator<Song*> Playlist::getItsSong() const {
    OMIterator<Song*> iter(itsSong);
    return iter;
}

void Playlist::addItsSong(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_addItsPlaylist_3(this);
        }
    _addItsSong(p_Song);
}

void Playlist::removeItsSong(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_removeItsPlaylist_3(this);
        }
    _removeItsSong(p_Song);
}

void Playlist::clearItsSong() {
    OMIterator<Song*> iter(itsSong);
    while (*iter){
        Song* current = *iter;
        if(current != NULL)
            {
                current->_removeItsPlaylist_3(this);
            }
        iter++;
    }
    _clearItsSong();
}

int Playlist::getItsSong_1() const {
    int iter = 0;
    return iter;
}

Song* Playlist::getItsSong_2() const {
    return itsSong_2;
}

void Playlist::setItsSong_2(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_setItsPlaylist(this);
        }
    _setItsSong_2(p_Song);
}

Song* Playlist::getItsSong_3() const {
    return (Song*) &itsSong_3;
}

void Playlist::initRelations() {
    {
        int iter = 0;
        while (iter < n){
            ((Song*)&itsSong_1[iter])->_setItsPlaylist_2(this);
            iter++;
        }
    }
    itsSong_3._setItsPlaylist_1(this);
}

void Playlist::cleanUpRelations() {
    {
        OMIterator<Song*> iter(itsSong);
        while (*iter){
            Song* current = *iter;
            if(current != NULL)
                {
                    current->_removeItsPlaylist_3(this);
                }
            iter++;
        }
        itsSong.removeAll();
    }
    if(itsSong_2 != NULL)
        {
            Playlist* p_Playlist = itsSong_2->getItsPlaylist();
            if(p_Playlist != NULL)
                {
                    itsSong_2->__setItsPlaylist(NULL);
                }
            itsSong_2 = NULL;
        }
}

void Playlist::_addItsSong(Song* p_Song) {
    itsSong.add(p_Song);
}

void Playlist::_removeItsSong(Song* p_Song) {
    itsSong.remove(p_Song);
}

void Playlist::_clearItsSong() {
    itsSong.removeAll();
}

void Playlist::__setItsSong_2(Song* p_Song) {
    itsSong_2 = p_Song;
}

void Playlist::_setItsSong_2(Song* p_Song) {
    if(itsSong_2 != NULL)
        {
            itsSong_2->__setItsPlaylist(NULL);
        }
    __setItsSong_2(p_Song);
}

void Playlist::_clearItsSong_2() {
    itsSong_2 = NULL;
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Playlist.cpp
*********************************************************************/
