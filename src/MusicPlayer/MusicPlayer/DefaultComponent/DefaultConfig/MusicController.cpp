/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MusicController
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\MusicController.cpp
*********************************************************************/

//## auto_generated
#include "MusicController.h"
//## package MusicPlayer

//## class MusicController
MusicController::MusicController() {
}

MusicController::~MusicController() {
}

void MusicController::AdjustVolume() {
    //#[ operation AdjustVolume()
    //#]
}

void MusicController::Next() {
    //#[ operation Next()
    //#]
}

void MusicController::Pause() {
    //#[ operation Pause()
    //#]
}

void MusicController::Play() {
    //#[ operation Play()
    //#]
}

void MusicController::Previous() {
    //#[ operation Previous()
    //#]
}

void MusicController::Repeat() {
    //#[ operation Repeat()
    //#]
}

void MusicController::Shuffle() {
    //#[ operation Shuffle()
    //#]
}

int MusicController::getVolumeLevel() const {
    return VolumeLevel;
}

void MusicController::setVolumeLevel(int p_VolumeLevel) {
    VolumeLevel = p_VolumeLevel;
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\MusicController.cpp
*********************************************************************/
