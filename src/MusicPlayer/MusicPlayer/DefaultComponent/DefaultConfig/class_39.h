/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_39
//!	Generated Date	: Thu, 19, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_39.h
*********************************************************************/

#ifndef class_39_H
#define class_39_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_39
class class_39 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_39();
    
    //## auto_generated
    ~class_39();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_39.h
*********************************************************************/
