/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Song2
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\Song2.h
*********************************************************************/

#ifndef Song2_H
#define Song2_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <oxf\omcollec.h>
//## link itsPlaylist2
class Playlist2;

//## package MusicPlayer

//## class Song2
class Song2 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Song2();
    
    //## auto_generated
    ~Song2();
    
    ////    Operations    ////
    
    //## operation Information()
    void Information();
    
    ////    Additional operations    ////
    
    //## auto_generated
    char getAlbum() const;
    
    //## auto_generated
    void setAlbum(char p_Album);
    
    //## auto_generated
    char getArtist() const;
    
    //## auto_generated
    void setArtist(char p_Artist);
    
    //## auto_generated
    int getLength() const;
    
    //## auto_generated
    void setLength(int p_Length);
    
    //## auto_generated
    char getTitle() const;
    
    //## auto_generated
    void setTitle(char p_Title);
    
    //## auto_generated
    Playlist2* getItsPlaylist2() const;
    
    //## auto_generated
    void setItsPlaylist2(Playlist2* p_Playlist2);
    
    //## auto_generated
    Playlist2* getItsPlaylist2_1() const;
    
    //## auto_generated
    void setItsPlaylist2_1(Playlist2* p_Playlist2);
    
    //## auto_generated
    Playlist2* getItsPlaylist2_2() const;
    
    //## auto_generated
    void setItsPlaylist2_2(Playlist2* p_Playlist2);
    
    //## auto_generated
    OMIterator<Playlist2*> getItsPlaylist2_3() const;
    
    //## auto_generated
    void addItsPlaylist2_3(Playlist2* p_Playlist2);
    
    //## auto_generated
    void removeItsPlaylist2_3(Playlist2* p_Playlist2);
    
    //## auto_generated
    void clearItsPlaylist2_3();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    char Album;		//## attribute Album
    
    char Artist;		//## attribute Artist
    
    int Length;		//## attribute Length
    
    char Title;		//## attribute Title
    
    ////    Relations and components    ////
    
    Playlist2* itsPlaylist2;		//## link itsPlaylist2
    
    Playlist2* itsPlaylist2_1;		//## link itsPlaylist2_1
    
    Playlist2* itsPlaylist2_2;		//## link itsPlaylist2_2
    
    OMCollection<Playlist2*> itsPlaylist2_3;		//## link itsPlaylist2_3
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsPlaylist2(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _setItsPlaylist2(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _clearItsPlaylist2();
    
    //## auto_generated
    void __setItsPlaylist2_1(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _setItsPlaylist2_1(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _clearItsPlaylist2_1();
    
    //## auto_generated
    void __setItsPlaylist2_2(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _setItsPlaylist2_2(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _clearItsPlaylist2_2();
    
    //## auto_generated
    void _addItsPlaylist2_3(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _removeItsPlaylist2_3(Playlist2* p_Playlist2);
    
    //## auto_generated
    void _clearItsPlaylist2_3();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Song2.h
*********************************************************************/
