/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_33
//!	Generated Date	: Wed, 18, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\class_33.h
*********************************************************************/

#ifndef class_33_H
#define class_33_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## class class_33
class class_33 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_33();
    
    //## auto_generated
    ~class_33();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\class_33.h
*********************************************************************/
