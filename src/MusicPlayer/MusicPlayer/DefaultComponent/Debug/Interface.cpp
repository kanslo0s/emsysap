/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Interface
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Interface.cpp
*********************************************************************/

//## auto_generated
#include "Interface.h"
//## link itsPlayerState
#include "PlayerState.h"
//## package Control

//## class Interface
Interface::Interface() {
    itsPlayerState = NULL;
    //#[ operation Interface()
    //#]
}

Interface::~Interface() {
    cleanUpRelations();
}

void Interface::AdjustVolume() {
    //#[ operation AdjustVolume()
    //#]
}

void Interface::Next() {
    //#[ operation Next()
    //#]
}

void Interface::Pause() {
    //#[ operation Pause()
    //#]
}

void Interface::Play() {
    //#[ operation Play()
    //#]
}

void Interface::Previous() {
    //#[ operation Previous()
    //#]
}

void Interface::Repeat() {
    //#[ operation Repeat()
    //#]
}

void Interface::Shuffle() {
    //#[ operation Shuffle()
    //#]
}

char Interface::getSelectedPlaylist() const {
    return SelectedPlaylist;
}

void Interface::setSelectedPlaylist(char p_SelectedPlaylist) {
    SelectedPlaylist = p_SelectedPlaylist;
}

char Interface::getSelectedSong() const {
    return SelectedSong;
}

void Interface::setSelectedSong(char p_SelectedSong) {
    SelectedSong = p_SelectedSong;
}

int Interface::getVolume() const {
    return Volume;
}

void Interface::setVolume(int p_Volume) {
    Volume = p_Volume;
}

PlayerState* Interface::getItsPlayerState() const {
    return itsPlayerState;
}

void Interface::setItsPlayerState(PlayerState* p_PlayerState) {
    itsPlayerState = p_PlayerState;
}

void Interface::cleanUpRelations() {
    if(itsPlayerState != NULL)
        {
            itsPlayerState = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\Interface.cpp
*********************************************************************/
