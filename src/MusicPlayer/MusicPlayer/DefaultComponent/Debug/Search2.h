/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Search2
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Search2.h
*********************************************************************/

#ifndef Search2_H
#define Search2_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## package Library

//## class Search2
class Search2 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Search2();
    
    //## auto_generated
    ~Search2();
    
    ////    Operations    ////
    
    //## operation Keyboard()
    void Keyboard();
    
    //## operation Voice()
    void Voice();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\Search2.h
*********************************************************************/
