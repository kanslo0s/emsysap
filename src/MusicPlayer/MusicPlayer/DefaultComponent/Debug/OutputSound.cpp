/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: OutputSound
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\OutputSound.cpp
*********************************************************************/

//## auto_generated
#include "OutputSound.h"
//## package Output

//## class OutputSound
OutputSound::OutputSound() {
    //#[ operation OutputSound()
    //#]
}

OutputSound::~OutputSound() {
}

int OutputSound::getVolume() const {
    return Volume;
}

void OutputSound::setVolume(int p_Volume) {
    Volume = p_Volume;
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\OutputSound.cpp
*********************************************************************/
