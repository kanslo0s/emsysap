/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Interface
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Interface.h
*********************************************************************/

#ifndef Interface_H
#define Interface_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## link itsPlayerState
class PlayerState;

//## package Control

//## class Interface
class Interface {
    ////    Constructors and destructors    ////
    
public :

    //## operation Interface()
    Interface();
    
    //## auto_generated
    ~Interface();
    
    ////    Operations    ////
    
    //## operation AdjustVolume()
    void AdjustVolume();
    
    //## operation Next()
    void Next();
    
    //## operation Pause()
    void Pause();
    
    //## operation Play()
    void Play();
    
    //## operation Previous()
    void Previous();
    
    //## operation Repeat()
    void Repeat();
    
    //## operation Shuffle()
    void Shuffle();
    
    ////    Additional operations    ////
    
    //## auto_generated
    char getSelectedPlaylist() const;
    
    //## auto_generated
    void setSelectedPlaylist(char p_SelectedPlaylist);
    
    //## auto_generated
    char getSelectedSong() const;
    
    //## auto_generated
    void setSelectedSong(char p_SelectedSong);
    
    //## auto_generated
    int getVolume() const;
    
    //## auto_generated
    void setVolume(int p_Volume);
    
    //## auto_generated
    PlayerState* getItsPlayerState() const;
    
    //## auto_generated
    void setItsPlayerState(PlayerState* p_PlayerState);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    char SelectedPlaylist;		//## attribute SelectedPlaylist
    
    char SelectedSong;		//## attribute SelectedSong
    
    int Volume;		//## attribute Volume
    
    ////    Relations and components    ////
    
    PlayerState* itsPlayerState;		//## link itsPlayerState
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\Interface.h
*********************************************************************/
