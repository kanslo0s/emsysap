/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Decoder
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Decoder.cpp
*********************************************************************/

//## auto_generated
#include "Decoder.h"
//## link itsOutputSound
#include "OutputSound.h"
//## package MusicStream

//## class Decoder
Decoder::Decoder() {
    itsOutputSound = NULL;
    //#[ operation Decoder()
    //#]
}

Decoder::~Decoder() {
    cleanUpRelations();
}

OutputSound* Decoder::getItsOutputSound() const {
    return itsOutputSound;
}

void Decoder::setItsOutputSound(OutputSound* p_OutputSound) {
    itsOutputSound = p_OutputSound;
}

void Decoder::cleanUpRelations() {
    if(itsOutputSound != NULL)
        {
            itsOutputSound = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\Decoder.cpp
*********************************************************************/
