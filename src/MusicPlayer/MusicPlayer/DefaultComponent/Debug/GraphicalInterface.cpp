/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: GraphicalInterface
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\GraphicalInterface.cpp
*********************************************************************/

//## auto_generated
#include "GraphicalInterface.h"
//## auto_generated
#include "PlayerState.h"
//## package Control

//## class GraphicalInterface
GraphicalInterface::GraphicalInterface() {
    //#[ operation GraphicalInterface()
    //#]
}

GraphicalInterface::~GraphicalInterface() {
}

void GraphicalInterface::AdjustVolume() {
    //#[ operation AdjustVolume()
    //#]
}

void GraphicalInterface::Next() {
    //#[ operation Next()
    //#]
}

void GraphicalInterface::Pause() {
    //#[ operation Pause()
    //#]
}

void GraphicalInterface::Play() {
    //#[ operation Play()
    //#]
}

void GraphicalInterface::Previous() {
    //#[ operation Previous()
    //#]
}

void GraphicalInterface::Repeat() {
    //#[ operation Repeat()
    //#]
}

void GraphicalInterface::Shuffle() {
    //#[ operation Shuffle()
    //#]
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\GraphicalInterface.cpp
*********************************************************************/
