/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: ConsoleInterface
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\ConsoleInterface.cpp
*********************************************************************/

//## auto_generated
#include "ConsoleInterface.h"
//## auto_generated
#include "PlayerState.h"
//## package Control

//## class ConsoleInterface
ConsoleInterface::ConsoleInterface() {
    //#[ operation ConsoleInterface()
    //#]
}

ConsoleInterface::~ConsoleInterface() {
}

void ConsoleInterface::AdjustVolume() {
    //#[ operation AdjustVolume()
    //#]
}

void ConsoleInterface::Next() {
    //#[ operation Next()
    //#]
}

void ConsoleInterface::Pause() {
    //#[ operation Pause()
    //#]
}

void ConsoleInterface::Play() {
    //#[ operation Play()
    //#]
}

void ConsoleInterface::Previous() {
    //#[ operation Previous()
    //#]
}

void ConsoleInterface::Repeat() {
    //#[ operation Repeat()
    //#]
}

void ConsoleInterface::Shuffle() {
    //#[ operation Shuffle()
    //#]
}

void ConsoleInterface::parseInput() {
    //#[ operation parseInput()
    //#]
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\ConsoleInterface.cpp
*********************************************************************/
