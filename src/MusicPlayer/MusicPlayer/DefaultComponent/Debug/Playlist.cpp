/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Playlist
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Playlist.cpp
*********************************************************************/

//## auto_generated
#include "Playlist.h"
//## package Library

//## class Playlist
Playlist::Playlist() {
    itsSong_2 = NULL;
    initRelations();
    //#[ operation Playlist()
    //#]
}

Playlist::~Playlist() {
    cleanUpRelations();
}

void Playlist::AddSong() {
    //#[ operation AddSong()
    //#]
}

void Playlist::Create() {
    //#[ operation Create()
    //#]
}

void Playlist::EditName() {
    //#[ operation EditName()
    //#]
}

void Playlist::RemoveSong() {
    //#[ operation RemoveSong()
    //#]
}

char Playlist::getName() const {
    return Name;
}

void Playlist::setName(char p_Name) {
    Name = p_Name;
}

int Playlist::getNumberOfSongs() const {
    return NumberOfSongs;
}

void Playlist::setNumberOfSongs(int p_NumberOfSongs) {
    NumberOfSongs = p_NumberOfSongs;
}

int Playlist::getTotalPlayTime() const {
    return TotalPlayTime;
}

void Playlist::setTotalPlayTime(int p_TotalPlayTime) {
    TotalPlayTime = p_TotalPlayTime;
}

OMIterator<Song*> Playlist::getItsSong() const {
    OMIterator<Song*> iter(itsSong);
    return iter;
}

void Playlist::addItsSong(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_addItsPlaylist_3(this);
        }
    _addItsSong(p_Song);
}

void Playlist::removeItsSong(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_removeItsPlaylist_3(this);
        }
    _removeItsSong(p_Song);
}

void Playlist::clearItsSong() {
    OMIterator<Song*> iter(itsSong);
    while (*iter){
        Song* current = *iter;
        if(current != NULL)
            {
                current->_removeItsPlaylist_3(this);
            }
        iter++;
    }
    _clearItsSong();
}

OMIterator<Song*> Playlist::getItsSong_1() const {
    OMIterator<Song*> iter(itsSong_1);
    return iter;
}

Song* Playlist::newItsSong_1() {
    Song* newSong = new Song;
    newSong->_setItsPlaylist_2(this);
    itsSong_1.add(newSong);
    return newSong;
}

void Playlist::deleteItsSong_1(Song* p_Song) {
    p_Song->_setItsPlaylist_2(NULL);
    itsSong_1.remove(p_Song);
    delete p_Song;
}

Song* Playlist::getItsSong_2() const {
    return itsSong_2;
}

void Playlist::setItsSong_2(Song* p_Song) {
    if(p_Song != NULL)
        {
            p_Song->_setItsPlaylist(this);
        }
    _setItsSong_2(p_Song);
}

Song* Playlist::getItsSong_3() const {
    return (Song*) &itsSong_3;
}

void Playlist::initRelations() {
    itsSong_3._setItsPlaylist_1(this);
}

void Playlist::cleanUpRelations() {
    {
        OMIterator<Song*> iter(itsSong_1);
        while (*iter){
            deleteItsSong_1(*iter);
            iter.reset();
        }
    }
    {
        OMIterator<Song*> iter(itsSong);
        while (*iter){
            Song* current = *iter;
            if(current != NULL)
                {
                    current->_removeItsPlaylist_3(this);
                }
            iter++;
        }
        itsSong.removeAll();
    }
    if(itsSong_2 != NULL)
        {
            Playlist* p_Playlist = itsSong_2->getItsPlaylist();
            if(p_Playlist != NULL)
                {
                    itsSong_2->__setItsPlaylist(NULL);
                }
            itsSong_2 = NULL;
        }
}

void Playlist::_addItsSong(Song* p_Song) {
    itsSong.add(p_Song);
}

void Playlist::_removeItsSong(Song* p_Song) {
    itsSong.remove(p_Song);
}

void Playlist::_clearItsSong() {
    itsSong.removeAll();
}

void Playlist::_addItsSong_1(Song* p_Song) {
    itsSong_1.add(p_Song);
}

void Playlist::_removeItsSong_1(Song* p_Song) {
    itsSong_1.remove(p_Song);
}

void Playlist::__setItsSong_2(Song* p_Song) {
    itsSong_2 = p_Song;
}

void Playlist::_setItsSong_2(Song* p_Song) {
    if(itsSong_2 != NULL)
        {
            itsSong_2->__setItsPlaylist(NULL);
        }
    __setItsSong_2(p_Song);
}

void Playlist::_clearItsSong_2() {
    itsSong_2 = NULL;
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\Playlist.cpp
*********************************************************************/
