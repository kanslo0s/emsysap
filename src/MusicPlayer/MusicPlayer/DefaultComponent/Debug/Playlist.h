/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Playlist
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Playlist.h
*********************************************************************/

#ifndef Playlist_H
#define Playlist_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## auto_generated
#include <oxf\omcollec.h>
//## auto_generated
#include <oxf\omlist.h>
//## link itsSong_3
#include "Song.h"
//## package Library

//## class Playlist
class Playlist {
    ////    Constructors and destructors    ////
    
public :

    //## operation Playlist()
    Playlist();
    
    //## auto_generated
    ~Playlist();
    
    ////    Operations    ////
    
    //## operation AddSong()
    void AddSong();
    
    //## operation Create()
    void Create();
    
    //## operation EditName()
    void EditName();
    
    //## operation RemoveSong()
    void RemoveSong();
    
    ////    Additional operations    ////
    
    //## auto_generated
    char getName() const;
    
    //## auto_generated
    void setName(char p_Name);
    
    //## auto_generated
    int getNumberOfSongs() const;
    
    //## auto_generated
    void setNumberOfSongs(int p_NumberOfSongs);
    
    //## auto_generated
    int getTotalPlayTime() const;
    
    //## auto_generated
    void setTotalPlayTime(int p_TotalPlayTime);
    
    //## auto_generated
    OMIterator<Song*> getItsSong() const;
    
    //## auto_generated
    void addItsSong(Song* p_Song);
    
    //## auto_generated
    void removeItsSong(Song* p_Song);
    
    //## auto_generated
    void clearItsSong();
    
    //## auto_generated
    OMIterator<Song*> getItsSong_1() const;
    
    //## auto_generated
    Song* newItsSong_1();
    
    //## auto_generated
    void deleteItsSong_1(Song* p_Song);
    
    //## auto_generated
    Song* getItsSong_2() const;
    
    //## auto_generated
    void setItsSong_2(Song* p_Song);
    
    //## auto_generated
    Song* getItsSong_3() const;

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    char Name;		//## attribute Name
    
    int NumberOfSongs;		//## attribute NumberOfSongs
    
    int TotalPlayTime;		//## attribute TotalPlayTime
    
    ////    Relations and components    ////
    
    OMCollection<Song*> itsSong;		//## link itsSong
    
    OMList<Song*> itsSong_1;		//## link itsSong_1
    
    Song* itsSong_2;		//## link itsSong_2
    
    Song itsSong_3;		//## link itsSong_3
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void _addItsSong(Song* p_Song);
    
    //## auto_generated
    void _removeItsSong(Song* p_Song);
    
    //## auto_generated
    void _clearItsSong();
    
    //## auto_generated
    void _addItsSong_1(Song* p_Song);
    
    //## auto_generated
    void _removeItsSong_1(Song* p_Song);
    
    //## auto_generated
    void __setItsSong_2(Song* p_Song);
    
    //## auto_generated
    void _setItsSong_2(Song* p_Song);
    
    //## auto_generated
    void _clearItsSong_2();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\Playlist.h
*********************************************************************/
