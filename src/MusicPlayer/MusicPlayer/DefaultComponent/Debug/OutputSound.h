/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: OutputSound
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\OutputSound.h
*********************************************************************/

#ifndef OutputSound_H
#define OutputSound_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## package Output

//## class OutputSound
class OutputSound {
    ////    Constructors and destructors    ////
    
public :

    //## operation OutputSound()
    OutputSound();
    
    //## auto_generated
    ~OutputSound();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getVolume() const;
    
    //## auto_generated
    void setVolume(int p_Volume);
    
    ////    Attributes    ////

protected :

    int Volume;		//## attribute Volume
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\OutputSound.h
*********************************************************************/
