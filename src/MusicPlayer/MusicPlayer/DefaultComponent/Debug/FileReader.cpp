/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: FileReader
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\FileReader.cpp
*********************************************************************/

//## auto_generated
#include "FileReader.h"
//## link itsDecoder
#include "Decoder.h"
//## package MusicStream

//## class FileReader
FileReader::FileReader() {
    itsDecoder = NULL;
    //#[ operation FileReader()
    //#]
}

FileReader::~FileReader() {
    cleanUpRelations();
}

char* FileReader::getFilename() const {
    return filename;
}

void FileReader::setFilename(char* p_filename) {
    filename = p_filename;
}

Decoder* FileReader::getItsDecoder() const {
    return itsDecoder;
}

void FileReader::setItsDecoder(Decoder* p_Decoder) {
    itsDecoder = p_Decoder;
}

void FileReader::cleanUpRelations() {
    if(itsDecoder != NULL)
        {
            itsDecoder = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\FileReader.cpp
*********************************************************************/
