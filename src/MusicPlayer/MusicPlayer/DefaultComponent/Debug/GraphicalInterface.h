/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: GraphicalInterface
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\GraphicalInterface.h
*********************************************************************/

#ifndef GraphicalInterface_H
#define GraphicalInterface_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## class GraphicalInterface
#include "Interface.h"
//## auto_generated
class PlayerState;

//## package Control

//## class GraphicalInterface
class GraphicalInterface : public Interface {
    ////    Constructors and destructors    ////
    
public :

    //## operation GraphicalInterface()
    GraphicalInterface();
    
    //## auto_generated
    ~GraphicalInterface();
    
    ////    Operations    ////
    
    //## operation AdjustVolume()
    void AdjustVolume();
    
    //## operation Next()
    void Next();
    
    //## operation Pause()
    void Pause();
    
    //## operation Play()
    void Play();
    
    //## operation Previous()
    void Previous();
    
    //## operation Repeat()
    void Repeat();
    
    //## operation Shuffle()
    void Shuffle();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\GraphicalInterface.h
*********************************************************************/
