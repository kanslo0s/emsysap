/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Debug
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\MainDefaultComponent.cpp
*********************************************************************/

//## auto_generated
#include "MainDefaultComponent.h"
//## auto_generated
#include "PlayerState.h"
int main(int argc, char* argv[]) {
    int status = 0;
    if(OXF::initialize())
        {
            PlayerState * p_PlayerState;
            p_PlayerState = new PlayerState;
            //#[ configuration DefaultComponent::Debug 
            //#]
            OXF::start();
            delete p_PlayerState;
            status = 0;
        }
    else
        {
            status = 1;
        }
    return status;
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\MainDefaultComponent.cpp
*********************************************************************/
