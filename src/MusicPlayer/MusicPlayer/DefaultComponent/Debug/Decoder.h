/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Decoder
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Decoder.h
*********************************************************************/

#ifndef Decoder_H
#define Decoder_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## link itsOutputSound
class OutputSound;

//## package MusicStream

//## class Decoder
class Decoder {
    ////    Constructors and destructors    ////
    
public :

    //## operation Decoder()
    Decoder();
    
    //## auto_generated
    ~Decoder();
    
    ////    Additional operations    ////
    
    //## auto_generated
    OutputSound* getItsOutputSound() const;
    
    //## auto_generated
    void setItsOutputSound(OutputSound* p_OutputSound);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    OutputSound* itsOutputSound;		//## link itsOutputSound
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\Decoder.h
*********************************************************************/
