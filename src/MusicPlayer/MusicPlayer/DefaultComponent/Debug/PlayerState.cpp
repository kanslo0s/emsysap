/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: PlayerState
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\PlayerState.cpp
*********************************************************************/

//## auto_generated
#include "PlayerState.h"
//## link itsFileReader
#include "FileReader.h"
//## link itsOutputSound
#include "OutputSound.h"
//## package PlayerStateMachine

//## class PlayerState
PlayerState::PlayerState() {
    itsFileReader = NULL;
    itsOutputSound = NULL;
    itsPlaylist = NULL;
    //#[ operation PlayerState()
    //#]
}

PlayerState::~PlayerState() {
    cleanUpRelations();
}

Playlist* PlayerState::getCurrentPlaylist() const {
    return (Playlist*) &CurrentPlaylist;
}

Song* PlayerState::getCurrentSong() const {
    return (Song*) &CurrentSong;
}

bool PlayerState::getPlaying() const {
    return Playing;
}

void PlayerState::setPlaying(bool p_Playing) {
    Playing = p_Playing;
}

bool PlayerState::getShuffleOn() const {
    return ShuffleOn;
}

void PlayerState::setShuffleOn(bool p_ShuffleOn) {
    ShuffleOn = p_ShuffleOn;
}

int PlayerState::getVolume() const {
    return Volume;
}

void PlayerState::setVolume(int p_Volume) {
    Volume = p_Volume;
}

FileReader* PlayerState::getItsFileReader() const {
    return itsFileReader;
}

void PlayerState::setItsFileReader(FileReader* p_FileReader) {
    itsFileReader = p_FileReader;
}

OutputSound* PlayerState::getItsOutputSound() const {
    return itsOutputSound;
}

void PlayerState::setItsOutputSound(OutputSound* p_OutputSound) {
    itsOutputSound = p_OutputSound;
}

Playlist* PlayerState::getItsPlaylist() const {
    return itsPlaylist;
}

void PlayerState::setItsPlaylist(Playlist* p_Playlist) {
    itsPlaylist = p_Playlist;
}

void PlayerState::cleanUpRelations() {
    if(itsFileReader != NULL)
        {
            itsFileReader = NULL;
        }
    if(itsOutputSound != NULL)
        {
            itsOutputSound = NULL;
        }
    if(itsPlaylist != NULL)
        {
            itsPlaylist = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\PlayerState.cpp
*********************************************************************/
