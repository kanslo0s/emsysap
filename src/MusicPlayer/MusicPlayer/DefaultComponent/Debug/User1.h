/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: User1
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\User1.h
*********************************************************************/

#ifndef User1_H
#define User1_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## package UseCasePkg

//## actor User1
class User1 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    User1();
    
    //## auto_generated
    ~User1();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\User1.h
*********************************************************************/
