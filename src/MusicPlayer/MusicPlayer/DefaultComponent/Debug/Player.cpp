/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: Player
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\Player.cpp
*********************************************************************/

//## auto_generated
#include "Player.h"
//## link itsFileReader
#include "FileReader.h"
//## link itsOutputSound
#include "OutputSound.h"
//## package PlayerStateMachine

//## class Player
Player::Player() {
    itsFileReader = NULL;
    itsOutputSound = NULL;
    itsPlaylist = NULL;
}

Player::~Player() {
    cleanUpRelations();
}

Playlist* Player::getCurrentPlaylist() const {
    return (Playlist*) &CurrentPlaylist;
}

Song* Player::getCurrentSong() const {
    return (Song*) &CurrentSong;
}

bool Player::getPlaying() const {
    return Playing;
}

void Player::setPlaying(bool p_Playing) {
    Playing = p_Playing;
}

bool Player::getShuffleOn() const {
    return ShuffleOn;
}

void Player::setShuffleOn(bool p_ShuffleOn) {
    ShuffleOn = p_ShuffleOn;
}

int Player::getVolume() const {
    return Volume;
}

void Player::setVolume(int p_Volume) {
    Volume = p_Volume;
}

FileReader* Player::getItsFileReader() const {
    return itsFileReader;
}

void Player::setItsFileReader(FileReader* p_FileReader) {
    itsFileReader = p_FileReader;
}

OutputSound* Player::getItsOutputSound() const {
    return itsOutputSound;
}

void Player::setItsOutputSound(OutputSound* p_OutputSound) {
    itsOutputSound = p_OutputSound;
}

Playlist* Player::getItsPlaylist() const {
    return itsPlaylist;
}

void Player::setItsPlaylist(Playlist* p_Playlist) {
    itsPlaylist = p_Playlist;
}

void Player::cleanUpRelations() {
    if(itsFileReader != NULL)
        {
            itsFileReader = NULL;
        }
    if(itsOutputSound != NULL)
        {
            itsOutputSound = NULL;
        }
    if(itsPlaylist != NULL)
        {
            itsPlaylist = NULL;
        }
}

/*********************************************************************
	File Path	: DefaultComponent\Debug\Player.cpp
*********************************************************************/
