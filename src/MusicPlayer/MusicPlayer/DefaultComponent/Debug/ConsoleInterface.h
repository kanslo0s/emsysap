/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: ConsoleInterface
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\ConsoleInterface.h
*********************************************************************/

#ifndef ConsoleInterface_H
#define ConsoleInterface_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## class ConsoleInterface
#include "Interface.h"
//## auto_generated
class PlayerState;

//## package Control

//## class ConsoleInterface
class ConsoleInterface : public Interface {
    ////    Constructors and destructors    ////
    
public :

    //## operation ConsoleInterface()
    ConsoleInterface();
    
    //## auto_generated
    ~ConsoleInterface();
    
    ////    Operations    ////
    
    //## operation AdjustVolume()
    void AdjustVolume();
    
    //## operation Next()
    void Next();
    
    //## operation Pause()
    void Pause();
    
    //## operation Play()
    void Play();
    
    //## operation Previous()
    void Previous();
    
    //## operation Repeat()
    void Repeat();
    
    //## operation Shuffle()
    void Shuffle();
    
    //## operation parseInput()
    void parseInput();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\ConsoleInterface.h
*********************************************************************/
