/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: Debug
	Model Element	: FileReader
//!	Generated Date	: Thu, 26, Nov 2015  
	File Path	: DefaultComponent\Debug\FileReader.h
*********************************************************************/

#ifndef FileReader_H
#define FileReader_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "D:\Git\EmsysAP\dloads\bass24\c\bass.h"
//## link itsDecoder
class Decoder;

//## package MusicStream

//## class FileReader
class FileReader {
    ////    Constructors and destructors    ////
    
public :

    //## operation FileReader()
    FileReader();
    
    //## auto_generated
    ~FileReader();
    
    ////    Additional operations    ////
    
    //## auto_generated
    char* getFilename() const;
    
    //## auto_generated
    void setFilename(char* p_filename);
    
    //## auto_generated
    Decoder* getItsDecoder() const;
    
    //## auto_generated
    void setItsDecoder(Decoder* p_Decoder);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    char* filename;		//## attribute filename
    
    ////    Relations and components    ////
    
    Decoder* itsDecoder;		//## link itsDecoder
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\Debug\FileReader.h
*********************************************************************/
