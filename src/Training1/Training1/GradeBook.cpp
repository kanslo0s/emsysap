// Fig. 5.10: GradeBook.cpp
// Member-function definitions for class GradeBook that
// uses a switch statement to count A, B, C, D and F grades.
#include "stdafx.h"
#include <iostream>
#include "GradeBook.h" // include definition of class GradeBook


using namespace std;

// constructor initializes courseName with string supplied as argument;
// initializes counter data members to 0
GradeBook::GradeBook( string name )
   : aCount( 0 ), // initialize count of A grades to 0 
     bCount( 0 ), // initialize count of B grades to 0
     cCount( 0 ), // initialize count of C grades to 0
     dCount( 0 ), // initialize count of D grades to 0
     fCount( 0 ),  // initialize count of F grades to 0
	 gradePointAverage( 0 ),  // initialize gradePointAverage to 0
	 studentsGraded( 0 )  // initialize gradePointAverage to 0
{
   setCourseName( name );
} // end GradeBook constructor

// function to set the course name; limits name to 25 or fewer characters
void GradeBook::setCourseName( string name )
{
   if ( name.size() <= 25 ) // if name has 25 or fewer characters
      courseName = name; // store the course name in the object
   else // if name is longer than 25 characters
   { // set courseName to first 25 characters of parameter name
      courseName = name.substr( 0, 25 ); // select first 25 characters
      cerr << "Name \"" << name << "\" exceeds maximum length (25).\n"
         << "Limiting courseName to first 25 characters.\n" << endl;
   } // end if...else
} // end function setCourseName

// function to retrieve the course name
string GradeBook::getCourseName() const
{
   return courseName;
} // end function getCourseName



// display a welcome message to the GradeBook user
void GradeBook::displayMessage() const
{
   // this statement calls getCourseName to get the 
   // name of the course this GradeBook represents
   cout << "Welcome to the grade book for\n" << getCourseName() << "!\n" 
      << endl;
} // end function displayMessage

// input arbitrary number of grades from user; update grade counter
void GradeBook::inputGrades() 
{
   int grade; // grade entered by user

   cout << "Enter the letter grades." << endl
      << "Enter the EOF character to end input." << endl;

   // loop until user types end-of-file key sequence
   while ( ( grade = cin.get() ) != EOF ) //ctrl-z when your done
   {
      // determine which grade was entered
      switch ( grade ) // switch statement nested in while
      {
		 
         case 'A': // grade was uppercase A
         case 'a': // or lowercase a
            ++aCount; // increment aCount
			gradePointAverage += 4;
			studentsGraded++;
            break; // necessary to exit switch

         case 'B': // grade was uppercase B
         case 'b': // or lowercase b
            ++bCount; // increment bCount
			gradePointAverage += 3;
			studentsGraded++;
            break; // exit switch

         case 'C': // grade was uppercase C
         case 'c': // or lowercase c
            ++cCount; // increment cCount
			gradePointAverage += 2;
			studentsGraded++;
            break; // exit switch

         case 'D': // grade was uppercase D
         case 'd': // or lowercase d
            ++dCount; // increment dCount 
			gradePointAverage++;
			studentsGraded++;
            break; // exit switch

         case 'F': // grade was uppercase F
         case 'f': // or lowercase f
            ++fCount; // increment fCount
			studentsGraded++;
            break; // exit switch

         case '\n': // ignore newlines,  
         case '\t': // tabs, 
         case ' ': // and spaces in input
            break; // exit switch

         default: // catch all other characters
            cout << "Incorrect letter grade entered."
               << " Enter a new grade." << endl;
            break; // optional; will exit switch anyway
      } // end switch
   } // end while
} // end function inputGrades

// display a report based on the grades entered by user
void GradeBook::displayGradeReport() const
{
   // output summary of results
   cout << "\n\nNumber of students who received each letter grade:" 
      << "\nA: " << aCount // display number of A grades
      << "\nB: " << bCount // display number of B grades
      << "\nC: " << cCount // display number of C grades 
      << "\nD: " << dCount // display number of D grades
      << "\nF: " << fCount // display number of F grades
	  << "\nThe average grade is: " << (gradePointAverage/studentsGraded) // display average number of grades
      << endl;
} // end function displayGradeReport

void GradeBook::sortGradeArray(Student * arrayTemp[], int size)
{
	int smallestIndex = 0; //initialize the minimum as the first element of arrayTemp
	for (int i = 0; i < (size-1); i++) //loop through arrayTemp starting at the current element, where i is the current index
	{
		smallestIndex = i;	//check every element of arrayTemp
		for (int j = i+1; j < size; j++) //loop through arrayTemp starting at the next element, where j is next index of current index.
		{
			//if current average is smaller than previous average than:
			if (arrayTemp[j]->getAverageGrade() < arrayTemp[smallestIndex]->getAverageGrade())
			{	
				smallestIndex = j;  //set smallestIndex to the current index.
			}
		}
		if (i != smallestIndex) { swapGradeArray(arrayTemp[i], arrayTemp[smallestIndex]); } //swap current index (i) with smallest index
	}
}// end function sortGradeArray
// swap values at memory locations to which
// element1Ptr and element2Ptr point
void GradeBook::swapGradeArray(Student * const element1Ptr, Student * const element2Ptr)
{
	Student hold = *element1Ptr; //place current object in hold.
	*element1Ptr = *element2Ptr; //place the object with smallest average in current object
	*element2Ptr = hold; //place current object in object with smallest average.
	// end of swapping
} //end function swapGradeArray

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
