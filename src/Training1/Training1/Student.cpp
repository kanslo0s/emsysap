#include "stdafx.h"
#include <iostream>
#include <vector>
#include <array>
#include "Student.h"
using namespace std;

Student::Student(string name, vector<double> grades)
{
	StudentName = name;	//name of the student
	gradeVector = grades; //student's grades
	AverageGrade = getAverageGrade(); //student's average grade
}

string Student::getStudentName() const
{
	return StudentName;
}

void Student::displayResults(Student* arrayTemp[], int size) const
{
	for (int i = 0; i < size; i++) //loop through the gradeArray
	{
		cout << arrayTemp[i]->getStudentName();	//prints on the terminal the student's name 
		for (int j = 0; j < arrayTemp[i]->gradeVector.size(); j++) //loops through the students grades
		{
			cout << "  " << arrayTemp[i]->gradeVector[j]; //prints on the terminal the grades belonging to the student 
		}
		cout << " Average: " << arrayTemp[i]->getAverageGrade() << endl; //prints on the terminal the average belonging to the student 
	}
} // end function displayResults

double Student::getAverageGrade()
{
	double temp = 0.0;
	for (int i = 0; i < gradeVector.size(); i++) //store the sum of grades in temp
	{
		temp += gradeVector[i];
	}
	
	return (temp / gradeVector.size()); //devide temp by the total amount of grades to get an average
}