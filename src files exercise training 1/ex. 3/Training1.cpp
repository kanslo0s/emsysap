// Fig. 5.11: fig05_11.cpp
// Creating a GradeBook object and calling its member functions.
#include "stdafx.h"
#include <iostream>
#include "GradeBook.h" // include definition of class GradeBook
#include <vector>
#include <Windows.h>
using namespace std;

int main()
{
	static const int numberOfStudents = 5;
	// create GradeBook object
	std::vector<double> grades1 = { 7.2,5.6,8.5 };
	std::vector<double> grades2 = { 3.2,5.6,5.5 };
	std::vector<double> grades3 = { 5,5,5,5,5 };

	Student gradeArrayTester("Henk", grades1);

	Student *gradeArray[numberOfStudents];
	gradeArray[0] = new Student("henki", grades1);
	gradeArray[1] = new Student("kees", grades2);
	gradeArray[2] = new Student("ilise", grades3);
	gradeArray[3] = new Student("dss", grades2);
	gradeArray[4] = new Student("hghg", grades3);
	cout << "Not Sorted:" << endl;
	gradeArrayTester.displayResults(gradeArray); // display welcome message
	GradeBook myGradeBook("CS101 C++ Programming");
	myGradeBook.sortGradeArray(gradeArray, numberOfStudents);
	cout << "\nSorted:" << endl;
	gradeArrayTester.displayResults(gradeArray);
	/*myGradeBook.displayMessage(); // display welcome message
	myGradeBook.inputGrades(); // read grades from user
	myGradeBook.displayGradeReport(); // display report based on grades*/
	Sleep(5000000);

} // end main



  /**************************************************************************
  * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
  * Pearson Education, Inc. All Rights Reserved.                           *
  *                                                                        *
  * DISCLAIMER: The authors and publisher of this book have used their     *
  * best efforts in preparing the book. These efforts include the          *
  * development, research, and testing of the theories and programs        *
  * to determine their effectiveness. The authors and publisher make       *
  * no warranty of any kind, expressed or implied, with regard to these    *
  * programs or to the documentation contained in these books. The authors *
  * and publisher shall not be liable in any event for incidental or       *
  * consequential damages in connection with, or arising out of, the       *
  * furnishing, performance, or use of these programs.                     *
  **************************************************************************/
