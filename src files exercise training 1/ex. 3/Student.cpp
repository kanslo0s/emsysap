#include "stdafx.h"
#include <iostream>
#include <vector>
#include <array>
#include "Student.h"
using namespace std;

Student::Student(string name, vector<double> grades)
{
	StudentName = name;
	gradeVector = grades;
	AverageGrade = getAverageGrade();
}

string Student::getStudentName() const
{
	return StudentName;
}

void Student::displayResults(Student* arrayTemp[]) const
{
	for (int i = 0; i < Student::NumberOfGrades; i++)
	{
		cout << arrayTemp[i]->getStudentName();
		for (int j = 0; j < arrayTemp[i]->gradeVector.size(); j++)
		{
			cout << "  " << arrayTemp[i]->gradeVector[j];
		}
		cout << " Average: " << arrayTemp[i]->getAverageGrade() << endl;
	}
} // end function displayResults

double Student::getAverageGrade()
{
	double temp = 0.0;
	for (int i = 0; i < gradeVector.size(); i++) //tel alle cijfers bij elkaar op en store ze in temp
	{
		temp += gradeVector[i];
	}
	
	return (temp / gradeVector.size()); //deel temp door het aantal cijfers om een gemiddelde te krijgen
}