#include <string> // program uses C++ standard string class
#include <vector> // program uses C++ standard string class
#include <array>
class Student
{
public:
	explicit Student( std::string, std::vector<double>); // initialize students name and grades
	void displayResults(Student* []) const; //display the results of each student
	double getAverageGrade();
	std::string getStudentName() const;

private:
	static const int NumberOfGrades = 5;
	std::string StudentName; // course name for this GradeBook
	std::vector<double> gradeVector; // course name for this GradeBook
	double AverageGrade;
};

