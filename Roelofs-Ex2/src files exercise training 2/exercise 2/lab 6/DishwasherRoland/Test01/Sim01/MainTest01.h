/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Sim01
	Model Element	: Sim01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Sim01\MainTest01.h
*********************************************************************/

#ifndef MainTest01_H
#define MainTest01_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
class Test01 {
    ////    Constructors and destructors    ////
    
public :

    Test01();
};

#endif
/*********************************************************************
	File Path	: Test01\Sim01\MainTest01.h
*********************************************************************/
