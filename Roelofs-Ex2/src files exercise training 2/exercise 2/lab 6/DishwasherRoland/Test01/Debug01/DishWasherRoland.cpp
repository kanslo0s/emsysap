/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: DishWasherRoland
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\DishWasherRoland.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "DishWasherRoland.h"
//## link itsMotor01
#include "Motor01.h"
//#[ ignore
#define Default_DishWasherRoland_DishWasherRoland_SERIALIZE OM_NO_OP

#define Default_DishWasherRoland_isDried01_SERIALIZE OM_NO_OP

#define Default_DishWasherRoland_isInNeedOfService01_SERIALIZE OM_NO_OP

#define Default_DishWasherRoland_isRinsed01_SERIALIZE OM_NO_OP

#define Default_DishWasherRoland_isWashed01_SERIALIZE OM_NO_OP

#define Default_DishWasherRoland_setup01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DishWasherRoland
DishWasherRoland::DishWasherRoland(IOxfActive* theActiveContext) : cycles01(0), dryTime01(0), rinseTime01(0), washTime01(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DishWasherRoland, DishWasherRoland(), 0, Default_DishWasherRoland_DishWasherRoland_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsMotor01 = NULL;
    initStatechart();
}

DishWasherRoland::~DishWasherRoland() {
    NOTIFY_DESTRUCTOR(~DishWasherRoland, true);
    cleanUpRelations();
    cancelTimeouts();
}

bool DishWasherRoland::isDried01() {
    NOTIFY_OPERATION(isDried01, isDried01(), 0, Default_DishWasherRoland_isDried01_SERIALIZE);
    //#[ operation isDried01()
    return (0==dryTime01);
    //#]
}

bool DishWasherRoland::isInNeedOfService01() {
    NOTIFY_OPERATION(isInNeedOfService01, isInNeedOfService01(), 0, Default_DishWasherRoland_isInNeedOfService01_SERIALIZE);
    //#[ operation isInNeedOfService01()
    return (cycles01>MAX_CYCLES01);
    //#]
}

bool DishWasherRoland::isRinsed01() {
    NOTIFY_OPERATION(isRinsed01, isRinsed01(), 0, Default_DishWasherRoland_isRinsed01_SERIALIZE);
    //#[ operation isRinsed01()
    return (0==rinseTime01);
    
    //#]
}

bool DishWasherRoland::isWashed01() {
    NOTIFY_OPERATION(isWashed01, isWashed01(), 0, Default_DishWasherRoland_isWashed01_SERIALIZE);
    //#[ operation isWashed01()
    return (0==washTime01);
    //#]
}

void DishWasherRoland::setup01() {
    NOTIFY_OPERATION(setup01, setup01(), 0, Default_DishWasherRoland_setup01_SERIALIZE);
    //#[ operation setup01()
    if (IS_IN(quick01)){
    rinseTime01=1;
    washTime01=2;
    dryTime01=1;
    } else {
    rinseTime01=4;
    washTime01=5;
    dryTime01=3;
    }
    
    //#]
}

int DishWasherRoland::getCycles01() const {
    return cycles01;
}

void DishWasherRoland::setCycles01(int p_cycles01) {
    cycles01 = p_cycles01;
    NOTIFY_SET_OPERATION;
}

int DishWasherRoland::getDryTime01() const {
    return dryTime01;
}

void DishWasherRoland::setDryTime01(int p_dryTime01) {
    dryTime01 = p_dryTime01;
    NOTIFY_SET_OPERATION;
}

int DishWasherRoland::getRinseTime01() const {
    return rinseTime01;
}

void DishWasherRoland::setRinseTime01(int p_rinseTime01) {
    rinseTime01 = p_rinseTime01;
    NOTIFY_SET_OPERATION;
}

int DishWasherRoland::getWashTime01() const {
    return washTime01;
}

void DishWasherRoland::setWashTime01(int p_washTime01) {
    washTime01 = p_washTime01;
    NOTIFY_SET_OPERATION;
}

bool DishWasherRoland::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void DishWasherRoland::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    service01_subState = OMNonState;
    service01_active = OMNonState;
    running01_subState = OMNonState;
    running01_active = OMNonState;
    on01_subState = OMNonState;
    on01_lastState = OMNonState;
    on01_timeout = NULL;
    mode01_subState = OMNonState;
    mode01_active = OMNonState;
}

void DishWasherRoland::cancelTimeouts() {
    cancel(on01_timeout);
}

bool DishWasherRoland::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(on01_timeout == arg)
        {
            on01_timeout = NULL;
            res = true;
        }
    return res;
}

Motor01* DishWasherRoland::getItsMotor01() const {
    return itsMotor01;
}

void DishWasherRoland::setItsMotor01(Motor01* p_Motor01) {
    itsMotor01 = p_Motor01;
    if(p_Motor01 != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsMotor01", p_Motor01, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsMotor01");
        }
}

void DishWasherRoland::cleanUpRelations() {
    if(itsMotor01 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsMotor01");
            itsMotor01 = NULL;
        }
}

void DishWasherRoland::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        Active01_entDef();
    }
}

IOxfReactive::TakeEventStatus DishWasherRoland::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State Active01
    if(rootState_active == Active01)
        {
            res = Active01_processEvent();
        }
    return res;
}

void DishWasherRoland::Active01_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active01");
    rootState_subState = Active01;
    rootState_active = Active01;
    running01_entDef();
    mode01_entDef();
    service01_entDef();
}

void DishWasherRoland::Active01_exit() {
    switch (running01_subState) {
        // State off01
        case off01:
        {
            NOTIFY_STATE_EXITED("ROOT.Active01.running01.off01");
        }
        break;
        // State open01
        case open01:
        {
            NOTIFY_STATE_EXITED("ROOT.Active01.running01.open01");
        }
        break;
        // State on01
        case on01:
        {
            on01_exit();
        }
        break;
        default:
            break;
    }
    running01_subState = OMNonState;
    NOTIFY_STATE_EXITED("ROOT.Active01.running01");
    switch (mode01_subState) {
        // State quick01
        case quick01:
        {
            NOTIFY_STATE_EXITED("ROOT.Active01.mode01.quick01");
        }
        break;
        // State intensive01
        case intensive01:
        {
            NOTIFY_STATE_EXITED("ROOT.Active01.mode01.intensive01");
        }
        break;
        default:
            break;
    }
    mode01_subState = OMNonState;
    NOTIFY_STATE_EXITED("ROOT.Active01.mode01");
    switch (service01_subState) {
        // State normal01
        case normal01:
        {
            NOTIFY_STATE_EXITED("ROOT.Active01.service01.normal01");
        }
        break;
        // State faulty01
        case faulty01:
        {
            NOTIFY_STATE_EXITED("ROOT.Active01.service01.faulty01");
        }
        break;
        default:
            break;
    }
    service01_subState = OMNonState;
    NOTIFY_STATE_EXITED("ROOT.Active01.service01");
    
    NOTIFY_STATE_EXITED("ROOT.Active01");
}

IOxfReactive::TakeEventStatus DishWasherRoland::Active01_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State running01
    if(running01_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(Active01))
                {
                    return res;
                }
        }
    // State mode01
    if(mode01_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(Active01))
                {
                    return res;
                }
        }
    // State service01
    if(service01_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(Active01))
                {
                    return res;
                }
        }
    
    return res;
}

void DishWasherRoland::service01_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active01.service01");
    NOTIFY_TRANSITION_STARTED("2");
    NOTIFY_STATE_ENTERED("ROOT.Active01.service01.normal01");
    service01_subState = normal01;
    service01_active = normal01;
    //#[ state Active01.service01.normal01.(Entry) 
    cycles01=0;
    //#]
    NOTIFY_TRANSITION_TERMINATED("2");
}

IOxfReactive::TakeEventStatus DishWasherRoland::service01_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (service01_active) {
        // State normal01
        case normal01:
        {
            if(IS_EVENT_TYPE_OF(evService01_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("17");
                    NOTIFY_STATE_EXITED("ROOT.Active01.service01.normal01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.service01.normal01");
                    service01_subState = normal01;
                    service01_active = normal01;
                    //#[ state Active01.service01.normal01.(Entry) 
                    cycles01=0;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("17");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evFault01_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("15");
                    NOTIFY_STATE_EXITED("ROOT.Active01.service01.normal01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.service01.faulty01");
                    service01_subState = faulty01;
                    service01_active = faulty01;
                    NOTIFY_TRANSITION_TERMINATED("15");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State faulty01
        case faulty01:
        {
            if(IS_EVENT_TYPE_OF(evService01_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("16");
                    NOTIFY_STATE_EXITED("ROOT.Active01.service01.faulty01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.service01.normal01");
                    service01_subState = normal01;
                    service01_active = normal01;
                    //#[ state Active01.service01.normal01.(Entry) 
                    cycles01=0;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("16");
                    res = eventConsumed;
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

void DishWasherRoland::running01_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active01.running01");
    NOTIFY_TRANSITION_STARTED("0");
    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.off01");
    running01_subState = off01;
    running01_active = off01;
    NOTIFY_TRANSITION_TERMINATED("0");
}

IOxfReactive::TakeEventStatus DishWasherRoland::running01_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (running01_active) {
        // State off01
        case off01:
        {
            if(IS_EVENT_TYPE_OF(evStart01_Default_id))
                {
                    //## transition 5 
                    if(IS_IN(normal01))
                        {
                            NOTIFY_TRANSITION_STARTED("5");
                            NOTIFY_STATE_EXITED("ROOT.Active01.running01.off01");
                            //#[ transition 5 
                            setup01();
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01");
                            running01_subState = on01;
                            //#[ state Active01.running01.on01.(Entry) 
                            itsMotor->on01();
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.washing01");
                            pushNullTransition();
                            on01_subState = washing01;
                            running01_active = washing01;
                            on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.washing01");
                            NOTIFY_TRANSITION_TERMINATED("5");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State open01
        case open01:
        {
            if(IS_EVENT_TYPE_OF(evClose01_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    NOTIFY_STATE_EXITED("ROOT.Active01.running01.open01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01");
                    running01_subState = on01;
                    //#[ state Active01.running01.on01.(Entry) 
                    itsMotor->on01();
                    //#]
                    on01_entShallowHist();
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State washing01
        case washing01:
        {
            res = washing01_handleEvent();
        }
        break;
        // State rising01
        case rising01:
        {
            res = rising01_handleEvent();
        }
        break;
        // State drying01
        case drying01:
        {
            res = drying01_handleEvent();
        }
        break;
        default:
            break;
    }
    return res;
}

void DishWasherRoland::on01_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01");
    running01_subState = on01;
    //#[ state Active01.running01.on01.(Entry) 
    itsMotor->on01();
    //#]
    on01EntDef();
}

void DishWasherRoland::on01EntDef() {
    NOTIFY_TRANSITION_STARTED("1");
    on01_entShallowHist();
    NOTIFY_TRANSITION_TERMINATED("1");
}

void DishWasherRoland::on01_exit() {
    on01_lastState = on01_subState;
    switch (on01_subState) {
        // State washing01
        case washing01:
        {
            popNullTransition();
            cancel(on01_timeout);
            NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.washing01");
            on01_lastState = washing01;
        }
        break;
        // State rising01
        case rising01:
        {
            popNullTransition();
            cancel(on01_timeout);
            NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.rising01");
            on01_lastState = rising01;
        }
        break;
        // State drying01
        case drying01:
        {
            popNullTransition();
            cancel(on01_timeout);
            NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.drying01");
            on01_lastState = drying01;
        }
        break;
        default:
            break;
    }
    on01_subState = OMNonState;
    //#[ state Active01.running01.on01.(Exit) 
    itsMotor01->off01();
    //#]
    NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01");
}

void DishWasherRoland::on01_entShallowHist() {
    if(on01_lastState == OMNonState)
        {
            NOTIFY_TRANSITION_STARTED("8");
            NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.washing01");
            pushNullTransition();
            on01_subState = washing01;
            running01_active = washing01;
            on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.washing01");
            NOTIFY_TRANSITION_TERMINATED("8");
        }
    else
        {
            on01_subState = on01_lastState;
            switch (on01_subState) {
                case washing01:
                {
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.washing01");
                    pushNullTransition();
                    on01_subState = washing01;
                    running01_active = washing01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.washing01");
                }
                break;
                case rising01:
                {
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.rising01");
                    pushNullTransition();
                    on01_subState = rising01;
                    running01_active = rising01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.rising01");
                }
                break;
                case drying01:
                {
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.drying01");
                    pushNullTransition();
                    on01_subState = drying01;
                    running01_active = drying01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.drying01");
                }
                break;
                default:
                    break;
            }
        }
}

IOxfReactive::TakeEventStatus DishWasherRoland::on01_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evOpen01_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("6");
            on01_exit();
            NOTIFY_STATE_ENTERED("ROOT.Active01.running01.open01");
            running01_subState = open01;
            running01_active = open01;
            NOTIFY_TRANSITION_TERMINATED("6");
            res = eventConsumed;
        }
    
    
    return res;
}

IOxfReactive::TakeEventStatus DishWasherRoland::washing01_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            if(getCurrentEvent() == on01_timeout)
                {
                    NOTIFY_TRANSITION_STARTED("9");
                    popNullTransition();
                    cancel(on01_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.washing01");
                    //#[ transition 9 
                    --washTime01;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.washing01");
                    pushNullTransition();
                    on01_subState = washing01;
                    running01_active = washing01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.washing01");
                    NOTIFY_TRANSITION_TERMINATED("9");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 10 
            if(isWashed01())
                {
                    NOTIFY_TRANSITION_STARTED("10");
                    popNullTransition();
                    cancel(on01_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.washing01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.rising01");
                    pushNullTransition();
                    on01_subState = rising01;
                    running01_active = rising01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.rising01");
                    NOTIFY_TRANSITION_TERMINATED("10");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = on01_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus DishWasherRoland::rising01_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            if(getCurrentEvent() == on01_timeout)
                {
                    NOTIFY_TRANSITION_STARTED("11");
                    popNullTransition();
                    cancel(on01_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.rising01");
                    //#[ transition 11 
                    --rinseTime01;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.rising01");
                    pushNullTransition();
                    on01_subState = rising01;
                    running01_active = rising01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.rising01");
                    NOTIFY_TRANSITION_TERMINATED("11");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 12 
            if(isRinsed01())
                {
                    NOTIFY_TRANSITION_STARTED("12");
                    popNullTransition();
                    cancel(on01_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.rising01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.drying01");
                    pushNullTransition();
                    on01_subState = drying01;
                    running01_active = drying01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.drying01");
                    NOTIFY_TRANSITION_TERMINATED("12");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = on01_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus DishWasherRoland::drying01_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            if(getCurrentEvent() == on01_timeout)
                {
                    NOTIFY_TRANSITION_STARTED("13");
                    popNullTransition();
                    cancel(on01_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active01.running01.on01.drying01");
                    //#[ transition 13 
                    --dryTime01;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.on01.drying01");
                    pushNullTransition();
                    on01_subState = drying01;
                    running01_active = drying01;
                    on01_timeout = scheduleTimeout(1000, "ROOT.Active01.running01.on01.drying01");
                    NOTIFY_TRANSITION_TERMINATED("13");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 14 
            if(isDried01())
                {
                    NOTIFY_TRANSITION_STARTED("14");
                    on01_exit();
                    //#[ transition 14 
                    cycles01++;if(isInNeedOfService01())GEN(evFault01);
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Active01.running01.off01");
                    running01_subState = off01;
                    running01_active = off01;
                    NOTIFY_TRANSITION_TERMINATED("14");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = on01_handleEvent();
        }
    return res;
}

void DishWasherRoland::mode01_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active01.mode01");
    NOTIFY_TRANSITION_STARTED("3");
    NOTIFY_STATE_ENTERED("ROOT.Active01.mode01.quick01");
    mode01_subState = quick01;
    mode01_active = quick01;
    NOTIFY_TRANSITION_TERMINATED("3");
}

IOxfReactive::TakeEventStatus DishWasherRoland::mode01_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (mode01_active) {
        // State quick01
        case quick01:
        {
            if(IS_EVENT_TYPE_OF(evMode01_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("18");
                    NOTIFY_STATE_EXITED("ROOT.Active01.mode01.quick01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.mode01.intensive01");
                    mode01_subState = intensive01;
                    mode01_active = intensive01;
                    NOTIFY_TRANSITION_TERMINATED("18");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State intensive01
        case intensive01:
        {
            if(IS_EVENT_TYPE_OF(evMode01_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("19");
                    NOTIFY_STATE_EXITED("ROOT.Active01.mode01.intensive01");
                    NOTIFY_STATE_ENTERED("ROOT.Active01.mode01.quick01");
                    mode01_subState = quick01;
                    mode01_active = quick01;
                    NOTIFY_TRANSITION_TERMINATED("19");
                    res = eventConsumed;
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDishWasherRoland::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("cycles01", x2String(myReal->cycles01));
    aomsAttributes->addAttribute("dryTime01", x2String(myReal->dryTime01));
    aomsAttributes->addAttribute("rinseTime01", x2String(myReal->rinseTime01));
    aomsAttributes->addAttribute("washTime01", x2String(myReal->washTime01));
}

void OMAnimatedDishWasherRoland::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsMotor01", false, true);
    if(myReal->itsMotor01)
        {
            aomsRelations->ADD_ITEM(myReal->itsMotor01);
        }
}

void OMAnimatedDishWasherRoland::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    if(myReal->rootState_subState == DishWasherRoland::Active01)
        {
            Active01_serializeStates(aomsState);
        }
}

void OMAnimatedDishWasherRoland::Active01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01");
    running01_serializeStates(aomsState);
    mode01_serializeStates(aomsState);
    service01_serializeStates(aomsState);
}

void OMAnimatedDishWasherRoland::service01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.service01");
    switch (myReal->service01_subState) {
        case DishWasherRoland::normal01:
        {
            normal01_serializeStates(aomsState);
        }
        break;
        case DishWasherRoland::faulty01:
        {
            faulty01_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDishWasherRoland::normal01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.service01.normal01");
}

void OMAnimatedDishWasherRoland::faulty01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.service01.faulty01");
}

void OMAnimatedDishWasherRoland::running01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.running01");
    switch (myReal->running01_subState) {
        case DishWasherRoland::off01:
        {
            off01_serializeStates(aomsState);
        }
        break;
        case DishWasherRoland::open01:
        {
            open01_serializeStates(aomsState);
        }
        break;
        case DishWasherRoland::on01:
        {
            on01_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDishWasherRoland::open01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.running01.open01");
}

void OMAnimatedDishWasherRoland::on01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.running01.on01");
    switch (myReal->on01_subState) {
        case DishWasherRoland::washing01:
        {
            washing01_serializeStates(aomsState);
        }
        break;
        case DishWasherRoland::rising01:
        {
            rising01_serializeStates(aomsState);
        }
        break;
        case DishWasherRoland::drying01:
        {
            drying01_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDishWasherRoland::washing01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.running01.on01.washing01");
}

void OMAnimatedDishWasherRoland::rising01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.running01.on01.rising01");
}

void OMAnimatedDishWasherRoland::drying01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.running01.on01.drying01");
}

void OMAnimatedDishWasherRoland::off01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.running01.off01");
}

void OMAnimatedDishWasherRoland::mode01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.mode01");
    switch (myReal->mode01_subState) {
        case DishWasherRoland::quick01:
        {
            quick01_serializeStates(aomsState);
        }
        break;
        case DishWasherRoland::intensive01:
        {
            intensive01_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDishWasherRoland::quick01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.mode01.quick01");
}

void OMAnimatedDishWasherRoland::intensive01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active01.mode01.intensive01");
}
//#]

IMPLEMENT_REACTIVE_META_P(DishWasherRoland, Default, Default, false, OMAnimatedDishWasherRoland)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Debug01\DishWasherRoland.cpp
*********************************************************************/
