/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DishWasherRoland
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\DishWasherRoland.cpp
*********************************************************************/

//## auto_generated
#include "DishWasherRoland.h"
//## package Default

//## class DishWasherRoland
DishWasherRoland::DishWasherRoland() : cycles01(0), dryTime01(0), rinseTime01(0), washTime01(0) {
}

DishWasherRoland::~DishWasherRoland() {
}

bool DishWasherRoland::isDried01() {
    //#[ operation isDried01()
    return (0==dryTime01);
    //#]
}

bool DishWasherRoland::isInNeedOfService01() {
    //#[ operation isInNeedOfService01()
    return (cycles01>MAX_CYCLES01);
    //#]
}

bool DishWasherRoland::isRinsed01() {
    //#[ operation isRinsed01()
    return (0==rinseTime01);
    
    //#]
}

bool DishWasherRoland::isWashed01() {
    //#[ operation isWashed01()
    return (0==washTime01);
    //#]
}

void DishWasherRoland::setup01() {
    //#[ operation setup01()
    rinseTime01=4;
    washTime01=5;
    dryTime01=3;
    //#]
}

int DishWasherRoland::getCycles01() const {
    return cycles01;
}

void DishWasherRoland::setCycles01(int p_cycles01) {
    cycles01 = p_cycles01;
}

int DishWasherRoland::getDryTime01() const {
    return dryTime01;
}

void DishWasherRoland::setDryTime01(int p_dryTime01) {
    dryTime01 = p_dryTime01;
}

int DishWasherRoland::getRinseTime01() const {
    return rinseTime01;
}

void DishWasherRoland::setRinseTime01(int p_rinseTime01) {
    rinseTime01 = p_rinseTime01;
}

int DishWasherRoland::getWashTime01() const {
    return washTime01;
}

void DishWasherRoland::setWashTime01(int p_washTime01) {
    washTime01 = p_washTime01;
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\DishWasherRoland.cpp
*********************************************************************/
