/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DishWasherRoland
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: DefaultComponent\DefaultConfig\DishWasherRoland.h
*********************************************************************/

#ifndef DishWasherRoland_H
#define DishWasherRoland_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class DishWasherRoland
class DishWasherRoland {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    DishWasherRoland();
    
    //## auto_generated
    ~DishWasherRoland();
    
    ////    Operations    ////
    
    //## operation isDried01()
    bool isDried01();
    
    //## operation isInNeedOfService01()
    bool isInNeedOfService01();
    
    //## operation isRinsed01()
    bool isRinsed01();
    
    //## operation isWashed01()
    bool isWashed01();
    
    //## operation setup01()
    void setup01();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getCycles01() const;
    
    //## auto_generated
    void setCycles01(int p_cycles01);
    
    //## auto_generated
    int getDryTime01() const;
    
    //## auto_generated
    void setDryTime01(int p_dryTime01);
    
    //## auto_generated
    int getRinseTime01() const;
    
    //## auto_generated
    void setRinseTime01(int p_rinseTime01);
    
    //## auto_generated
    int getWashTime01() const;
    
    //## auto_generated
    void setWashTime01(int p_washTime01);
    
    ////    Attributes    ////

protected :

    int cycles01;		//## attribute cycles01
    
    int dryTime01;		//## attribute dryTime01
    
    int rinseTime01;		//## attribute rinseTime01
    
    int washTime01;		//## attribute washTime01
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\DishWasherRoland.h
*********************************************************************/
