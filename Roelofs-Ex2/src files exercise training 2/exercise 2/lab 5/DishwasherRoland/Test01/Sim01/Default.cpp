/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Sim01
	Model Element	: Default
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Sim01\Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "DishWasherRoland.h"
//#[ ignore
#define evStart_SERIALIZE OM_NO_OP

#define evStart_UNSERIALIZE OM_NO_OP

#define evStart_CONSTRUCTOR evStart()

#define evOpen_SERIALIZE OM_NO_OP

#define evOpen_UNSERIALIZE OM_NO_OP

#define evOpen_CONSTRUCTOR evOpen()

#define evClose_SERIALIZE OM_NO_OP

#define evClose_UNSERIALIZE OM_NO_OP

#define evClose_CONSTRUCTOR evClose()

#define evService01_SERIALIZE OM_NO_OP

#define evService01_UNSERIALIZE OM_NO_OP

#define evService01_CONSTRUCTOR evService01()

#define evClose01_SERIALIZE OM_NO_OP

#define evClose01_UNSERIALIZE OM_NO_OP

#define evClose01_CONSTRUCTOR evClose01()

#define evOpen01_SERIALIZE OM_NO_OP

#define evOpen01_UNSERIALIZE OM_NO_OP

#define evOpen01_CONSTRUCTOR evOpen01()

#define evMode01_SERIALIZE OM_NO_OP

#define evMode01_UNSERIALIZE OM_NO_OP

#define evMode01_CONSTRUCTOR evMode01()

#define evStart01_SERIALIZE OM_NO_OP

#define evStart01_UNSERIALIZE OM_NO_OP

#define evStart01_CONSTRUCTOR evStart01()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evStart()
evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart())

//## event evOpen()
evOpen::evOpen() {
    NOTIFY_EVENT_CONSTRUCTOR(evOpen)
    setId(evOpen_Default_id);
}

bool evOpen::isTypeOf(const short id) const {
    return (evOpen_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evOpen, Default, Default, evOpen())

//## event evClose()
evClose::evClose() {
    NOTIFY_EVENT_CONSTRUCTOR(evClose)
    setId(evClose_Default_id);
}

bool evClose::isTypeOf(const short id) const {
    return (evClose_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evClose, Default, Default, evClose())

//## event evService01()
evService01::evService01() {
    NOTIFY_EVENT_CONSTRUCTOR(evService01)
    setId(evService01_Default_id);
}

bool evService01::isTypeOf(const short id) const {
    return (evService01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evService01, Default, Default, evService01())

//## event evClose01()
evClose01::evClose01() {
    NOTIFY_EVENT_CONSTRUCTOR(evClose01)
    setId(evClose01_Default_id);
}

bool evClose01::isTypeOf(const short id) const {
    return (evClose01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evClose01, Default, Default, evClose01())

//## event evOpen01()
evOpen01::evOpen01() {
    NOTIFY_EVENT_CONSTRUCTOR(evOpen01)
    setId(evOpen01_Default_id);
}

bool evOpen01::isTypeOf(const short id) const {
    return (evOpen01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evOpen01, Default, Default, evOpen01())

//## event evMode01()
evMode01::evMode01() {
    NOTIFY_EVENT_CONSTRUCTOR(evMode01)
    setId(evMode01_Default_id);
}

bool evMode01::isTypeOf(const short id) const {
    return (evMode01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evMode01, Default, Default, evMode01())

//## event evStart01()
evStart01::evStart01() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart01)
    setId(evStart01_Default_id);
}

bool evStart01::isTypeOf(const short id) const {
    return (evStart01_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart01, Default, Default, evStart01())

/*********************************************************************
	File Path	: Test01\Sim01\Default.cpp
*********************************************************************/
