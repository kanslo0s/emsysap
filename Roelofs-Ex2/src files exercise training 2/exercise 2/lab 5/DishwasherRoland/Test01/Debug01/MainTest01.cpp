/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: Debug01
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test01\Debug01\MainTest01.cpp
*********************************************************************/

//## auto_generated
#include "MainTest01.h"
//## auto_generated
#include "DishWasherRoland.h"
int main(int argc, char* argv[]) {
    int status = 0;
    if(OXF::initialize(argc, argv, 6423))
        {
            DishWasherRoland * p_DishWasherRoland;
            p_DishWasherRoland = new DishWasherRoland;
            p_DishWasherRoland->startBehavior();
            //#[ configuration Test01::Debug01 
            //#]
            OXF::start();
            delete p_DishWasherRoland;
            status = 0;
        }
    else
        {
            status = 1;
        }
    return status;
}

/*********************************************************************
	File Path	: Test01\Debug01\MainTest01.cpp
*********************************************************************/
