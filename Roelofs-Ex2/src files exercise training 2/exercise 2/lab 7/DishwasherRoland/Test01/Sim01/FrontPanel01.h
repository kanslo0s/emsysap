/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Sim01
	Model Element	: FrontPanel01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Sim01\FrontPanel01.h
*********************************************************************/

#ifndef FrontPanel01_H
#define FrontPanel01_H

//## auto_generated
#include <WebComponents\WebComponentsTypes.h>
//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## link itsDishWasherRoland
class DishWasherRoland;

//## package Default

//## class FrontPanel01
class FrontPanel01 : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedFrontPanel01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    FrontPanel01(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~FrontPanel01();
    
    ////    Operations    ////

private :

    //## operation processKey01(int)
    void processKey01(int aKey01);
    
    ////    Additional operations    ////

public :

    //## auto_generated
    DishWasherRoland* getItsDishWasherRoland() const;
    
    //## auto_generated
    void setItsDishWasherRoland(DishWasherRoland* p_DishWasherRoland);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

public :

//#[ ignore
    virtual const ClassWebAdapter * getItsWebAdapter() const;
    
    void visitWebAdaptedRelations() const;
//#]

protected :

//#[ ignore
    void initWebAdapters();
//#]

private :

//#[ ignore
    void evKeyPress01WebWrapper(int key01);
//#]

    ////    Relations and components    ////

protected :

    DishWasherRoland* itsDishWasherRoland;		//## link itsDishWasherRoland
    
    ////    Framework operations    ////

public :

    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // idle01:
    //## statechart_method
    inline bool idle01_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum FrontPanel01_Enum {
        OMNonState = 0,
        idle01 = 1
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]

public :

//#[ ignore
    ClassWebAdapter * itsWebAdapter;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedFrontPanel01 : virtual public AOMInstance {
    DECLARE_REACTIVE_META(FrontPanel01, OMAnimatedFrontPanel01)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void idle01_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool FrontPanel01::rootState_IN() const {
    return true;
}

inline bool FrontPanel01::idle01_IN() const {
    return rootState_subState == idle01;
}

#endif
/*********************************************************************
	File Path	: Test01\Sim01\FrontPanel01.h
*********************************************************************/
