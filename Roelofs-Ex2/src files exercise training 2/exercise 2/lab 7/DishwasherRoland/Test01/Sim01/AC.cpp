/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Sim01
	Model Element	: AC
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Sim01\AC.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "AC.h"
//#[ ignore
#define Default_AC_AC_SERIALIZE OM_NO_OP

#define Default_AC_off01_SERIALIZE OM_NO_OP

#define Default_AC_on01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class AC
AC::AC() {
    NOTIFY_CONSTRUCTOR(AC, AC(), 0, Default_AC_AC_SERIALIZE);
}

AC::~AC() {
    NOTIFY_DESTRUCTOR(~AC, false);
}

void AC::off01() {
    NOTIFY_OPERATION(off01, off01(), 0, Default_AC_off01_SERIALIZE);
    //#[ operation off01()
    std::cout << "AC Motor off" << std::endl;
    //#]
}

void AC::on01() {
    NOTIFY_OPERATION(on01, on01(), 0, Default_AC_on01_SERIALIZE);
    //#[ operation on01()
    std::cout<<"Starting AC motor"<<std::endl;
    //#]
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedAC::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedMotor01::serializeAttributes(aomsAttributes);
}

void OMAnimatedAC::serializeRelations(AOMSRelations* aomsRelations) const {
    OMAnimatedMotor01::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(AC, Default, false, Motor01, OMAnimatedMotor01, OMAnimatedAC)

OMINIT_SUPERCLASS(Motor01, OMAnimatedMotor01)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Sim01\AC.cpp
*********************************************************************/
