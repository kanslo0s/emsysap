/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: DC_Sim01
	Model Element	: FrontPanel01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\DC_Sim01\FrontPanel01.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "FrontPanel01.h"
//## link itsDishWasherRoland
#include "DishWasherRoland.h"
//#[ ignore
#define Default_FrontPanel01_FrontPanel01_SERIALIZE OM_NO_OP

#define Default_FrontPanel01_processKey01_SERIALIZE aomsmethod->addAttribute("aKey01", x2String(aKey01));
//#]

//## package Default

//## class FrontPanel01
FrontPanel01::FrontPanel01(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(FrontPanel01, FrontPanel01(), 0, Default_FrontPanel01_FrontPanel01_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsDishWasherRoland = NULL;
    initStatechart();
    initWebAdapters();
}

FrontPanel01::~FrontPanel01() {
    NOTIFY_DESTRUCTOR(~FrontPanel01, true);
    delete itsWebAdapter;
    cleanUpRelations();
}

void FrontPanel01::processKey01(int aKey01) {
    NOTIFY_OPERATION(processKey01, processKey01(int), 1, Default_FrontPanel01_processKey01_SERIALIZE);
    //#[ operation processKey01(int)
    switch (aKey01) {
     case 0: itsDishWasherRoland->GEN(evStart01); break;
     case 1: itsDishWasherRoland->GEN(evOpen01); break;
     case 2: itsDishWasherRoland->GEN(evClose01); break;
     case 3: itsDishWasherRoland->GEN(evService01); break;
     case 4: itsDishWasherRoland->GEN(evMode01); break;
     default: break;
    }
    
    //#]
}

DishWasherRoland* FrontPanel01::getItsDishWasherRoland() const {
    return itsDishWasherRoland;
}

void FrontPanel01::setItsDishWasherRoland(DishWasherRoland* p_DishWasherRoland) {
    itsDishWasherRoland = p_DishWasherRoland;
    if(p_DishWasherRoland != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsDishWasherRoland", p_DishWasherRoland, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsDishWasherRoland");
        }
}

bool FrontPanel01::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void FrontPanel01::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void FrontPanel01::cleanUpRelations() {
    if(itsDishWasherRoland != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsDishWasherRoland");
            itsDishWasherRoland = NULL;
        }
}

//#[ ignore
const ClassWebAdapter * FrontPanel01::getItsWebAdapter() const {
    return itsWebAdapter;
}

void FrontPanel01::visitWebAdaptedRelations() const {
}

void FrontPanel01::initWebAdapters() {
    itsWebAdapter = new ClassWebAdapterTmpl< FrontPanel01 >(this, "FrontPanel01");
    itsWebAdapter->AddSubObject(new NonConstIntAttrWebAdapterTmpl< FrontPanel01 >(this,
                                    "evKeyPress01",
                                     NULL,
                                     &FrontPanel01::evKeyPress01WebWrapper));
}

void FrontPanel01::evKeyPress01WebWrapper(int key01) {
    GEN_BY_X(evKeyPress01((int) key01), this);
}
//#]

void FrontPanel01::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_STATE_ENTERED("ROOT.idle01");
        rootState_subState = idle01;
        rootState_active = idle01;
    }
}

IOxfReactive::TakeEventStatus FrontPanel01::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State idle01
    if(rootState_active == idle01)
        {
            if(IS_EVENT_TYPE_OF(evKeyPress01_Default_id))
                {
                    OMSETPARAMS(evKeyPress01);
                    NOTIFY_TRANSITION_STARTED("0");
                    NOTIFY_STATE_EXITED("ROOT.idle01");
                    //#[ transition 0 
                    processKey01(params->key01);
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.idle01");
                    rootState_subState = idle01;
                    rootState_active = idle01;
                    NOTIFY_TRANSITION_TERMINATED("0");
                    res = eventConsumed;
                }
            
        }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedFrontPanel01::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsDishWasherRoland", false, true);
    if(myReal->itsDishWasherRoland)
        {
            aomsRelations->ADD_ITEM(myReal->itsDishWasherRoland);
        }
}

void OMAnimatedFrontPanel01::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    if(myReal->rootState_subState == FrontPanel01::idle01)
        {
            idle01_serializeStates(aomsState);
        }
}

void OMAnimatedFrontPanel01::idle01_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.idle01");
}
//#]

IMPLEMENT_REACTIVE_META_P(FrontPanel01, Default, Default, false, OMAnimatedFrontPanel01)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\DC_Sim01\FrontPanel01.cpp
*********************************************************************/
