/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: DC_Sim01
	Model Element	: DC
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\DC_Sim01\DC.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "DC.h"
//#[ ignore
#define Default_DC_DC_SERIALIZE OM_NO_OP

#define Default_DC_off01_SERIALIZE OM_NO_OP

#define Default_DC_on01_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DC
DC::DC() {
    NOTIFY_CONSTRUCTOR(DC, DC(), 0, Default_DC_DC_SERIALIZE);
}

DC::~DC() {
    NOTIFY_DESTRUCTOR(~DC, false);
}

void DC::off01() {
    NOTIFY_OPERATION(off01, off01(), 0, Default_DC_off01_SERIALIZE);
    //#[ operation off01()
    std::cout<<"Stopping DC motor"<<std::endl;
    //#]
}

void DC::on01() {
    NOTIFY_OPERATION(on01, on01(), 0, Default_DC_on01_SERIALIZE);
    //#[ operation on01()
    std::cout<<"Starting DC motor"<<std::endl;
    //#]
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDC::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedMotor01::serializeAttributes(aomsAttributes);
}

void OMAnimatedDC::serializeRelations(AOMSRelations* aomsRelations) const {
    OMAnimatedMotor01::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(DC, Default, false, Motor01, OMAnimatedMotor01, OMAnimatedDC)

OMINIT_SUPERCLASS(Motor01, OMAnimatedMotor01)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\DC_Sim01\DC.cpp
*********************************************************************/
