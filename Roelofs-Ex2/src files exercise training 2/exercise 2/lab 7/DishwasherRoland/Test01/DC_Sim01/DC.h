/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: DC_Sim01
	Model Element	: DC
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\DC_Sim01\DC.h
*********************************************************************/

#ifndef DC_H
#define DC_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## class DC
#include "Motor01.h"
//## package Default

//## class DC
class DC : public Motor01 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDC;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    DC();
    
    //## auto_generated
    ~DC();
    
    ////    Operations    ////
    
    //## operation off01()
    void off01();
    
    //## operation on01()
    void on01();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDC : public OMAnimatedMotor01 {
    DECLARE_META(DC, OMAnimatedDC)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\DC_Sim01\DC.h
*********************************************************************/
