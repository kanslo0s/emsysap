/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: AC_Sim01
	Model Element	: DishwasherBuilder01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\AC_Sim01\DishwasherBuilder01.h
*********************************************************************/

#ifndef DishwasherBuilder01_H
#define DishwasherBuilder01_H

//## auto_generated
#include <WebComponents\WebComponentsTypes.h>
//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## classInstance itsAC
#include "AC.h"
//## classInstance itsDishWasherRoland
#include "DishWasherRoland.h"
//## classInstance itsFrontPanel01
#include "FrontPanel01.h"
//## package Default

//## class DishwasherBuilder01
class DishwasherBuilder01 : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDishwasherBuilder01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    DishwasherBuilder01(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~DishwasherBuilder01();
    
    ////    Additional operations    ////
    
    //## auto_generated
    AC* getItsAC() const;
    
    //## auto_generated
    DishWasherRoland* getItsDishWasherRoland() const;
    
    //## auto_generated
    FrontPanel01* getItsFrontPanel01() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();

public :

//#[ ignore
    virtual const ClassWebAdapter * getItsWebAdapter() const;
    
    void visitWebAdaptedRelations() const;
//#]

protected :

//#[ ignore
    void initWebAdapters();
//#]

private :

//#[ ignore
    void notifyWebRelationModified() const;
//#]

    ////    Relations and components    ////

protected :

    AC itsAC;		//## classInstance itsAC
    
    DishWasherRoland itsDishWasherRoland;		//## classInstance itsDishWasherRoland
    
    FrontPanel01 itsFrontPanel01;		//## classInstance itsFrontPanel01
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
    
    ////    Framework    ////
    
//#[ ignore
    ClassWebAdapter * itsWebAdapter;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDishwasherBuilder01 : virtual public AOMInstance {
    DECLARE_META(DishwasherBuilder01, OMAnimatedDishwasherBuilder01)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\AC_Sim01\DishwasherBuilder01.h
*********************************************************************/
