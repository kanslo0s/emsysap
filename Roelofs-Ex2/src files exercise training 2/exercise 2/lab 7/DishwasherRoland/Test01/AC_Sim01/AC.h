/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: AC_Sim01
	Model Element	: AC
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\AC_Sim01\AC.h
*********************************************************************/

#ifndef AC_H
#define AC_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## class AC
#include "Motor01.h"
//## package Default

//## class AC
class AC : public Motor01 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedAC;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    AC();
    
    //## auto_generated
    virtual ~AC();
    
    ////    Operations    ////
    
    //## operation off01()
    virtual void off01();
    
    //## operation on01()
    virtual void on01();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedAC : public OMAnimatedMotor01 {
    DECLARE_META(AC, OMAnimatedAC)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\AC_Sim01\AC.h
*********************************************************************/
