/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: AC_Sim01
	Model Element	: DishWasherRoland
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\AC_Sim01\DishWasherRoland.h
*********************************************************************/

#ifndef DishWasherRoland_H
#define DishWasherRoland_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## auto_generated
#include <WebComponents\WebComponentsTypes.h>
//## link itsMotor01
class Motor01;

//## package Default

//## class DishWasherRoland
class DishWasherRoland : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDishWasherRoland;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    DishWasherRoland(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~DishWasherRoland();
    
    ////    Operations    ////
    
    //## operation isDried01()
    bool isDried01();
    
    //## operation isInNeedOfService01()
    bool isInNeedOfService01();
    
    //## operation isRinsed01()
    bool isRinsed01();
    
    //## operation isWashed01()
    bool isWashed01();
    
    //## operation setup01()
    void setup01();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getCycles01() const;
    
    //## auto_generated
    void setCycles01(int p_cycles01);
    
    //## auto_generated
    int getDryTime01() const;
    
    //## auto_generated
    void setDryTime01(int p_dryTime01);
    
    //## auto_generated
    int getRinseTime01() const;
    
    //## auto_generated
    void setRinseTime01(int p_rinseTime01);
    
    //## auto_generated
    int getWashTime01() const;
    
    //## auto_generated
    void setWashTime01(int p_washTime01);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    int cycles01;		//## attribute cycles01
    
    int dryTime01;		//## attribute dryTime01
    
    int rinseTime01;		//## attribute rinseTime01
    
    int washTime01;		//## attribute washTime01
    
    ////    Framework operations    ////
    
    ////    Framework    ////

public :

    //## auto_generated
    Motor01* getItsMotor01() const;
    
    //## auto_generated
    void setItsMotor01(Motor01* p_Motor01);

protected :

    //## auto_generated
    void cleanUpRelations();

public :

//#[ ignore
    virtual const ClassWebAdapter * getItsWebAdapter() const;
    
    void visitWebAdaptedRelations() const;
//#]

protected :

//#[ ignore
    void initWebAdapters();
//#]

private :

//#[ ignore
    void evService01WebWrapper();
    
    void evClose01WebWrapper();
    
    void evOpen01WebWrapper();
    
    void evMode01WebWrapper();
    
    void evStart01WebWrapper();
    
    void evFault01WebWrapper();
//#]

protected :

    Motor01* itsMotor01;		//## link itsMotor01

public :

    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Active01:
    //## statechart_method
    inline bool Active01_IN() const;
    
    //## statechart_method
    void Active01_entDef();
    
    //## statechart_method
    void Active01_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Active01_processEvent();
    
    // service01:
    //## statechart_method
    inline bool service01_IN() const;
    
    //## statechart_method
    void service01_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus service01_processEvent();
    
    // normal01:
    //## statechart_method
    inline bool normal01_IN() const;
    
    // faulty01:
    //## statechart_method
    inline bool faulty01_IN() const;
    
    // running01:
    //## statechart_method
    inline bool running01_IN() const;
    
    //## statechart_method
    void running01_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus running01_processEvent();
    
    // open01:
    //## statechart_method
    inline bool open01_IN() const;
    
    // on01:
    //## statechart_method
    inline bool on01_IN() const;
    
    //## statechart_method
    void on01_entDef();
    
    //## statechart_method
    void on01EntDef();
    
    //## statechart_method
    void on01_exit();
    
    //## statechart_method
    void on01_entShallowHist();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus on01_handleEvent();
    
    // washing01:
    //## statechart_method
    inline bool washing01_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus washing01_handleEvent();
    
    // rising01:
    //## statechart_method
    inline bool rising01_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus rising01_handleEvent();
    
    // drying01:
    //## statechart_method
    inline bool drying01_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus drying01_handleEvent();
    
    // off01:
    //## statechart_method
    inline bool off01_IN() const;
    
    // mode01:
    //## statechart_method
    inline bool mode01_IN() const;
    
    //## statechart_method
    void mode01_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus mode01_processEvent();
    
    // quick01:
    //## statechart_method
    inline bool quick01_IN() const;
    
    // intensive01:
    //## statechart_method
    inline bool intensive01_IN() const;

protected :

//#[ ignore
    enum DishWasherRoland_Enum {
        OMNonState = 0,
        Active01 = 1,
        service01 = 2,
        normal01 = 3,
        faulty01 = 4,
        running01 = 5,
        open01 = 6,
        on01 = 7,
        washing01 = 8,
        rising01 = 9,
        drying01 = 10,
        off01 = 11,
        mode01 = 12,
        quick01 = 13,
        intensive01 = 14
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int service01_subState;
    
    int service01_active;
    
    int running01_subState;
    
    int running01_active;
    
    int on01_subState;
    
    int on01_lastState;
    
    IOxfTimeout* on01_timeout;
    
    int mode01_subState;
    
    int mode01_active;
//#]

public :

//#[ ignore
    ClassWebAdapter * itsWebAdapter;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDishWasherRoland : virtual public AOMInstance {
    DECLARE_REACTIVE_META(DishWasherRoland, OMAnimatedDishWasherRoland)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Active01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void service01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void normal01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void faulty01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void running01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void open01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void on01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void washing01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void rising01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void drying01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void off01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void mode01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void quick01_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void intensive01_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool DishWasherRoland::rootState_IN() const {
    return true;
}

inline bool DishWasherRoland::Active01_IN() const {
    return rootState_subState == Active01;
}

inline bool DishWasherRoland::service01_IN() const {
    return Active01_IN();
}

inline bool DishWasherRoland::normal01_IN() const {
    return service01_subState == normal01;
}

inline bool DishWasherRoland::faulty01_IN() const {
    return service01_subState == faulty01;
}

inline bool DishWasherRoland::running01_IN() const {
    return Active01_IN();
}

inline bool DishWasherRoland::open01_IN() const {
    return running01_subState == open01;
}

inline bool DishWasherRoland::on01_IN() const {
    return running01_subState == on01;
}

inline bool DishWasherRoland::washing01_IN() const {
    return on01_subState == washing01;
}

inline bool DishWasherRoland::rising01_IN() const {
    return on01_subState == rising01;
}

inline bool DishWasherRoland::drying01_IN() const {
    return on01_subState == drying01;
}

inline bool DishWasherRoland::off01_IN() const {
    return running01_subState == off01;
}

inline bool DishWasherRoland::mode01_IN() const {
    return Active01_IN();
}

inline bool DishWasherRoland::quick01_IN() const {
    return mode01_subState == quick01;
}

inline bool DishWasherRoland::intensive01_IN() const {
    return mode01_subState == intensive01;
}

#endif
/*********************************************************************
	File Path	: Test01\AC_Sim01\DishWasherRoland.h
*********************************************************************/
