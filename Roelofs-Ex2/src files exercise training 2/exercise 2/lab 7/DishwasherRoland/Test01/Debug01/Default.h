/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: Default
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include <oxf\event.h>
//## auto_generated
class DishWasherRoland;

//## classInstance itsDishwasherBuilder01
class DishwasherBuilder01;

//## auto_generated
class FrontPanel01;

//## auto_generated
class Motor01;

//#[ ignore
#define evStart_Default_id 18601

#define evOpen_Default_id 18602

#define evClose_Default_id 18603

#define evService01_Default_id 18604

#define evClose01_Default_id 18605

#define evOpen01_Default_id 18606

#define evMode01_Default_id 18607

#define evStart01_Default_id 18608

#define evFault01_Default_id 18609

#define evKeyPress01_Default_id 18610
//#]

//## package Default


//#[ type MAX_CYCLES01
const int MAX_CYCLES01=3;
//#]

//## classInstance itsDishwasherBuilder01
extern DishwasherBuilder01 itsDishwasherBuilder01;

//## auto_generated
void Default_initRelations();

//## auto_generated
bool Default_startBehavior();

//#[ ignore
class Default_OMInitializer {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Default_OMInitializer();
    
    //## auto_generated
    ~Default_OMInitializer();
};
//#]

//## event evStart()
class evStart : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStart();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart)
};
//#]
#endif // _OMINSTRUMENT

//## event evOpen()
class evOpen : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevOpen;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evOpen();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevOpen : virtual public AOMEvent {
    DECLARE_META_EVENT(evOpen)
};
//#]
#endif // _OMINSTRUMENT

//## event evClose()
class evClose : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevClose;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evClose();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevClose : virtual public AOMEvent {
    DECLARE_META_EVENT(evClose)
};
//#]
#endif // _OMINSTRUMENT

//## event evService01()
class evService01 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevService01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evService01();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevService01 : virtual public AOMEvent {
    DECLARE_META_EVENT(evService01)
};
//#]
#endif // _OMINSTRUMENT

//## event evClose01()
class evClose01 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevClose01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evClose01();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevClose01 : virtual public AOMEvent {
    DECLARE_META_EVENT(evClose01)
};
//#]
#endif // _OMINSTRUMENT

//## event evOpen01()
class evOpen01 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevOpen01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evOpen01();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevOpen01 : virtual public AOMEvent {
    DECLARE_META_EVENT(evOpen01)
};
//#]
#endif // _OMINSTRUMENT

//## event evMode01()
class evMode01 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevMode01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evMode01();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevMode01 : virtual public AOMEvent {
    DECLARE_META_EVENT(evMode01)
};
//#]
#endif // _OMINSTRUMENT

//## event evStart01()
class evStart01 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStart01();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart01 : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart01)
};
//#]
#endif // _OMINSTRUMENT

//## event evFault01()
class evFault01 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevFault01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evFault01();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevFault01 : virtual public AOMEvent {
    DECLARE_META_EVENT(evFault01)
};
//#]
#endif // _OMINSTRUMENT

//## event evKeyPress01(int)
class evKeyPress01 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKeyPress01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKeyPress01(int p_key01 = 0);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    int key01;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKeyPress01 : virtual public AOMEvent {
    DECLARE_META_EVENT(evKeyPress01)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\Debug01\Default.h
*********************************************************************/
