/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: Motor01
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\Motor01.h
*********************************************************************/

#ifndef Motor01_H
#define Motor01_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## package Default

//## class Motor01
class Motor01 : virtual public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedMotor01;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Motor01();
    
    //## auto_generated
    virtual ~Motor01() = 0;
    
    ////    Operations    ////
    
    //## operation off01()
    virtual void off01() = 0;
    
    //## operation on01()
    virtual void on01() = 0;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedMotor01 : virtual public AOMInstance {
    DECLARE_META(Motor01, OMAnimatedMotor01)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: Test01\Debug01\Motor01.h
*********************************************************************/
