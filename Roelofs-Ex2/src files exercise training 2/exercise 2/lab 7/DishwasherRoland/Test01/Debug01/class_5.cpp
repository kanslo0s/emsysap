/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: class_5
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\class_5.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_5.h"
//#[ ignore
#define Default_class_5_class_5_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class class_5
class_5::class_5() {
    NOTIFY_CONSTRUCTOR(class_5, class_5(), 0, Default_class_5_class_5_SERIALIZE);
}

class_5::~class_5() {
    NOTIFY_DESTRUCTOR(~class_5, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_5, Default, Default, false, OMAnimatedclass_5)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Debug01\class_5.cpp
*********************************************************************/
