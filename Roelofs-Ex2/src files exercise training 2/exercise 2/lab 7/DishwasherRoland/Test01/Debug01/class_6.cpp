/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test01 
	Configuration 	: Debug01
	Model Element	: class_6
//!	Generated Date	: Sun, 15, Nov 2015  
	File Path	: Test01\Debug01\class_6.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_6.h"
//#[ ignore
#define Default_class_6_class_6_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class class_6
class_6::class_6() {
    NOTIFY_CONSTRUCTOR(class_6, class_6(), 0, Default_class_6_class_6_SERIALIZE);
}

class_6::~class_6() {
    NOTIFY_DESTRUCTOR(~class_6, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_6, Default, Default, false, OMAnimatedclass_6)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test01\Debug01\class_6.cpp
*********************************************************************/
