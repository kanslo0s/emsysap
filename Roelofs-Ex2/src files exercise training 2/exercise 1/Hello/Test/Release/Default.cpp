/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Release
	Model Element	: Default
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Release\Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "Display.h"
//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test\Release\Default.cpp
*********************************************************************/
