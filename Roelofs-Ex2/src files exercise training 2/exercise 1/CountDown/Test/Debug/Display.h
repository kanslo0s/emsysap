/*********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Debug
	Model Element	: Display
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Debug\Display.h
*********************************************************************/

#ifndef Display_H
#define Display_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## package Default

//## class Display
class Display : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDisplay;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## operation Display()
    Display(IOxfActive* theActiveContext = 0);
    
    //## operation ~Display()
    ~Display();
    
    ////    Operations    ////
    
    //## operation isDone()
    bool isDone();
    
    //## operation print(int)
    void print(int n);
    
    //## operation print(char*)
    void print(char* s);
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getCount() const;
    
    //## auto_generated
    void setCount(int p_count);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpStatechart();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    int count;		//## attribute count
    
    ////    Framework operations    ////

public :

    //## statechart_method
    void rootStateEntDef();
    
    //## statechart_method
    void PausingEnter();
    
    //## statechart_method
    void PausingExit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus PausingTakeTimeout();
    
    //## statechart_method
    void ActiveEnter();
    
    //## statechart_method
    void ActiveExit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus ActiveTakeTimeout();
    
    ////    Framework    ////
    
//#[ ignore
    Display* concept;
    
    OMState* Pausing;
    
    OMState* Active;
//#]

protected :

//#[ ignore
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDisplay : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Display, OMAnimatedDisplay)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
};
//#]
#endif // _OMINSTRUMENT

//#[ ignore
class Display_ROOT : public OMComponentState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Display_ROOT(Display* c, OMState* p);
    
    ////    Framework operations    ////
    
    //## statechart_method
    AOMInstance* getConcept() const;
    
    //## statechart_method
    virtual void entDef();
    
    ////    Framework    ////
    
    Display* concept;
};

class Display_Pausing : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Display_Pausing(Display* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    AOMInstance* getConcept() const;
    
    //## statechart_method
    virtual void enterState();
    
    //## statechart_method
    virtual void exitState();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Display* concept;
};

class Display_Active : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Display_Active(Display* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    AOMInstance* getConcept() const;
    
    //## statechart_method
    virtual void enterState();
    
    //## statechart_method
    virtual void exitState();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Display* concept;
};
//#]

#endif
/*********************************************************************
	File Path	: Test\Debug\Display.h
*********************************************************************/
