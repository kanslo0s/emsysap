/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Debug
	Model Element	: Debug
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Debug\MainTest.cpp
*********************************************************************/

//## auto_generated
#include "MainTest.h"
//## auto_generated
#include "Display.h"
int main(int argc, char* argv[]) {
    int status = 0;
    if(OXF::initialize(argc, argv, 6423))
        {
            Display * p_Display;
            p_Display = new Display;
            p_Display->startBehavior();
            //#[ configuration Test::Debug 
            //#]
            OXF::start();
            delete p_Display;
            status = 0;
        }
    else
        {
            status = 1;
        }
    return status;
}

/*********************************************************************
	File Path	: Test\Debug\MainTest.cpp
*********************************************************************/
