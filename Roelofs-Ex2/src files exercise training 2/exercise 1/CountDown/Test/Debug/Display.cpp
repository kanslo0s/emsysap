/********************************************************************
	Rhapsody	: 8.1 
	Login		: MAster
	Component	: Test 
	Configuration 	: Debug
	Model Element	: Display
//!	Generated Date	: Sat, 14, Nov 2015  
	File Path	: Test\Debug\Display.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Display.h"
//## auto_generated
#include <iostream>
//#[ ignore
#define Default_Display_Display_SERIALIZE OM_NO_OP

#define Default_Display_isDone_SERIALIZE OM_NO_OP

#define Default_Display_print_SERIALIZE aomsmethod->addAttribute("n", x2String(n));

#define OM_Default_Display_print_1_SERIALIZE aomsmethod->addAttribute("s", x2String(s));
//#]

//## package Default

//## class Display
Display::Display(IOxfActive* theActiveContext) : count(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Display, Display(), 0, Default_Display_Display_SERIALIZE);
    setActiveContext(theActiveContext, false);
    initStatechart();
    //#[ operation Display()
    print ("Constructed");
    //#]
}

Display::~Display() {
    NOTIFY_DESTRUCTOR(~Display, true);
    //#[ operation ~Display()
    print("Destroyed");
    //#]
    cleanUpStatechart();
    cancelTimeouts();
}

//#[ ignore
#undef OM_RET_TYPE
#define OM_RET_TYPE bool
#undef OM_SER_RET
#define OM_SER_RET(val) x2String(val)
#undef OM_SER_OUT
#define OM_SER_OUT 
//#]


bool Display::isDone() {
    NOTIFY_OPERATION(isDone, isDone(), 0, Default_Display_isDone_SERIALIZE);
    //#[ operation isDone()
    OM_RETURN (0==count);
    //#]
}

void Display::print(int n) {
    NOTIFY_OPERATION(print, print(int), 1, Default_Display_print_SERIALIZE);
    //#[ operation print(int)
    std::cout << "Count = " << n << std::endl;
    //#]
}

void Display::print(char* s) {
    NOTIFY_OPERATION(print, print(char*), 1, OM_Default_Display_print_1_SERIALIZE);
    //#[ operation print(char*)
    std::cout << s << std::endl;
    //#]
}

int Display::getCount() const {
    return count;
}

void Display::setCount(int p_count) {
    count = p_count;
}

bool Display::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Display::initStatechart() {
    rootState_timeout = NULL;
    delete rootState;
    rootState = new Display_ROOT(this, NULL);
    rootState->subState = NULL;
    rootState->active = NULL;
    Pausing = new Display_Pausing(this, rootState, rootState);
    Active = new Display_Active(this, rootState, rootState);
    concept = this;
}

void Display::cleanUpStatechart() {
    delete rootState;
    rootState = NULL;
    delete Pausing;
    Pausing = NULL;
    delete Active;
    Active = NULL;
}

void Display::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Display::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void Display::rootStateEntDef() {
    NOTIFY_TRANSITION_STARTED("2");
    //#[ transition 2 
    count=10;print("Started");
    //#]
    Active->entDef();
    NOTIFY_TRANSITION_TERMINATED("2");
}

void Display::PausingEnter() {
    rootState_timeout = scheduleTimeout(300, "ROOT.Pausing");
}

void Display::PausingExit() {
    cancel(rootState_timeout);
}

IOxfReactive::TakeEventStatus Display::PausingTakeTimeout() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(getCurrentEvent() == rootState_timeout)
        {
            NOTIFY_TRANSITION_STARTED("4");
            Pausing->exitState();
            Active->entDef();
            NOTIFY_TRANSITION_TERMINATED("4");
            res = eventConsumed;
        }
    return res;
}

void Display::ActiveEnter() {
    rootState_timeout = scheduleTimeout(200, "ROOT.Active");
}

void Display::ActiveExit() {
    cancel(rootState_timeout);
}

IOxfReactive::TakeEventStatus Display::ActiveTakeTimeout() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(getCurrentEvent() == rootState_timeout)
        {
            //## transition 1 
            if(isDone())
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_TRANSITION_STARTED("1");
                    Active->exitState();
                    //#[ transition 3 
                    print(count);
                    //#]
                    //#[ transition 1 
                    print("Done");
                    //#]
                    endBehavior();
                    NOTIFY_TRANSITION_TERMINATED("1");
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            else
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_TRANSITION_STARTED("0");
                    Active->exitState();
                    //#[ transition 3 
                    print(count);
                    //#]
                    //#[ transition 0 
                    --count;
                    //#]
                    Pausing->entDef();
                    NOTIFY_TRANSITION_TERMINATED("0");
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
        }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDisplay::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("count", x2String(myReal->count));
}
//#]
#endif // _OMINSTRUMENT

//#[ ignore
Display_ROOT::Display_ROOT(Display* c, OMState* p) : OMComponentState(p) {
    concept = c;
    stateHandle = "ROOT";
}

AOMInstance* Display_ROOT::getConcept() const {
    return AOM_CONCEPT;
}

void Display_ROOT::entDef() {
    enterState();
    concept->rootStateEntDef();
}

Display_Pausing::Display_Pausing(Display* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
    stateHandle = "ROOT.Pausing";
}

AOMInstance* Display_Pausing::getConcept() const {
    return AOM_CONCEPT;
}

void Display_Pausing::enterState() {
    NOTIFY_STATE_ENTERED(stateHandle);
    parent->setSubState(this);
    component->active = this;
    concept->PausingEnter();
}

void Display_Pausing::exitState() {
    concept->PausingExit();
    NOTIFY_STATE_EXITED(stateHandle);
}

IOxfReactive::TakeEventStatus Display_Pausing::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            res = concept->PausingTakeTimeout();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Display_Active::Display_Active(Display* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
    stateHandle = "ROOT.Active";
}

AOMInstance* Display_Active::getConcept() const {
    return AOM_CONCEPT;
}

void Display_Active::enterState() {
    NOTIFY_STATE_ENTERED(stateHandle);
    parent->setSubState(this);
    component->active = this;
    concept->ActiveEnter();
}

void Display_Active::exitState() {
    concept->ActiveExit();
    NOTIFY_STATE_EXITED(stateHandle);
}

IOxfReactive::TakeEventStatus Display_Active::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            res = concept->ActiveTakeTimeout();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}
//#]

#ifdef _OMINSTRUMENT
IMPLEMENT_REACTIVE_META_P(Display, Default, Default, false, OMAnimatedDisplay)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: Test\Debug\Display.cpp
*********************************************************************/
