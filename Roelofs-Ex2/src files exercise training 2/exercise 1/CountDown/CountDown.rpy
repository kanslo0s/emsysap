I-Logix-RPY-Archive version 8.10.0 C++ 8169320
{ IProject 
	- _id = GUID 67f9abc9-3d33-4373-a5c8-be96bade75f2;
	- _myState = 8192;
	- _properties = { IPropertyContainer 
		- Subjects = { IRPYRawContainer 
			- size = 1;
			- value = 
			{ IPropertySubject 
				- _Name = "CPP_CG";
				- Metaclasses = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertyMetaclass 
						- _Name = "Class";
						- Properties = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IProperty 
								- _Name = "ImpIncludes";
								- _Value = "<iostream>";
								- _Type = String;
							}
						}
					}
				}
			}
		}
	}
	- _name = "CountDown";
	- _modifiedTimeWeak = 11.14.2015::13:59:15;
	- _lastID = 1;
	- _UserColors = { IRPYRawContainer 
		- size = 16;
		- value = 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 
	}
	- _defaultSubsystem = { ISubsystemHandle 
		- _m2Class = "ISubsystem";
		- _filename = "Default.sbs";
		- _subsystem = "";
		- _class = "";
		- _name = "Default";
		- _id = GUID 67dc189c-ca74-4050-9f33-8fddb7f6a6c8;
	}
	- _component = { IHandle 
		- _m2Class = "IComponent";
		- _filename = "Test.cmp";
		- _subsystem = "";
		- _class = "";
		- _name = "Test";
		- _id = GUID 4291cf35-cbdd-4d7b-8279-421c8119d053;
	}
	- Multiplicities = { IRPYRawContainer 
		- size = 4;
		- value = 
		{ IMultiplicityItem 
			- _name = "1";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "*";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "0,1";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "1..*";
			- _count = -1;
		}
	}
	- Subsystems = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ ISubsystem 
			- fileName = "Default";
			- _id = GUID 67dc189c-ca74-4050-9f33-8fddb7f6a6c8;
		}
	}
	- Diagrams = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IDiagram 
			- _id = GUID 05e6f849-c7b8-4e7e-8bc6-dc3ddea0ddfa;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Class";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,34,84,148";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "Overview";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "11.14.2015::13:57:26";
			- _graphicChart = { CGIClassChart 
				- _id = GUID 8580fcd9-0349-4328-9e5e-c429dfd38b18;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IDiagram";
					- _id = GUID 05e6f849-c7b8-4e7e-8bc6-dc3ddea0ddfa;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 2;
				{ CGIClass 
					- _id = GUID 13577440-58a4-416a-a35e-91ff8b05f285;
					- m_type = 78;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "TopLevel";
						- _id = GUID f815665e-ee87-4a09-bede-e5ca79677e64;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "TopLevel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID 6f3f6bb3-81f6-4781-aa2d-e661262377fa;
							- m_name = "Attribute";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
						{ CGICompartment 
							- _id = GUID a937e7dd-ca23-4c56-b039-ab909f82f2a5;
							- m_name = "Operation";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIClass 
					- _id = GUID d6888ec9-8697-4ebd-bcd6-9ab3f97e2d3e;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Display";
						- _id = GUID badf637d-dc71-4947-ae16-57a19d2337fc;
					}
					- m_pParent = GUID 13577440-58a4-416a-a35e-91ff8b05f285;
					- m_name = { CGIText 
						- m_str = "Display";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2088;
					- m_transform = 0.157696 0 0 0.139037 108.685 19.2567 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_bFramesetModified = 1;
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=20%,80%>
<frame name=AttributeListCompartment>
<frame name=OperationListCompartment>";
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID a3d5c40e-3ffe-46c8-8289-a959f795d991;
							- m_name = "Attribute";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
						{ CGICompartment 
							- _id = GUID 8c0e9460-df7b-4c37-97a0-67864766dcab;
							- m_name = "Operation";
							- m_displayOption = All;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 13577440-58a4-416a-a35e-91ff8b05f285;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID 67dc189c-ca74-4050-9f33-8fddb7f6a6c8;
			}
		}
	}
	- MSCS = { IRPYRawContainer 
		- size = 3;
		- value = 
		{ IMSC 
			- _id = GUID 17c11e54-e6f3-4bb5-924d-85fda53a8600;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 2;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "execution";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "11.14.2015::13:47:48";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID 836baa9a-a44f-4f8f-9331-8cc1ee4355ed;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 17c11e54-e6f3-4bb5-924d-85fda53a8600;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 3;
				{ CGIBox 
					- _id = GUID 15605675-df22-4737-bc44-59eaf6c79876;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID df560c46-4b33-483f-9069-97448b167c90;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscColumnCR 
					- _id = GUID cff47695-a255-43ea-b11f-d5b0dd6be0b2;
					- m_type = 118;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 639be3d7-7efe-4ba0-84e4-e49c56b49730;
					}
					- m_pParent = GUID 15605675-df22-4737-bc44-59eaf6c79876;
					- m_name = { CGIText 
						- m_str = "ENV";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00874 29 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 50000  96 50000  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 47e383eb-c30f-4d90-a5df-c67943b7df38;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID e5b225fd-258e-425d-af4a-829b1de8bf88;
					}
					- m_pParent = GUID 15605675-df22-4737-bc44-59eaf6c79876;
					- m_name = { CGIText 
						- m_str = ":Display";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00876015 308 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 15605675-df22-4737-bc44-59eaf6c79876;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID 67dc189c-ca74-4050-9f33-8fddb7f6a6c8;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID df560c46-4b33-483f-9069-97448b167c90;
				- _modifiedTimeWeak = 1.2.1990::0:0:0;
				- ClassifierRoles = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IClassifierRole 
						- _id = GUID 639be3d7-7efe-4ba0-84e4-e49c56b49730;
						- _name = "ENV";
						- _modifiedTimeWeak = 11.14.2015::13:47:30;
						- m_eRoleType = SYSTEM_BORDER;
						- m_pBase = { IHandle 
							- _m2Class = "";
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID e5b225fd-258e-425d-af4a-829b1de8bf88;
						- _myState = 2048;
						- _modifiedTimeWeak = 11.14.2015::13:47:48;
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Display";
							- _id = GUID badf637d-dc71-4947-ae16-57a19d2337fc;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID 6ff10e5f-bde2-4b16-9e09-38df62967700;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 8;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Condition_Mark";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,67,39";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "CreateMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DestroyMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "ReplyMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "TimeoutMessage";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "Animated execution";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "11.14.2015::13:59:13";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID b9e2ec92-3f45-4d0b-a276-07aaf7d8725d;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 6ff10e5f-bde2-4b16-9e09-38df62967700;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 64;
				{ CGIBox 
					- _id = GUID 5c57618f-1bac-459c-9416-900871ecc718;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID 937a02b7-3461-4534-a7d7-4d4f27841c9f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscColumnCR 
					- _id = GUID 184f0a42-2bd9-40df-a873-1c1f66e78a02;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "Format";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "EnvironmentLine";
										- Properties = { IRPYRawContainer 
											- size = 9;
											- value = 
											{ IProperty 
												- _Name = "DefaultSize";
												- _Value = "0,0,96,437";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Fill.FillColor";
												- _Value = "255,255,255";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Fill.FillColor@Child.SDLifelineBody";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Fill.Transparent_Fill@Child.SDLifelineBody";
												- _Value = "0";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Font";
												- _Value = "Tahoma";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Font.Size";
												- _Value = "8";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Weight@Child.NameCompartment@Name";
												- _Value = "700";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Line.LineColor";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Line.LineWidth";
												- _Value = "1";
												- _Type = Int;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 118;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID fab18ec3-2ba6-4a02-abfe-ca145017716d;
					}
					- m_pParent = GUID 5c57618f-1bac-459c-9416-900871ecc718;
					- m_name = { CGIText 
						- m_str = "ENV";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.05922 29 50 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 1 0 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "Format";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "InstanceLine";
										- Properties = { IRPYRawContainer 
											- size = 7;
											- value = 
											{ IProperty 
												- _Name = "DefaultSize";
												- _Value = "0,0,96,437";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Fill.FillColor";
												- _Value = "255,255,255";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Font.Font";
												- _Value = "Tahoma";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Font.Size";
												- _Value = "8";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Weight@Child.NameCompartment@Name";
												- _Value = "700";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Line.LineColor";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Line.LineWidth";
												- _Value = "0";
												- _Type = Int;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
					}
					- m_pParent = GUID 5c57618f-1bac-459c-9416-900871ecc718;
					- m_name = { CGIText 
						- m_str = ":Display";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0593565 308 50 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 1 0 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 377488dc-9dae-4a1d-877c-0bd7aba9045e;
					- m_type = 112;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 730f931c-3473-444f-88d6-209770b3f159;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "Create()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 184f0a42-2bd9-40df-a873-1c1f66e78a02;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 1182 ;
					- m_TargetPort = 48 1179 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 40329fe2-de71-4a71-bc8b-cd54899c953d;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3d36678b-c208-4f43-90d3-d60492517558;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Constructed)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 140  386 161  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 1516 ;
					- m_TargetPort = 48 1870 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7cbf4d51-a9b6-48f0-805b-225fc7f5b3ac;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9e88703e-49b0-4e94-bed7-8ff15ac269df;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Started)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 180  386 200  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 2207 ;
					- m_TargetPort = 48 2527 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 228999e5-b3b2-402f-b12c-e4d6be7ac1c5;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 82d930be-a10a-4718-b670-e6ad8f32f6c6;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 2762.97 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID dd6c244d-f98a-41a4-b4c5-0eeb63b7b8c5;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9724bbf2-813f-41b2-a0a7-b538ef447afa;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 290  386 311  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 4043 ;
					- m_TargetPort = 48 4397 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e825aa81-f97c-4966-836b-661fefffb3a0;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 8602be4d-4514-415a-9879-2543b06c3ee1;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 331  386 352  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 4734 ;
					- m_TargetPort = 48 5088 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7c0cfb35-05f1-4b88-b995-a067d6b8e47a;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID bfa8e597-50ea-4dbc-8b56-a9e906bf787b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 371  386 391  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 5408 ;
					- m_TargetPort = 48 5745 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 9082372a-c080-45cc-a53b-cfaca23cc23e;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c980078c-5cd7-42fe-9fa4-8d8e0bbbfe13;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 10)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 411  386 430  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 6082 ;
					- m_TargetPort = 48 6402 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 3510e36a-e79f-4ebc-868c-df7a27f65db0;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 6ab59042-f0ce-432c-bf10-44cec4236553;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 6654.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 001ca1d9-2ac5-4c4a-9304-a2df58f74147;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0214900f-8ccb-4808-b44b-f02a617e8fdf;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 522  386 543  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 7952 ;
					- m_TargetPort = 48 8306 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 9f9e36d2-78c9-4326-8984-a8a899670655;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3504f917-ef08-4e5d-9740-b46a26f590a9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 562  386 582  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 8643 ;
					- m_TargetPort = 48 8963 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 2b4fbdf4-b72a-49d3-b278-d162050214b9;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID dd3ad2cc-d522-4310-9122-ab8014f53ad0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 602  386 621  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 9300 ;
					- m_TargetPort = 48 9620 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID f8d7e485-8246-4e56-857c-12f7e622cbab;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 91ca0bec-bf5f-4fc2-8e2e-e2d357a4b418;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 9)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 642  386 662  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 9974 ;
					- m_TargetPort = 48 10311 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 05cb5bd7-9cc8-4f4f-af28-7561286ad2ec;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f4a1ff64-6652-4b04-a95a-4b82415de397;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 10546.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID a91cc9ca-f5a7-4975-be35-11782b9c6ed6;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 783cc3bf-f01c-4510-81b8-b412735881c9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 753  386 773  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 11844 ;
					- m_TargetPort = 48 12181 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 110371ff-45b0-4732-87fa-523953674441;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d382e32a-cebd-4f3f-aaba-f505bfe47d52;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 793  386 812  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 12518 ;
					- m_TargetPort = 48 12838 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID ddff4eb4-3c1d-40a5-8e4f-fdb9b976100e;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 932e6d99-5552-42d9-ab96-7db8086b3633;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 833  386 853  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 13191 ;
					- m_TargetPort = 48 13528 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1416250d-dc01-4eb0-8174-212fb434b193;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 949a8de2-a7ce-430e-965b-ea3367dcf2f0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 8)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 874  386 892  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 13882 ;
					- m_TargetPort = 48 14185 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 71729417-79c0-4eff-8891-718dc05f4b85;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 6d52dab1-0032-44bc-8b67-82748cfe0db0;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 14438.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID c9177a85-0bc8-479e-a8cf-3e3009698fe1;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 18e8f2f8-c9c4-4117-887d-db9c53631e2f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 984  386 1003  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 15735 ;
					- m_TargetPort = 48 16056 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID f8b0e346-e1ed-4b89-94d9-b6b3236ea8f1;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9606ce39-06b4-4580-953e-69f6c0425a1e;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1024  386 1044  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 16409 ;
					- m_TargetPort = 48 16746 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 34e32fba-7389-44f2-8de1-f3b3018e30c8;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2a3789ec-2e52-4eba-86d3-3e580a5215a5;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1065  386 1083  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 17100 ;
					- m_TargetPort = 48 17403 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 8e99b1cc-e0ad-42fd-bc43-96d88e452b32;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2e38ba26-efa1-4e8c-8c0d-54c74145df9b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 7)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1104  386 1124  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 17757 ;
					- m_TargetPort = 48 18094 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID c5cd58d5-94f0-4862-bc62-76ff5bba5f55;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1b2c261f-7921-4255-ba62-c092f5db3b69;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 18329.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID b40abb9e-d088-45b7-bf9a-e5465430b9c8;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ef1bed54-63c4-48e7-bf72-e41367e5bfa7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1215  386 1235  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 19627 ;
					- m_TargetPort = 48 19964 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1baddf94-75d3-4db3-af4a-96e609ee9419;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1bd44656-34fd-4171-803c-0a516f29483e;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1256  386 1274  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 20318 ;
					- m_TargetPort = 48 20621 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 31a2245f-6be6-40ee-a2da-e023364e6621;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 30b54acc-e61a-4348-a592-5698f85f2815;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1295  386 1315  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 20975 ;
					- m_TargetPort = 48 21312 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 9a0d665c-56b1-4051-953c-0df321d81899;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1e4d5a76-6688-4964-a946-ccd323bb0c2a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 6)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1336  386 1354  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 21666 ;
					- m_TargetPort = 48 21969 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 6e51467a-7bc7-4b63-a334-1683f315f26a;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ca6d17ff-79e6-4b4d-9320-90d0a3ec8577;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 22221.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID fd931494-69ea-40a7-9885-61f632614c58;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID b882839a-b422-4f51-88b6-785ec0b4050b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1447  386 1465  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 23536 ;
					- m_TargetPort = 48 23839 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1ccbc459-0b41-4195-bf70-5dc75d54ccfd;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 030f2e71-b64e-40cc-95ef-0c89af77597f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1486  386 1506  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 24193 ;
					- m_TargetPort = 48 24530 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 667c8cd3-58f1-4c42-8223-8416ee47a751;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 55cb38ac-393b-4afc-9fb0-1fa207c4fa33;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1527  386 1545  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 24884 ;
					- m_TargetPort = 48 25187 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 932a71a5-1212-42d7-bcfa-54edf4b1139b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3446271c-3f31-4790-8d2b-ad1b64ea31e9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 5)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1566  386 1586  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 25541 ;
					- m_TargetPort = 48 25878 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID e05c7f3f-fde0-496a-9836-9a7312d57287;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7b90fbb4-40a0-4850-baf7-c5a77f090a15;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 26113.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 1bb9150b-a1f7-40b6-9153-e33b6304cfec;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a9c59e89-b713-4091-9eef-d556f2cd9c3c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1677  386 1697  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 27411 ;
					- m_TargetPort = 48 27748 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 5fc779e3-a3f0-4b2d-b5fb-706a9d6c24cf;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4dae8eb0-4fbd-4714-8338-1a12af7846c5;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1718  386 1736  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 28101 ;
					- m_TargetPort = 48 28405 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a463f430-140b-4558-9a1d-05fef0479b78;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID b78e4857-d4c7-4fe8-b5a5-2ffe7380e053;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1757  386 1777  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 28758 ;
					- m_TargetPort = 48 29095 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 19db56a9-de3c-4861-b207-6d4920fb5eaa;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f4615d15-8d70-41ac-82e3-04b78d6f55b3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 4)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1798  386 1817  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 29449 ;
					- m_TargetPort = 48 29769 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID c84e5c53-c2c8-454b-a11a-750861c6c9dd;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 03cb321a-c256-40b2-8157-c28b918b16ad;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 30005.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 01b5bccc-2827-4f12-a4c6-19f0afbcba78;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f145d70f-ce5c-4e73-9105-8208858eb3fe;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1909  386 1927  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 31319 ;
					- m_TargetPort = 48 31622 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a2da5f16-cf00-4791-bdc3-230d2747f608;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d66d20c9-c99a-486a-be3e-14e11b401395;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1948  386 1968  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 31976 ;
					- m_TargetPort = 48 32313 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 940ec1c3-e63b-46c5-bb10-745d8587e765;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 725d94a9-e601-4ba0-b485-89119f1ccdd7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1989  386 2008  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 32667 ;
					- m_TargetPort = 48 32987 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID ca46faf6-2362-4f60-9667-652b5137fdd0;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7bc69987-0113-4b15-adaf-370869287ef7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 3)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2028  386 2048  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 33324 ;
					- m_TargetPort = 48 33661 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID c488f6be-58e2-4983-878b-7d2f43d87136;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d8ffe721-8756-46d2-80ec-823c88f50af8;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 33896.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 3b79a005-4367-493d-9ea3-14ad3887736a;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7263eb99-7029-4f2a-b420-468e0af3140d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2139  386 2159  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 35194 ;
					- m_TargetPort = 48 35531 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a2a38d8f-e992-4b15-a6ee-510d36ebe4b6;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 865bffee-c835-42a1-8e75-6206561452a3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2180  386 2199  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 35885 ;
					- m_TargetPort = 48 36205 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 02a91d81-886c-4209-b894-9aca81b58036;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c4e34a8f-4019-475a-ade1-175a2bc6c67a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2219  386 2239  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 36542 ;
					- m_TargetPort = 48 36879 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID de59cdfc-9e46-4bcb-9f7b-60df55aca768;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 16c53e52-0c35-4b92-854a-7dcbd593e436;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 2)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2258  386 2279  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 37199 ;
					- m_TargetPort = 48 37553 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 6f069a3b-a9ba-4f61-bad4-cb78c0ad2410;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 146ac3c2-face-405a-a008-d7809de2e12c;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 37788.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 7c094288-153f-4e0e-9160-941b8fd400d3;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3777b9b9-443f-42e2-afdf-0d4e5fa20cf3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2371  386 2389  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 39103 ;
					- m_TargetPort = 48 39406 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1f83d6da-8588-44e9-a81f-c6cfda72cf76;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID b72f8922-2a7c-468d-9134-7f1073a19d17;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2410  386 2430  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 39760 ;
					- m_TargetPort = 48 40097 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7dc643e4-e1a1-4b6a-bc24-9f254a6fca36;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 282bf457-21d4-4fa8-b673-c0bb54de1718;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2449  386 2470  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 40417 ;
					- m_TargetPort = 48 40771 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a191eca2-ecc9-4cf0-9955-76b2b6534688;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 863b562e-50bc-49ee-97b5-664002c863ba;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 1)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2490  386 2511  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 41108 ;
					- m_TargetPort = 48 41461 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 01d5267d-f178-48f1-8a8a-43f67982a519;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 85929c8b-cf40-4f29-8a64-a9086cfb455a;
					}
					- m_pParent = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 16.8473 0 41680.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID a61947a7-1bbb-424c-af98-8d2fabd2f881;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1962af1c-d500-45d3-9788-71f0067c51e4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2601  386 2621  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 42978 ;
					- m_TargetPort = 48 43315 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a50a7d0a-c2ab-4d9e-8292-fd17a3e06f8c;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 8e322e6c-faff-414b-b0e6-f7904ef0fe22;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2642  386 2661  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 43668 ;
					- m_TargetPort = 48 43988 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 801a28cf-344e-4859-922a-aac94d3e44a6;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 5871edb3-16b7-494a-977e-b5e39cc6483d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "1 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2681  386 2702  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 44325 ;
					- m_TargetPort = 48 44679 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID cbedf843-9ffe-4acb-8d55-ececa97c0be3;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1e034a03-4bcc-473a-ae53-60c57dbdab15;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 0)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2720  386 2741  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 44982 ;
					- m_TargetPort = 48 45336 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3f315d27-5909-4a05-a59e-1dbe087d58d6;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 36aa3e7f-d3cd-4fb1-97c0-948c6fb5708d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Done)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2761  386 2782  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 45673 ;
					- m_TargetPort = 48 46027 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 0cc605b2-a2da-41b6-b6a6-2da3c6b3ac46;
					- m_type = 113;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 24c3dd62-0dba-45aa-bc14-e0807231bebb;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "Destroy()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 184f0a42-2bd9-40df-a873-1c1f66e78a02;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 46488 ;
					- m_TargetPort = 48 46398 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 0306ab4e-953d-4849-b5e3-ffe0dff9755b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID dd941a0f-1b26-4530-98f9-133310b47ffd;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Destroyed)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_sourceType = 'F';
					- m_pTarget = GUID 0cb70596-eff0-4a87-b991-9bc1dc56d096;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2821  386 2841  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 46735 ;
					- m_TargetPort = 48 47021 ;
					- m_bLeft = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 5c57618f-1bac-459c-9416-900871ecc718;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID 67dc189c-ca74-4050-9f33-8fddb7f6a6c8;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID 937a02b7-3461-4534-a7d7-4d4f27841c9f;
				- _modifiedTimeWeak = 1.2.1990::0:0:0;
				- ClassifierRoles = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IClassifierRole 
						- _id = GUID fab18ec3-2ba6-4a02-abfe-ca145017716d;
						- _name = "ENV";
						- _modifiedTimeWeak = 11.14.2015::13:47:30;
						- m_eRoleType = SYSTEM_BORDER;
						- m_pBase = { IHandle 
							- _m2Class = "";
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						- _myState = 2048;
						- _modifiedTimeWeak = 11.14.2015::13:47:48;
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Display";
							- _id = GUID badf637d-dc71-4947-ae16-57a19d2337fc;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 111;
					- value = 
					{ IMessage 
						- _id = GUID 5de25a49-0ce8-4a9b-9550-0b05fa5a636f;
						- _name = "Create";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID fab18ec3-2ba6-4a02-abfe-ca145017716d;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CREATE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3b729be0-0074-4a8d-9a32-e8b7e3fd2ae7;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "2.";
						- m_szActualArgs = "s = Constructed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3eb479fa-c171-461c-ab0a-163028ab1d1f;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "3.";
						- m_szActualArgs = "s = Started";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 152c291a-e2e0-42a7-98d8-96dff493613d;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7fbffe5a-5d40-4296-bc96-b86b7e4b11d5;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "4.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID fa80d118-7b49-42ba-a0a0-fda1590a37de;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 55c7d64c-bfb9-4fd6-9ea2-857e3c61c70c;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "6.";
						- m_szActualArgs = "n = 10";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID cc75daae-040d-42f5-bee3-609a7a9486a9;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6834fbf0-a458-4342-a23e-590ac36da58b;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "7.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID e4308e2f-90f5-48dd-bf9e-28157a54d064;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "8.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3e0ce374-50df-42d8-a452-019b786b22d9;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "9.";
						- m_szActualArgs = "n = 9";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID db192b09-bb06-4149-89e6-5c4b0b9b94f6;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 09d68ff1-c273-482c-97f4-6fec8d6fcae5;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "10.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID da39c53b-d690-48ed-bcc3-bf50ed023eae;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "11.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8c2c51c4-d19a-4ee7-8a9a-5a1abb63697f;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "12.";
						- m_szActualArgs = "n = 8";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3e99742e-b97b-457a-b685-3de506b1f336;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 406ea600-836a-4ae1-b93b-88e4476973f6;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "13.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 08d82b20-3446-4acb-b674-9a140bf00ad7;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "14.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 999df875-2b67-4140-a1be-5890a781ee29;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "15.";
						- m_szActualArgs = "n = 7";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b519e5e5-5871-432d-9e6f-c6d1f92a3192;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ae7dcde1-d5fa-4d18-b59d-85135eb9c9a8;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "16.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0149fccb-b7f6-49a7-a874-ef4746da9ebb;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "17.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1397bd81-3450-4fef-a9fe-3f4059095c81;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "18.";
						- m_szActualArgs = "n = 6";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 21d04344-fe85-45a0-9061-f2906f398cc7;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d6a0a208-9427-4a80-9c76-b0b75f4ad9c3;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "19.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c4bd6215-dfe8-42ec-ab56-1d58818295f8;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "20.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c1e7d1a3-297f-4b9c-84b5-5e36ed893101;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "21.";
						- m_szActualArgs = "n = 5";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ce5d3564-3765-454b-94f7-4f44541e78c3;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 93ed666c-db6f-4de7-a880-d4ffc18b8013;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "22.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 063f9f46-c1bf-46c4-9b33-d3764de83f18;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "23.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b6c89f78-5670-40cf-afff-cfcc121c1d21;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "24.";
						- m_szActualArgs = "n = 4";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3c160749-17a0-4ae4-b55e-f0b46e6e975d;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1502f9a6-56f7-4a82-b102-1f515ba37adc;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "25.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a54875f0-2309-49ff-8b7b-4b577d367ea9;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "26.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 228b0ae9-1449-4fbc-b439-0db3b6013019;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "27.";
						- m_szActualArgs = "n = 3";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d61e27f0-16bf-40cb-b656-f7bdae548259;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 962fe594-c135-4fb5-91bc-61593310125f;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "28.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 198ddc4a-4104-4e6e-a2aa-bb8f74120e9f;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "29.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c59119d1-aec9-4bee-b86c-dc4fb720f72e;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "30.";
						- m_szActualArgs = "n = 2";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 5d005bcc-4661-444a-af95-74d83539fd9d;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 46ff9026-4f89-4401-b602-b3e1bd19baf9;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "31.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6a9dff02-5326-442e-b6b3-1207ed3b654f;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "32.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 831f4a19-5498-4239-915d-c6b5127de79d;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "33.";
						- m_szActualArgs = "n = 1";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 34c2295e-3ab0-4e36-b5cf-ac8ed5f76a2e;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID fcff1e44-a5e5-4721-a380-4e62d7fb7d7b;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "34.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 897b83eb-fe19-4822-89ab-1ec600d46a6a;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "35.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 077c4796-b628-4f93-beda-355375145061;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "36.";
						- m_szActualArgs = "n = 0";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9b202ed4-d0c3-454d-aa01-0bdde581bb52;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "37.";
						- m_szActualArgs = "s = Done";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9340b8af-a0d3-46da-8132-2fb8dfa3c549;
						- _name = "Destroy";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "38.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID fab18ec3-2ba6-4a02-abfe-ca145017716d;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DESTROY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 69d22fe7-991d-4a9f-bed7-95cf2c7344f1;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "39.";
						- m_szActualArgs = "s = Destroyed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 730f931c-3473-444f-88d6-209770b3f159;
						- _name = "Create";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID fab18ec3-2ba6-4a02-abfe-ca145017716d;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CREATE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3d36678b-c208-4f43-90d3-d60492517558;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "2.";
						- m_szActualArgs = "s = Constructed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9e88703e-49b0-4e94-bed7-8ff15ac269df;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "3.";
						- m_szActualArgs = "s = Started";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 82d930be-a10a-4718-b670-e6ad8f32f6c6;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9724bbf2-813f-41b2-a0a7-b538ef447afa;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "4.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8602be4d-4514-415a-9879-2543b06c3ee1;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID bfa8e597-50ea-4dbc-8b56-a9e906bf787b;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c980078c-5cd7-42fe-9fa4-8d8e0bbbfe13;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "7.";
						- m_szActualArgs = "n = 10";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6ab59042-f0ce-432c-bf10-44cec4236553;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0214900f-8ccb-4808-b44b-f02a617e8fdf;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "8.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3504f917-ef08-4e5d-9740-b46a26f590a9;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "9.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID dd3ad2cc-d522-4310-9122-ab8014f53ad0;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "10.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 91ca0bec-bf5f-4fc2-8e2e-e2d357a4b418;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "11.";
						- m_szActualArgs = "n = 9";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f4a1ff64-6652-4b04-a95a-4b82415de397;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 783cc3bf-f01c-4510-81b8-b412735881c9;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "12.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d382e32a-cebd-4f3f-aaba-f505bfe47d52;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "13.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 932e6d99-5552-42d9-ab96-7db8086b3633;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "14.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 949a8de2-a7ce-430e-965b-ea3367dcf2f0;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "15.";
						- m_szActualArgs = "n = 8";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6d52dab1-0032-44bc-8b67-82748cfe0db0;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 18e8f2f8-c9c4-4117-887d-db9c53631e2f;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "16.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9606ce39-06b4-4580-953e-69f6c0425a1e;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "17.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 2a3789ec-2e52-4eba-86d3-3e580a5215a5;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "18.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 2e38ba26-efa1-4e8c-8c0d-54c74145df9b;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "19.";
						- m_szActualArgs = "n = 7";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1b2c261f-7921-4255-ba62-c092f5db3b69;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ef1bed54-63c4-48e7-bf72-e41367e5bfa7;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "20.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1bd44656-34fd-4171-803c-0a516f29483e;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "21.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 30b54acc-e61a-4348-a592-5698f85f2815;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "22.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1e4d5a76-6688-4964-a946-ccd323bb0c2a;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "23.";
						- m_szActualArgs = "n = 6";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ca6d17ff-79e6-4b4d-9320-90d0a3ec8577;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b882839a-b422-4f51-88b6-785ec0b4050b;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "24.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 030f2e71-b64e-40cc-95ef-0c89af77597f;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "25.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 55cb38ac-393b-4afc-9fb0-1fa207c4fa33;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "26.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3446271c-3f31-4790-8d2b-ad1b64ea31e9;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "27.";
						- m_szActualArgs = "n = 5";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7b90fbb4-40a0-4850-baf7-c5a77f090a15;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a9c59e89-b713-4091-9eef-d556f2cd9c3c;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "28.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4dae8eb0-4fbd-4714-8338-1a12af7846c5;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "29.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b78e4857-d4c7-4fe8-b5a5-2ffe7380e053;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "30.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f4615d15-8d70-41ac-82e3-04b78d6f55b3;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "31.";
						- m_szActualArgs = "n = 4";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 03cb321a-c256-40b2-8157-c28b918b16ad;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f145d70f-ce5c-4e73-9105-8208858eb3fe;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "32.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d66d20c9-c99a-486a-be3e-14e11b401395;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "33.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 725d94a9-e601-4ba0-b485-89119f1ccdd7;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "34.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7bc69987-0113-4b15-adaf-370869287ef7;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "35.";
						- m_szActualArgs = "n = 3";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d8ffe721-8756-46d2-80ec-823c88f50af8;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7263eb99-7029-4f2a-b420-468e0af3140d;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "36.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 865bffee-c835-42a1-8e75-6206561452a3;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "37.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c4e34a8f-4019-475a-ade1-175a2bc6c67a;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "38.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 16c53e52-0c35-4b92-854a-7dcbd593e436;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "39.";
						- m_szActualArgs = "n = 2";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 146ac3c2-face-405a-a008-d7809de2e12c;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3777b9b9-443f-42e2-afdf-0d4e5fa20cf3;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "40.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b72f8922-2a7c-468d-9134-7f1073a19d17;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "41.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 282bf457-21d4-4fa8-b673-c0bb54de1718;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "42.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 863b562e-50bc-49ee-97b5-664002c863ba;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "43.";
						- m_szActualArgs = "n = 1";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 85929c8b-cf40-4f29-8a64-a9086cfb455a;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1962af1c-d500-45d3-9788-71f0067c51e4;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "44.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8e322e6c-faff-414b-b0e6-f7904ef0fe22;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "45.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 5871edb3-16b7-494a-977e-b5e39cc6483d;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "46.";
						- m_szActualArgs = "";
						- m_szReturnVal = "1 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1e034a03-4bcc-473a-ae53-60c57dbdab15;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "47.";
						- m_szActualArgs = "n = 0";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 36aa3e7f-d3cd-4fb1-97c0-948c6fb5708d;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "48.";
						- m_szActualArgs = "s = Done";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 24c3dd62-0dba-45aa-bc14-e0807231bebb;
						- _name = "Destroy";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "49.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID fab18ec3-2ba6-4a02-abfe-ca145017716d;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DESTROY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID dd941a0f-1b26-4530-98f9-133310b47ffd;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "50.";
						- m_szActualArgs = "s = Destroyed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID b139cbb1-c304-417f-9f66-4a93e8746229;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID d8368165-30fe-4235-b016-e9493ca5607d;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 8;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Condition_Mark";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,67,39";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "CreateMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DestroyMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "ReplyMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "TimeoutMessage";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "Animated execution_0";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "11.14.2015::13:52:7";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID 757b18d8-fd6c-479b-aa09-93df19d805d3;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID d8368165-30fe-4235-b016-e9493ca5607d;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 84;
				{ CGIBox 
					- _id = GUID 0ccf3be5-dd61-48b6-9107-76826f9e3feb;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID 6e09af77-704d-4072-9a99-ca0ee7196a51;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscColumnCR 
					- _id = GUID 0b7b2ea8-2088-4bc1-ac6d-70539cb8ccef;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "Format";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "EnvironmentLine";
										- Properties = { IRPYRawContainer 
											- size = 9;
											- value = 
											{ IProperty 
												- _Name = "DefaultSize";
												- _Value = "0,0,96,437";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Fill.FillColor";
												- _Value = "255,255,255";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Fill.FillColor@Child.SDLifelineBody";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Fill.Transparent_Fill@Child.SDLifelineBody";
												- _Value = "0";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Font";
												- _Value = "Tahoma";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Font.Size";
												- _Value = "8";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Weight@Child.NameCompartment@Name";
												- _Value = "700";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Line.LineColor";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Line.LineWidth";
												- _Value = "1";
												- _Type = Int;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 118;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 9ce7440e-fb2b-4a59-b285-51f1e7a699a1;
					}
					- m_pParent = GUID 0ccf3be5-dd61-48b6-9107-76826f9e3feb;
					- m_name = { CGIText 
						- m_str = "ENV";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.08142 29 50 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 1 0 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "Format";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "InstanceLine";
										- Properties = { IRPYRawContainer 
											- size = 7;
											- value = 
											{ IProperty 
												- _Name = "DefaultSize";
												- _Value = "0,0,96,437";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Fill.FillColor";
												- _Value = "255,255,255";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Font.Font";
												- _Value = "Tahoma";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Font.Size";
												- _Value = "8";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Weight@Child.NameCompartment@Name";
												- _Value = "700";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Line.LineColor";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Line.LineWidth";
												- _Value = "0";
												- _Type = Int;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
					}
					- m_pParent = GUID 0ccf3be5-dd61-48b6-9107-76826f9e3feb;
					- m_name = { CGIText 
						- m_str = ":Display";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0816077 308 50 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 1 0 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 0c316dbf-2eca-4efb-a9b2-a1dc4a2f7e43;
					- m_type = 112;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 59e18879-9a6f-4be0-94a7-3d0c58d051e7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "Create()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0b7b2ea8-2088-4bc1-ac6d-70539cb8ccef;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 847 ;
					- m_TargetPort = 48 858 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3b624a1b-b2ec-4c52-9023-9233e9f27b3b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID aa26d352-5e45-46b6-8f76-3159467428e6;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Constructed)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 141  386 160  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 1115 ;
					- m_TargetPort = 48 1348 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID d0ec2776-75d8-4ac4-aa5a-b4b2a7964f69;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c7dcf8bf-2fe4-4547-a934-c2f4a53f5b66;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Started)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 179  386 200  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 1581 ;
					- m_TargetPort = 48 1838 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID ade06c80-37db-44c0-beaf-21c730d2fd11;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 8e03f07c-581d-44a8-b3e2-b43cc0cb24c0;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 2009.61 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID a916128e-8974-45eb-a89c-3364873f90be;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 96f6ad4f-8f3a-4c16-bbf8-03a4eda95606;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 291  386 310  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 2953 ;
					- m_TargetPort = 48 3186 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID d84a963b-ce52-4e51-98b4-614470918963;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 5609848a-42e9-4065-8de2-98e2e020f3f1;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 331  386 351  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 3443 ;
					- m_TargetPort = 48 3688 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e85c3eb4-f2f6-4057-9a67-e30fdb66f5a4;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 41d93b37-c455-4be0-a6a7-208039aa4f79;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 370  386 391  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 3921 ;
					- m_TargetPort = 48 4179 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7556f4e2-bf74-4a1a-967b-7b75a125460a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID fd7c09de-0dec-4fae-b54f-fbbf05eeebb4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 10)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 411  386 432  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 4424 ;
					- m_TargetPort = 48 4681 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID e6390ddc-f493-4903-97e5-bbcb7c6cf027;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID cc9f2b3f-bcac-45d7-830a-538ddbcd8025;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 4840.23 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID bb05ab4d-99ba-4bd7-8df5-48fd875bc2fd;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 79cd6085-e8bb-4a50-81eb-58aa20a8fb2b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 522  386 542  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 5784 ;
					- m_TargetPort = 48 6029 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID dc92d033-5226-46f9-ac53-8ef519433a3f;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d4b5a9a9-3e3a-4e72-9337-bbe7a28263de;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 6200.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID b5a3fde0-b5b6-4ba1-9f0d-4f3c143df03b;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3479525d-7f4f-4c21-b1d2-5490e835ce18;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 632  386 653  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 7132 ;
					- m_TargetPort = 48 7389 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID dc68e923-0c45-4ba7-9d0a-deffb8ca80b5;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4e1b2879-e281-4a66-9210-800e6c683844;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 673  386 694  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 7634 ;
					- m_TargetPort = 48 7891 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e8231397-4b7b-4618-8a71-9fca3ed43c4a;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1c129bd8-70b2-4598-9f7e-9cee256d9622;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 713  386 732  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 8124 ;
					- m_TargetPort = 48 8357 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID f691b6e5-70da-4c2f-b693-37193747e221;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 5d4bcf98-d762-4155-a478-8eb37a36cba6;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 9)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 754  386 773  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 8627 ;
					- m_TargetPort = 48 8859 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 7f45270b-d8d7-46ae-8361-33ff55927a87;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 03cb65a9-e661-4117-9a0e-1a2c9296e96c;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 9031.01 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID e4c074d7-0385-484e-a5a8-f6f0844026c7;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 545af60f-a72d-490c-aae0-94b0aaa5c4a4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 863  386 885  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 9962 ;
					- m_TargetPort = 48 10232 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID f19a7145-c326-4997-a259-3613a027bbe6;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 71a844f7-5321-4956-93ea-efa402eb0a8f;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 10391.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 34a7f774-1193-4b19-a372-6c8a1b47f02a;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 6cb5314b-3376-4e59-9523-9108baf03889;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 975  386 994  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 11335 ;
					- m_TargetPort = 48 11568 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 2337240e-3280-45c0-b39b-4744ba3333ad;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 51c31010-4730-4972-a054-13ef4a9c15c0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1016  386 1035  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 11837 ;
					- m_TargetPort = 48 12070 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 40d31995-1603-414f-b2a0-cea00a6132be;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID bec2bed2-4709-4916-8be1-bb0d10aeef61;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1054  386 1075  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 12303 ;
					- m_TargetPort = 48 12560 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID d8f7d769-31a9-4eb7-bb00-30ed73a115fb;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4a5d81c4-7ff1-419e-8fa0-cd50732209c2;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 8)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1095  386 1116  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 12805 ;
					- m_TargetPort = 48 13062 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 8f43abbf-c992-48ae-8dc4-fbfefb581d06;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID adfa120a-d4d7-42f7-9b3d-77d849ebaf5f;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 13221.8 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 67f77d11-5045-4390-919d-44a8c29a1011;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2811cfb2-4c71-4a05-b8ce-2a849b644fb5;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1206  386 1226  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 14165 ;
					- m_TargetPort = 48 14410 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 18fdceb8-67f9-4dce-a711-aa03adaae557;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0f652f8e-c493-44d6-bc00-5e6e47955274;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 14582 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 72e8dd52-633e-4632-9b31-976bb817cf53;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a2d4e360-22dc-43cb-b057-9e26df97cf23;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1316  386 1337  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 15513 ;
					- m_TargetPort = 48 15771 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1974c245-629c-4434-bfbe-f1591836afa7;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4951d51f-7afc-4d04-a63a-87a27bca077d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1357  386 1378  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 16016 ;
					- m_TargetPort = 48 16273 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 8898966b-a87d-4d02-b307-a8e8100b797d;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID cac693b5-8688-46ce-ba53-532d9cedbbbe;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1397  386 1417  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 16506 ;
					- m_TargetPort = 48 16751 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID da0f3764-bb4d-4e81-b190-e2863fb9b26b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e0430217-15da-4907-a06a-9f21df04b847;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 7)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1438  386 1457  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 17008 ;
					- m_TargetPort = 48 17241 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 4ecba1d1-40d7-44c5-ba9f-46971cd2a6af;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 57e23050-bae3-40d1-a465-5bf7f242d18c;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 17412.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID e9ed57cd-e875-4010-b8b4-9e7fd834c8b1;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 54796d0f-98bf-4d2a-9dfd-7176fcd10e9b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1548  386 1569  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 18356 ;
					- m_TargetPort = 48 18613 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID e4922aae-11b9-4ec4-bc55-b6deebd90c7e;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 63783526-f267-4b49-a570-31da4c5d8e4a;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 18772.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID a2b19ab4-e6eb-41bb-b221-a3a7b178fead;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 26a6d917-8350-42a3-b9ab-498e636f94e4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1659  386 1679  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 19716 ;
					- m_TargetPort = 48 19961 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 337b2de1-8181-4632-a9a8-b87a76e4c095;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID b4184783-a46b-46db-8441-d882e83f0b0f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1700  386 1719  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 20219 ;
					- m_TargetPort = 48 20452 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a5e03bfa-2360-4f9d-bc9a-a1d00f3db799;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 54795f4f-e0c5-49a7-983f-0628a611d0d2;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1738  386 1759  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 20684 ;
					- m_TargetPort = 48 20942 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e6c9e1a8-d1b8-457f-b379-3adf37afdbde;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f92408c8-63c7-40cf-b22b-2131f00cb7e6;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 6)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1779  386 1798  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 21187 ;
					- m_TargetPort = 48 21420 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 74c91e93-0543-460a-86f2-2db534272167;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c31f5201-1a2c-4f86-ae97-8efeb2674593;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 21603.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 1b136e42-5da2-4286-bade-44b8271f01a9;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 44fa0c67-33ac-4c2f-b973-5c0914af8375;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 1890  386 1910  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 22547 ;
					- m_TargetPort = 48 22792 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 51a6fc69-7053-417b-898b-b8b9922ee907;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a20cda27-b463-474c-9af4-78d771a5b32e;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 22963.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID f784857e-c3f5-4e27-a8c1-e7f60b6ce531;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 40ee9ace-c4a9-440f-a0c0-30561ed6c7c0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2000  386 2021  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 23895 ;
					- m_TargetPort = 48 24152 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 20a3c630-55dd-4e12-8fa6-622b330bef0b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 06f32eb3-b8c3-44aa-8100-9942b0dc15af;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2041  386 2060  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 24397 ;
					- m_TargetPort = 48 24630 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 5260efed-95ad-4a23-b09d-3a9fe06e6c89;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ee937d65-90b9-44c6-bccb-25007d157b6e;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2081  386 2101  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 24887 ;
					- m_TargetPort = 48 25132 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7f73e936-4b3e-4bdd-a96f-60a7cce09842;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ddd3c22e-c35f-48c9-98fb-03aef5a6954e;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 5)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2122  386 2141  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 25390 ;
					- m_TargetPort = 48 25623 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID a6d4c654-1715-4d6b-b69d-50806849f00b;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID b0043706-d15c-4c0f-8b7f-35df7a0493ed;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 25794.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 992579e1-c5ee-4fb6-88e0-28aec0a7e028;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ebd6a079-dd07-46b6-a5ce-8589d237d8a5;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2232  386 2253  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 26738 ;
					- m_TargetPort = 48 26995 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID a75d6d7f-b943-4209-abd4-a0dc4374c16a;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 34af36f1-855e-4160-b37b-4791e0a334fe;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 27154.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID a8cea66b-2e00-40e6-89c7-366d886ded8d;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f07ca83d-f540-42f1-ab6e-fc2ceaed6023;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2343  386 2363  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 28098 ;
					- m_TargetPort = 48 28343 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3a6a02b1-edf6-47ea-b8fe-ad424b7a2560;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID fb7b0725-5117-40ef-9624-56d5dea12011;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2384  386 2403  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 28600 ;
					- m_TargetPort = 48 28833 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID febfa8ea-4392-48be-aacd-5ab63d5672f7;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 8aa2eb35-186c-4db3-b4db-e5916bfb8cbe;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2423  386 2444  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 29078 ;
					- m_TargetPort = 48 29335 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 26753e62-cee6-4b86-8781-2b1669ef3e83;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID b1d7ec16-1c6a-4f13-a281-e28178aa420f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 4)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2463  386 2482  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 29568 ;
					- m_TargetPort = 48 29801 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 1432144e-82b7-4906-9754-880bf1fec6ae;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f2736519-863f-4568-852b-a3ba221c6b51;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 29984.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID c5a4df0e-0631-40e3-863a-ae200822d8db;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1d88fe6e-86fd-4dab-b03d-7c058516433e;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2575  386 2594  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 30941 ;
					- m_TargetPort = 48 31174 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 80f40d2b-3390-4c16-a25c-ebdd80ca4717;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 665b3550-66e6-411e-8bb0-9306d8721742;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 31345.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 0b192c23-f0bc-4c24-a8df-c47696edf700;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 850ccbfa-6781-415b-9b05-5eb4defa8f92;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2685  386 2706  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 32289 ;
					- m_TargetPort = 48 32546 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 9d663f17-6022-471e-8492-4bcc12dfee4a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 78559e6b-f8c0-423e-8c15-d0be6bbeaea7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2725  386 2744  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 32803 ;
					- m_TargetPort = 48 33012 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 6e4c1257-7511-45e8-b5c0-cb639295de7d;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID b5d976ae-4f80-4654-b512-5dfc37e82792;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2765  386 2785  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 33269 ;
					- m_TargetPort = 48 33514 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 34c634e9-8ae3-4ed7-bae5-6cd069177e5c;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 6aa464e6-235d-4976-9dce-0aef04bd7a50;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 3)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2804  386 2825  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 33747 ;
					- m_TargetPort = 48 34004 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 9a51c53f-fc9b-41c4-a7b8-f35ce1987c8a;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e088fa84-2b9a-4d85-9e4e-28de7dd6990b;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 34175.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 9e9aa518-338e-42dc-8f56-d3e7b6ceb9c2;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7985f5ee-038e-4c93-853c-089499facf35;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 2916  386 2937  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 35119 ;
					- m_TargetPort = 48 35377 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 80c96266-514f-444a-867c-4a0744fdc22e;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0680e2fb-897b-4f47-89cf-177e74833128;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 35535.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 19c7103e-d89e-4371-b878-dae1cd7f4755;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9de39cd3-9273-45c9-bcc1-d94bfb0a3127;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3027  386 3047  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 36479 ;
					- m_TargetPort = 48 36724 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID faee5c2b-3b4f-4722-ab29-153286cfa40e;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID df93cc26-b2c4-4b60-9e69-8d1fe9cda872;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3068  386 3087  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 36982 ;
					- m_TargetPort = 48 37215 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 229d1ff8-5b1a-4c04-a0ef-e3f9bc6c921c;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 899726fe-0b86-4af6-8169-246a73ed9488;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3107  386 3128  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 37460 ;
					- m_TargetPort = 48 37717 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a5aff723-0b15-4e54-87ca-8fabd82e585a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0a55b3f2-dae9-46c7-b7fc-a4cfef4e5b64;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 2)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3147  386 3166  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 37950 ;
					- m_TargetPort = 48 38183 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 72d62ba0-aa1f-4f1d-9985-b6554d69f416;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 6e70b4f7-14be-4a8e-ada0-076e68fbd17a;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 38366.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 6e3d16b5-8228-4bde-b4d1-c36544d1693f;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 6c990153-f511-4e1f-a298-00238f9bdb2e;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3259  386 3278  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 39322 ;
					- m_TargetPort = 48 39555 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID d7a29722-4ef8-419d-856a-a8a9a4891ac7;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ef80b7d3-7e97-414a-b45c-54ff033c0891;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 39726.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 9fff4191-6cac-4b94-87f3-8df9dba0b956;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ebcdab11-f72b-44d9-8ff7-cb29671507c1;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3369  386 3390  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 40670 ;
					- m_TargetPort = 48 40928 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 24db36b1-e53d-4cbb-90ff-69cd32a14a60;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a9e9694f-d729-4d73-ab38-5993a84f477a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3409  386 3428  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 41185 ;
					- m_TargetPort = 48 41393 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 166c0ae5-63d8-4e5c-aabf-bd9a4e412a86;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4baf281d-834c-4cf1-bf58-55859b731632;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "0 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3449  386 3469  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 41650 ;
					- m_TargetPort = 48 41896 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 71623f16-d055-44d0-bf5f-20b0a28ba7b3;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d46724fc-83ec-4731-8fa6-72f6a475080b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 1)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3488  386 3509  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 42128 ;
					- m_TargetPort = 48 42386 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID da0e14ae-2dfd-48f2-af76-43156d5617ec;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 95ce9749-2ab1-42f5-b321-f69da4a0898d;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 42557.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID e4f90af3-9b5c-4633-b099-007c2d42c38c;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9584dac5-dbf9-49bd-a251-6ae46972653c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(300) at ROOT.Pausing";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3600  386 3619  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 43501 ;
					- m_TargetPort = 48 43734 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 805531db-3649-4ed8-aa98-2b3821f2564e;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7fcf6458-b7b0-4d95-8054-51345b782364;
					}
					- m_pParent = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 12.2537 0 43917.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 98af51ff-d14c-4205-bde8-337bb6bc273b;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e244dd4d-7471-43da-9e19-ddd260ab9dd7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3711  386 3731  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 44861 ;
					- m_TargetPort = 48 45106 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3a482e98-fec1-48fa-b078-53973e8b7a7a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 03d43b31-7c8e-4ff0-b8e7-8444c6218b28;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3750  386 3771  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 45339 ;
					- m_TargetPort = 48 45596 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e147322f-b296-487f-a9fa-d8829586bb5f;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9e049724-98a5-4017-bf90-a9e50cb0e5d7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "1 =isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3791  386 3812  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 45841 ;
					- m_TargetPort = 48 46099 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 19f5688c-83eb-47ff-8a03-998f9ef662cb;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3a906e8d-3053-4099-8bbf-2c04a1bcb0b2;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 0)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3831  386 3851  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 46331 ;
					- m_TargetPort = 48 46576 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1453874a-a96f-4a24-b82b-b6450129e510;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 17520e28-a1a5-4bf8-8c87-459f73382b0c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Done)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3872  386 3891  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 46834 ;
					- m_TargetPort = 48 47067 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 0b3bea57-a838-488d-9ed1-1a69bff337a7;
					- m_type = 113;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 07f25249-042f-4985-a495-fdf1a02329a1;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "Destroy()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 0b7b2ea8-2088-4bc1-ac6d-70539cb8ccef;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 47421 ;
					- m_TargetPort = 48 47299 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 529cbf9b-b04d-42a1-84f9-31c07b1865ae;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID cf19b177-3b50-4ef6-a6c2-8533b7030912;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Destroyed)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_sourceType = 'F';
					- m_pTarget = GUID d15adf81-26bc-433b-9fc9-8bb29f7b4670;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 386 3931  386 3951  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 47557 ;
					- m_TargetPort = 48 47802 ;
					- m_bLeft = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 0ccf3be5-dd61-48b6-9107-76826f9e3feb;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID 67dc189c-ca74-4050-9f33-8fddb7f6a6c8;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID 6e09af77-704d-4072-9a99-ca0ee7196a51;
				- _modifiedTimeWeak = 1.2.1990::0:0:0;
				- ClassifierRoles = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IClassifierRole 
						- _id = GUID 9ce7440e-fb2b-4a59-b285-51f1e7a699a1;
						- _name = "ENV";
						- _modifiedTimeWeak = 11.14.2015::13:47:30;
						- m_eRoleType = SYSTEM_BORDER;
						- m_pBase = { IHandle 
							- _m2Class = "";
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						- _myState = 2048;
						- _modifiedTimeWeak = 11.14.2015::13:47:48;
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Display";
							- _id = GUID badf637d-dc71-4947-ae16-57a19d2337fc;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 81;
					- value = 
					{ IMessage 
						- _id = GUID 59e18879-9a6f-4be0-94a7-3d0c58d051e7;
						- _name = "Create";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 9ce7440e-fb2b-4a59-b285-51f1e7a699a1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CREATE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID aa26d352-5e45-46b6-8f76-3159467428e6;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "2.";
						- m_szActualArgs = "s = Constructed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c7dcf8bf-2fe4-4547-a934-c2f4a53f5b66;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "3.";
						- m_szActualArgs = "s = Started";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8e03f07c-581d-44a8-b3e2-b43cc0cb24c0;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 96f6ad4f-8f3a-4c16-bbf8-03a4eda95606;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "4.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 5609848a-42e9-4065-8de2-98e2e020f3f1;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 41d93b37-c455-4be0-a6a7-208039aa4f79;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID fd7c09de-0dec-4fae-b54f-fbbf05eeebb4;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "7.";
						- m_szActualArgs = "n = 10";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID cc9f2b3f-bcac-45d7-830a-538ddbcd8025;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 79cd6085-e8bb-4a50-81eb-58aa20a8fb2b;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "8.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d4b5a9a9-3e3a-4e72-9337-bbe7a28263de;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3479525d-7f4f-4c21-b1d2-5490e835ce18;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "9.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4e1b2879-e281-4a66-9210-800e6c683844;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "10.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1c129bd8-70b2-4598-9f7e-9cee256d9622;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "11.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 5d4bcf98-d762-4155-a478-8eb37a36cba6;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "12.";
						- m_szActualArgs = "n = 9";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 03cb65a9-e661-4117-9a0e-1a2c9296e96c;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 545af60f-a72d-490c-aae0-94b0aaa5c4a4;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "13.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 71a844f7-5321-4956-93ea-efa402eb0a8f;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6cb5314b-3376-4e59-9523-9108baf03889;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "14.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 51c31010-4730-4972-a054-13ef4a9c15c0;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "15.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID bec2bed2-4709-4916-8be1-bb0d10aeef61;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "16.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4a5d81c4-7ff1-419e-8fa0-cd50732209c2;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "17.";
						- m_szActualArgs = "n = 8";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID adfa120a-d4d7-42f7-9b3d-77d849ebaf5f;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 2811cfb2-4c71-4a05-b8ce-2a849b644fb5;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "18.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0f652f8e-c493-44d6-bc00-5e6e47955274;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a2d4e360-22dc-43cb-b057-9e26df97cf23;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "19.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4951d51f-7afc-4d04-a63a-87a27bca077d;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "20.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID cac693b5-8688-46ce-ba53-532d9cedbbbe;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "21.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID e0430217-15da-4907-a06a-9f21df04b847;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "22.";
						- m_szActualArgs = "n = 7";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 57e23050-bae3-40d1-a465-5bf7f242d18c;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 54796d0f-98bf-4d2a-9dfd-7176fcd10e9b;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "23.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 63783526-f267-4b49-a570-31da4c5d8e4a;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 26a6d917-8350-42a3-b9ab-498e636f94e4;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "24.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b4184783-a46b-46db-8441-d882e83f0b0f;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "25.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 54795f4f-e0c5-49a7-983f-0628a611d0d2;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "26.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f92408c8-63c7-40cf-b22b-2131f00cb7e6;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "27.";
						- m_szActualArgs = "n = 6";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c31f5201-1a2c-4f86-ae97-8efeb2674593;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 44fa0c67-33ac-4c2f-b973-5c0914af8375;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "28.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a20cda27-b463-474c-9af4-78d771a5b32e;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 40ee9ace-c4a9-440f-a0c0-30561ed6c7c0;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "29.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 06f32eb3-b8c3-44aa-8100-9942b0dc15af;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "30.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ee937d65-90b9-44c6-bccb-25007d157b6e;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "31.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ddd3c22e-c35f-48c9-98fb-03aef5a6954e;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "32.";
						- m_szActualArgs = "n = 5";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b0043706-d15c-4c0f-8b7f-35df7a0493ed;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ebd6a079-dd07-46b6-a5ce-8589d237d8a5;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "33.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 34af36f1-855e-4160-b37b-4791e0a334fe;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f07ca83d-f540-42f1-ab6e-fc2ceaed6023;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "34.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID fb7b0725-5117-40ef-9624-56d5dea12011;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "35.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8aa2eb35-186c-4db3-b4db-e5916bfb8cbe;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "36.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b1d7ec16-1c6a-4f13-a281-e28178aa420f;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "37.";
						- m_szActualArgs = "n = 4";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f2736519-863f-4568-852b-a3ba221c6b51;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1d88fe6e-86fd-4dab-b03d-7c058516433e;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "38.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 665b3550-66e6-411e-8bb0-9306d8721742;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 850ccbfa-6781-415b-9b05-5eb4defa8f92;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "39.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 78559e6b-f8c0-423e-8c15-d0be6bbeaea7;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "40.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID b5d976ae-4f80-4654-b512-5dfc37e82792;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "41.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6aa464e6-235d-4976-9dce-0aef04bd7a50;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "42.";
						- m_szActualArgs = "n = 3";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID e088fa84-2b9a-4d85-9e4e-28de7dd6990b;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7985f5ee-038e-4c93-853c-089499facf35;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "43.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0680e2fb-897b-4f47-89cf-177e74833128;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9de39cd3-9273-45c9-bcc1-d94bfb0a3127;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "44.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID df93cc26-b2c4-4b60-9e69-8d1fe9cda872;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "45.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 899726fe-0b86-4af6-8169-246a73ed9488;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "46.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0a55b3f2-dae9-46c7-b7fc-a4cfef4e5b64;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "47.";
						- m_szActualArgs = "n = 2";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6e70b4f7-14be-4a8e-ada0-076e68fbd17a;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 6c990153-f511-4e1f-a298-00238f9bdb2e;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "48.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ef80b7d3-7e97-414a-b45c-54ff033c0891;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ebcdab11-f72b-44d9-8ff7-cb29671507c1;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "49.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a9e9694f-d729-4d73-ab38-5993a84f477a;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "50.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4baf281d-834c-4cf1-bf58-55859b731632;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "51.";
						- m_szActualArgs = "";
						- m_szReturnVal = "0 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d46724fc-83ec-4731-8fa6-72f6a475080b;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "52.";
						- m_szActualArgs = "n = 1";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 95ce9749-2ab1-42f5-b321-f69da4a0898d;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9584dac5-dbf9-49bd-a251-6ae46972653c;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "53.";
						- m_szActualArgs = "300";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Pausing";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7fcf6458-b7b0-4d95-8054-51345b782364;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID e244dd4d-7471-43da-9e19-ddd260ab9dd7;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "54.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 03d43b31-7c8e-4ff0-b8e7-8444c6218b28;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "55.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9e049724-98a5-4017-bf90-a9e50cb0e5d7;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "56.";
						- m_szActualArgs = "";
						- m_szReturnVal = "1 ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 6c18c2e6-274e-4097-8a56-11318435dde0;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3a906e8d-3053-4099-8bbf-2c04a1bcb0b2;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "57.";
						- m_szActualArgs = "n = 0";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 17520e28-a1a5-4bf8-8c87-459f73382b0c;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "58.";
						- m_szActualArgs = "s = Done";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 07f25249-042f-4985-a495-fdf1a02329a1;
						- _name = "Destroy";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "59.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 9ce7440e-fb2b-4a59-b285-51f1e7a699a1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DESTROY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID cf19b177-3b50-4ef6-a6c2-8533b7030912;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "60.";
						- m_szActualArgs = "s = Destroyed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 40020074-57e3-4506-aa3b-1676ba3176f1;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 57b45eab-c2d9-4955-ba62-16edc23680c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
			}
		}
	}
	- Components = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IComponent 
			- fileName = "Test";
			- _id = GUID 4291cf35-cbdd-4d7b-8279-421c8119d053;
		}
	}
}

